SELECT 
	Villes.id AS 'ID',
	Villes.nom AS 'nom',
	Villes.CP AS 'CP',
	Brigades.nom AS 'brigade',
	groupe_brigade.nom AS 'cob',
	Brigades.adresse AS 'adresse',
	Brigades.telephone AS 'tel',
	Brigades.fax AS 'fax',
	type_brigade.nom AS 'type',
	type_brigade.detail AS 'type_detail',
	Compagnies.nom AS 'compagnie',
	REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(LOWER(CASE WHEN Brigades.type = 1 THEN type_brigade.nom || '.' || Brigades.nom ELSE 'cob.' || groupe_brigade.nom END), ' ', '-'), '/', ''), '', '-'), 'é', 'e'), 'è', 'e') || CASE WHEN Compagnies.nom != '' THEN '@gendarmerie.interieur.gouv.fr' END AS 'email' 
FROM Villes INNER JOIN Brigades ON Villes.brigade = Brigades.id 
	LEFT OUTER JOIN groupe_brigade ON groupe_brigade.id = Brigades.cob
	LEFT OUTER JOIN Compagnies ON Brigades.compagnie = Compagnies.id 
	INNER JOIN type_brigade ON type_brigade.id = Brigades.type
ORDER BY Villes.id ASC;