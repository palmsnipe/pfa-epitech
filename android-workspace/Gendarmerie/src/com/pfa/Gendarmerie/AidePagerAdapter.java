package com.pfa.Gendarmerie;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ToggleButton;

public class AidePagerAdapter  extends PagerAdapter {
	 public int getCount() {
         return 2;
     }	 
	 ImageButton bRightArrow, bLeftArrow;
	 View coll;
	 View aideView;
     public Object instantiateItem(View collection, int position) {
    	 coll = collection;
    	 
         LayoutInflater inflater1 = (LayoutInflater) collection.getContext()
         .getSystemService(Context.LAYOUT_INFLATER_SERVICE);   
         aideView = inflater1.inflate(R.layout.aideswaplayout, null);

		 ToggleButton bIL = (ToggleButton) aideView.findViewById(R.id.ButtonAideInformationsLegale);
    	 final ToggleButton b2IL = (ToggleButton) aideView.findViewById(R.id.Button2AideInformationsLegale);
    	 final View tIL = aideView.findViewById(R.id.TextAideInformationsLegale);
    	  
    	 ToggleButton bOTV = (ToggleButton) aideView.findViewById(R.id.ButtonAideOTV);
    	 final ToggleButton b2OTV = (ToggleButton) aideView.findViewById(R.id.Button2AideOTV);
    	 final View tOTV = aideView.findViewById(R.id.AideAccueil);
    	  
    	 ToggleButton bMB = (ToggleButton) aideView.findViewById(R.id.ButtonAideBrigade);
    	 final ToggleButton b2MB = (ToggleButton) aideView.findViewById(R.id.Button2AideBrigade);
    	 final View tMB = aideView.findViewById(R.id.TextAideBrigade);
    	  
    	 ToggleButton bP = (ToggleButton) aideView.findViewById(R.id.ButtonAidePrevention);
    	 final ToggleButton b2P = (ToggleButton) aideView.findViewById(R.id.Button2AidePrevention);
    	 final View tP = aideView.findViewById(R.id.TextAidePrevention);
    	  
    	  ToggleButton bA = (ToggleButton) aideView.findViewById(R.id.ButtonAideAlerte17);
    	  	final ToggleButton b2A = (ToggleButton) aideView.findViewById(R.id.Button2AideAlerte17);
    	  	final View tA = aideView.findViewById(R.id.TextAideAlerte17);
    	  
    	  	ToggleButton bAP = (ToggleButton) aideView.findViewById(R.id.ButtonAideAPropos);
    	  	final ToggleButton b2AP = (ToggleButton) aideView.findViewById(R.id.Button2AideAPropos);
    	  	final View tAP = aideView.findViewById(R.id.TextAideAPropos);

    	  	final View tML =  aideView.findViewById(R.id.TextAideML);
    	  	final View SV =  aideView.findViewById(R.id.scroll_aide);
         

         TextView prefix = (TextView)aideView.findViewById(R.id.aide_prefix);
     	 bRightArrow = (ImageButton)aideView.findViewById(R.id.right_arrow);
         bLeftArrow = (ImageButton)aideView.findViewById(R.id.left_arrow);
         
   switch (position) {
  	 case 0:       	 
         prefix.setText("Mentions Légales");
			 bRightArrow.setImageResource(R.drawable.droite);
			 bLeftArrow.setImageResource(R.drawable.gauche_inactive);
	         prefix.setText(R.string.ML);
	         tML.setVisibility(View.VISIBLE);
	         SV.setVisibility(View.GONE);
       	  bRightArrow.setOnClickListener(new Button.OnClickListener() {
 	         public void onClick(View v) {
 	     			((ViewPager) coll).setCurrentItem(1);
 	         }
       	  });
         break;
 	 
 	 case 1:
         prefix.setText("Utilisation de l´application");
         	 
			 bLeftArrow.setImageResource(R.drawable.gauche);
			 bRightArrow.setImageResource(R.drawable.droite_inactive);
			 prefix.setText(R.string.UApp);
	         tML.setVisibility(View.GONE);
	         SV.setVisibility(View.VISIBLE);
			  bIL.setOnClickListener(new OnClickListener()
			  {	 
		  			public void onClick(View v)
		  			{
		  				if (tIL.getVisibility() == android.view.View.GONE)
		  				{tIL.setVisibility(android.view.View.VISIBLE);
		  				b2IL.setBackgroundResource(R.drawable.toggle_down);}
		  				else
		  				{tIL.setVisibility(android.view.View.GONE);
		  				b2IL.setBackgroundResource(R.drawable.toggle_right);}
		  			}
			  });
	  b2IL.setOnClickListener(new OnClickListener()
	  {	 
  			public void onClick(View v)
  			{
  				if (tIL.getVisibility() == android.view.View.GONE)
  				{tIL.setVisibility(android.view.View.VISIBLE);
  				b2IL.setBackgroundResource(R.drawable.toggle_down);}
  				else
  				{tIL.setVisibility(android.view.View.GONE);
  				b2IL.setBackgroundResource(R.drawable.toggle_right);}
  			}
	  });
	  bOTV.setOnClickListener(new OnClickListener()
	  {	 
  			public void onClick(View v)
  			{
  				if (tOTV.getVisibility() == android.view.View.GONE)
  				{tOTV.setVisibility(android.view.View.VISIBLE);
  				b2OTV.setBackgroundResource(R.drawable.toggle_down);}
  				else
  				{tOTV.setVisibility(android.view.View.GONE);
  				b2OTV.setBackgroundResource(R.drawable.toggle_right);}
  			}
	  });
	  b2OTV.setOnClickListener(new OnClickListener()
	  {	 
		public void onClick(View v)
		{
			if (tOTV.getVisibility() == android.view.View.GONE)
			{tOTV.setVisibility(android.view.View.VISIBLE);
			b2OTV.setBackgroundResource(R.drawable.toggle_down);}
			else
			{tOTV.setVisibility(android.view.View.GONE);
			b2OTV.setBackgroundResource(R.drawable.toggle_right);}
		}
	  });
	  bMB.setOnClickListener(new OnClickListener()
	  {	 
  			public void onClick(View v)
  			{
  				if (tMB.getVisibility() == android.view.View.GONE)
  				{tMB.setVisibility(android.view.View.VISIBLE);
  				b2MB.setBackgroundResource(R.drawable.toggle_down);}
  				else
  				{tMB.setVisibility(android.view.View.GONE);
  				b2MB.setBackgroundResource(R.drawable.toggle_right);}
  			}
	  });
	  b2MB.setOnClickListener(new OnClickListener()
	  {	 
		public void onClick(View v)
		{
			if (tMB.getVisibility() == android.view.View.GONE)
			{tMB.setVisibility(android.view.View.VISIBLE);
			b2MB.setBackgroundResource(R.drawable.toggle_down);}
			else
			{tMB.setVisibility(android.view.View.GONE);
			b2MB.setBackgroundResource(R.drawable.toggle_right);}
		}
	  });
	  bP.setOnClickListener(new OnClickListener()
	  {	 
  			public void onClick(View v)
  			{
  				if (tP.getVisibility() == android.view.View.GONE)
  				{tP.setVisibility(android.view.View.VISIBLE);
  				b2P.setBackgroundResource(R.drawable.toggle_down);}
  				else
  				{tP.setVisibility(android.view.View.GONE);
  				b2P.setBackgroundResource(R.drawable.toggle_right);}
  			}
	  });
	  b2P.setOnClickListener(new OnClickListener()
	  {	 
		public void onClick(View v)
		{
			if (tP.getVisibility() == android.view.View.GONE)
			{tP.setVisibility(android.view.View.VISIBLE);
			b2P.setBackgroundResource(R.drawable.toggle_down);}
			else
			{tP.setVisibility(android.view.View.GONE);
			b2P.setBackgroundResource(R.drawable.toggle_right);}
		}
	  });
	  bA.setOnClickListener(new OnClickListener()
	  {	 
  			public void onClick(View v)
  			{
  				if (tA.getVisibility() == android.view.View.GONE)
  				{tA.setVisibility(android.view.View.VISIBLE);
  				b2A.setBackgroundResource(R.drawable.toggle_down);}
  				else
  				{tA.setVisibility(android.view.View.GONE);
  				b2A.setBackgroundResource(R.drawable.toggle_right);}
  			}
	  });
	  b2A.setOnClickListener(new OnClickListener()
	  {	 
		public void onClick(View v)
		{
			if (tA.getVisibility() == android.view.View.GONE)
			{tA.setVisibility(android.view.View.VISIBLE);
			b2A.setBackgroundResource(R.drawable.toggle_down);}
			else
			{tA.setVisibility(android.view.View.GONE);
			b2A.setBackgroundResource(R.drawable.toggle_right);}
		}
	  });
	  bAP.setOnClickListener(new OnClickListener()
	  {	 
  			public void onClick(View v)
  			{
  				if (tAP.getVisibility() == android.view.View.GONE)
  				{tAP.setVisibility(android.view.View.VISIBLE);
  				b2AP.setBackgroundResource(R.drawable.toggle_down);}
  				else
  				{tAP.setVisibility(android.view.View.GONE);
  				b2AP.setBackgroundResource(R.drawable.toggle_right);}
  			}
	  });
	  b2AP.setOnClickListener(new OnClickListener()
	  {	 
		public void onClick(View v)
		{
			if (tAP.getVisibility() == android.view.View.GONE)
			{tAP.setVisibility(android.view.View.VISIBLE);
			b2AP.setBackgroundResource(R.drawable.toggle_down);}
			else
			{tAP.setVisibility(android.view.View.GONE);
			b2AP.setBackgroundResource(R.drawable.toggle_right);}
		}
	  });  	
    	 bLeftArrow.setOnClickListener(new Button.OnClickListener() {
 	         public void onClick(View v) {
 		     	((ViewPager) coll).setCurrentItem(0);
 	         }
    	  });
         break;
     }
        ((ViewPager) collection).addView(aideView, 0);
	  return aideView;
	 }


	   @Override
	     public void destroyItem(View arg0, int arg1, Object arg2) {
	         ((ViewPager) arg0).removeView((View) arg2);
	     }
	     @Override
	     public boolean isViewFromObject(View arg0, Object arg1) {
	         return arg0 == ((View) arg1);
	     }
	     @Override
	     public Parcelable saveState() {
	         return null;
	     }
}
