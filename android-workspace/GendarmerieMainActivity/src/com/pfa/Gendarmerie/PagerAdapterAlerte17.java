package com.pfa.Gendarmerie;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class PagerAdapterAlerte17 extends PagerAdapter {
	 public int getCount() {
         return 2;
     }

	 ImageButton bRightArrow, bLeftArrow;
	 Button bCallGen;
	 View coll;
	 View view;
     public Object instantiateItem(View collection, int position) {
    	 coll = collection;
    	 
         LayoutInflater inflater = (LayoutInflater) collection.getContext()
                 .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
         view = inflater.inflate(R.layout.alerte17swaplayout, null);
     	 
         TextView prefix = (TextView)view.findViewById(R.id.alerte_prefix);
     	 String[] fullvalues = collection.getContext().getResources().getStringArray(R.array.msg_array);
     	 String[] values = null;
     	 
     	 bRightArrow = (ImageButton)view.findViewById(R.id.right_arrow);
         bLeftArrow = (ImageButton)view.findViewById(R.id.left_arrow);
         bCallGen = (Button)view.findViewById(R.id.call_17); 
     	 bCallGen.setOnClickListener(appeler_gendarmerie);
         
     	 switch (position) {
     	 
     	 case 0:       	 
             prefix.setText("Si vous �tes victime d'un cambriolage");
   			 bRightArrow.setImageResource(R.drawable.droite);
   			 bLeftArrow.setImageResource(R.drawable.gauche_inactive);
             values = new String[31 - 25];
             for (int i = 25, j = 0; i < 31; i++, j++) {
           	  values[j] = fullvalues[i];
             }
           	  bRightArrow.setOnClickListener(new Button.OnClickListener() {
     	         public void onClick(View v) {
     	     			((ViewPager) coll).setCurrentItem(1);
     	         }
           	  });
             break;
     	 
     	 case 1:
             prefix.setText("Si vous �tes victime d'un vol � main arm�e");
   			 bLeftArrow.setImageResource(R.drawable.gauche);
   			 bRightArrow.setImageResource(R.drawable.droite_inactive);
             values = new String[fullvalues.length - 31];
             for (int i = 31, j = 0; i < fullvalues.length; i++, j++) {
           	  	values[j] = fullvalues[i];
             }
        	  bLeftArrow.setOnClickListener(new Button.OnClickListener() {
     	         public void onClick(View v) {
     		     	((ViewPager) coll).setCurrentItem(0);
     	         }
        	  });
             break;
         }
     	  
     	 ArrayAdapter<String> files = new ArrayAdapter<String>(collection.getContext(), 
    		  R.layout.customlayoutlist, 
                  values);
    
      ListView domlist = (ListView) view.findViewById(R.id.alerte_list);
     
     // ColorDrawable sage = new ColorDrawable(collection.getResources().getColor(R.color.transparent));
      //domlist.setDivider(sage);
      //domlist.setDividerHeight(20);
      
      
      domlist.setAdapter(files);
      ((ViewPager) collection).addView(view, 0);
      return view;
     }
     
	 Button.OnClickListener appeler_gendarmerie
	 = new Button.OnClickListener(){

	 @Override
	 public void onClick(View v) {
		 	AlertDialog.Builder builder = new AlertDialog.Builder((Context) view.getContext());
		 	
		 	LayoutInflater inflater = (LayoutInflater) ((Activity) view.getContext()).getLayoutInflater();
		 	
		    builder.setView(inflater.inflate(R.layout.dialoglayout, null))
		    .setPositiveButton(R.string.dialog_continue, new DialogInterface.OnClickListener() {
		    		@Override
	               public void onClick(DialogInterface dialog, int id) {
	            	   try {
	       		        Intent callIntent = new Intent(Intent.ACTION_CALL);
	       		        callIntent.setData(Uri.parse("tel:17"));
	       		        view.getContext().startActivity(callIntent);
	       		    } catch (ActivityNotFoundException e) {
	       		        Log.e("Appeler le 17", "Call failed", e);
	       		    }
	               }
	           })
	        .setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int id) {
	                   dialog.cancel();
	               }
	           });
		    AlertDialog dialog = builder.create();	
		    dialog.setTitle(R.string.dialog_title);
		    dialog.setIcon(android.R.drawable.ic_dialog_alert);
		    dialog.show();
	 }}; 
	 
     @Override
     public void destroyItem(View arg0, int arg1, Object arg2) {
         ((ViewPager) arg0).removeView((View) arg2);
     }
     @Override
     public boolean isViewFromObject(View arg0, Object arg1) {
         return arg0 == ((View) arg1);
     }
     @Override
     public Parcelable saveState() {
         return null;
     }
}
