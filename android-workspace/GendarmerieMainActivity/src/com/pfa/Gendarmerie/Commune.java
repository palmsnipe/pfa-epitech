package com.pfa.Gendarmerie;

public class Commune {
	String nom = "";
	String id = "";
	String cp = "";
	String brigade = "";
	String cob = "";
	String adresse = "";
	String tel = "";
	String fax = "";
	String type = "";
	String type_detail = "";
	String compagnie = "";

	public String getNom() {
		return this.nom;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getId() {
		return this.id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	public String getCp() {
		return this.cp;
	}
	
	public void setCp(String cp) {
		this.cp = cp;
	}
	
	public String getBrigade() {
		return this.brigade;
	}
	
	public void setBrigade(String brigade) {
		this.brigade = brigade;
	}
	
	public String getCob() {
		return this.cob;
	}
	
	public void setCob(String cob) {
		this.cob = cob;
	}
	
	public String getAdresse() {
		return this.adresse;
	}
	
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	
	public String getTel() {
		return this.tel;
	}
	
	public void setTel(String tel) {
		this.tel = tel;
	}
	
	public String getFax() {
		return this.fax;
	}
	
	public void setFax(String fax) {
		this.fax = fax;
	}
	
	public String getType() {
		return this.type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getTypeDetail() {
		return this.type_detail;
	}
	
	public void setTypeDetail(String type) {
		this.type_detail = type;
	}
	
	public String getCompagnie() {
		return this.compagnie;
	}
	
	public void setCompagnie(String compagnie) {
		this.compagnie = compagnie;
	}
}