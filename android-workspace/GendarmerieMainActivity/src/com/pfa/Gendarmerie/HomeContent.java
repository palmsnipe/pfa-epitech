package com.pfa.Gendarmerie;

import java.util.Random;
import com.pfa.Gendarmerie.R;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class HomeContent extends Fragment {

	 @Override
	 public View onCreateView(LayoutInflater inflater, ViewGroup container,
	   Bundle savedInstanceState) {
		 TextView prefix, tips, ribbon;
		 int index;		 
		 View homeContentView = inflater.inflate(R.layout.homecontentlayout, container, false);
		
		  String [] cMsg;
		  Random r;
		  prefix = (TextView)homeContentView.findViewById(R.id.home_prefix);
		  
		  ribbon = (TextView)homeContentView.findViewById(R.id.text_ribbon);
		  ribbon.setText(getString(R.string.home_ribbon));
		  cMsg = getResources().getStringArray(R.array.msg_array);
		  r = new Random();
		  index = r.nextInt(25);
		
		  tips = (TextView)homeContentView.findViewById(R.id.text_tips);
		  if (index < 7)
			  prefix.setText("Pensez-y ! Pour prot�ger votre domicile...");
		  else if (index > 19)
			  prefix.setText("Pensez-y ! En cas d'absence durable...");
		  else
			  prefix.setText("Pensez-y ! Les bons gestes...");
		  tips.setText(cMsg[index]);
		
	    return homeContentView;
	 }
	 
	
}