package com.pfa.Gendarmerie;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CustomListViewAdapter extends ArrayAdapter<RowItem>{

    Context context;
 
    public CustomListViewAdapter(Context context, int resourceId,
            List<RowItem> items) {
        super(context, resourceId, items);
        this.context = context;
    }
 
    /*private view holder class*/
    private class ViewHolder {
        TextView label;
        TextView adresse;
        TextView ville;
        TextView tel;
        TextView mail;
    }
 
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        RowItem rowItem = getItem(position);
 
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.customsquadlayout, null);
            holder = new ViewHolder();
            holder.label = (TextView) convertView.findViewById(R.id.label);
            holder.adresse = (TextView) convertView.findViewById(R.id.tv);
            holder.ville = (TextView) convertView.findViewById(R.id.tv2);
            holder.tel = (TextView) convertView.findViewById(R.id.tv3);
            holder.mail = (TextView) convertView.findViewById(R.id.tv4);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();
 
        holder.label.setText(rowItem.getLabel());
        holder.adresse.setText(rowItem.getAdresse());
        holder.ville.setText(rowItem.getVille());
        holder.tel.setText(rowItem.getTel());
        holder.mail.setText(rowItem.getMail());
 
        return convertView;
    }
}
