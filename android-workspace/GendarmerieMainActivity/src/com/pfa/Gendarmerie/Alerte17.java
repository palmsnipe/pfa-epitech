package com.pfa.Gendarmerie;

import com.pfa.Gendarmerie.R;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class Alerte17 extends Fragment {

	 @Override
	 public View onCreateView(LayoutInflater inflater, ViewGroup container,
	   Bundle savedInstanceState) {

		View alertView = inflater.inflate(R.layout.alerte17layout, container, false);
		PagerAdapterAlerte17 adapter = new PagerAdapterAlerte17();	
		ViewPager myPager = (ViewPager) alertView.findViewById(R.id.alerte_swap);
	 	myPager.setAdapter(adapter);
	    myPager.setCurrentItem(0);
	    
		TextView ribbon = (TextView)alertView.findViewById(R.id.text_ribbon);
		ribbon.setText(getString(R.string.alert_ribbon));
	    return alertView;
	 }	
}

