package com.pfa.Gendarmerie;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

public class Otv extends Fragment {
	public static final String SAVE = "saved_city"; 
	String city;
	
	 @Override
	 public View onCreateView(LayoutInflater inflater, ViewGroup container,
	   Bundle savedInstanceState) {
	  // TODO Auto-generated method stub
	  View otvView = inflater.inflate(R.layout.otvlayout, container, false);
	  
	  final ToggleButton DO2 = (ToggleButton) otvView.findViewById(R.id.buttondesc2OTV);
	  final View TDO = otvView.findViewById(R.id.otv_info);
	  
	  LinearLayout OtvWhat = (LinearLayout) otvView.findViewById(R.id.otv_what);
	  LinearLayout OtvHow = (LinearLayout) otvView.findViewById(R.id.otv_how);
	  
	  OtvWhat.setOnClickListener(new OnClickListener()
	  {	 
  			public void onClick(View v)
  			{
  				if (TDO.getVisibility() == View.GONE)
  				{TDO.setVisibility(View.VISIBLE);
  				DO2.setBackgroundResource(R.drawable.toggle_down);}
  				else
  				{TDO.setVisibility(View.GONE);
  				DO2.setBackgroundResource(R.drawable.toggle_right);}
  			}
	  });
	  DO2.setOnClickListener(new OnClickListener()
	  {	 
		public void onClick(View v)
		{
			if (TDO.getVisibility() == View.GONE)
			{TDO.setVisibility(View.VISIBLE);
			DO2.setBackgroundResource(R.drawable.toggle_down);}
			else
			{TDO.setVisibility(View.GONE);
			DO2.setBackgroundResource(R.drawable.toggle_right);}
		}
	  });
	  
	  final ToggleButton NO2 = (ToggleButton) otvView.findViewById(R.id.buttonneed2OTV);
	  final View TNO = otvView.findViewById(R.id.otv_needs);
	  final TextView tv = (TextView) otvView.findViewById(R.id.otv_map);
	  String text = "<a href='http://www.gendarmerie.interieur.gouv.fr/fre/content/download/585/5167/file/Demande_individuelle.pdf'>Formulaire de contact OTV</a>";
	  tv.setClickable(true);
	  tv.setMovementMethod(LinkMovementMethod.getInstance());
	  tv.setText(Html.fromHtml(text));
	  
	  
	  
	  OtvHow.setOnClickListener(new OnClickListener()
	  {	 
  			public void onClick(View v)
  			{
  				if (TNO.getVisibility() == View.GONE)
  				{TNO.setVisibility(View.VISIBLE);
  				tv.setVisibility(View.VISIBLE);
  				NO2.setBackgroundResource(R.drawable.toggle_down);}
  				else
  				{TNO.setVisibility(View.GONE);
  				tv.setVisibility(View.GONE);
  				NO2.setBackgroundResource(R.drawable.toggle_right);}
  			}
	  });
	  NO2.setOnClickListener(new OnClickListener()
	  {	 
		public void onClick(View v)
		{
			if (TNO.getVisibility() == View.GONE)
			{TNO.setVisibility(View.VISIBLE);
			tv.setVisibility(View.VISIBLE);
			NO2.setBackgroundResource(R.drawable.toggle_down);}
			else
			{TNO.setVisibility(View.GONE);
			tv.setVisibility(View.GONE);
			NO2.setBackgroundResource(R.drawable.toggle_right);}
		}
	  });
	  
	  TextView ribbon = (TextView)otvView.findViewById(R.id.text_ribbon);
	  ribbon.setText(getString(R.string.otv_ribbon));
 	
	  View TGO = otvView.findViewById(R.id.buttonmap);
	  TGO.setVisibility(View.VISIBLE);
	  TGO.setOnClickListener(openmap);

	  return otvView;
	 }
	  
	  Button.OnClickListener openmap = new OnClickListener() {
		  
		  @Override
		  public void onClick(View v)
		  {
			  try {
				  FileInputStream fis;
				  fis = v.getContext().openFileInput(SAVE);
				  InputStreamReader in = new InputStreamReader(fis);
				  BufferedReader br = new BufferedReader(in);
				  city = br.readLine();
				  String brigade = null;
				  String adresse = null;
				  
					try {
					InputStream inStr = getResources().openRawResource(R.raw.communes);
					DocumentBuilder docBuilder=DocumentBuilderFactory.newInstance().newDocumentBuilder();
					Document doc = docBuilder.parse(inStr, null);
					NodeList my_words = doc.getElementsByTagName("row");
					for (int i=0; i < my_words.getLength(); i++) {
			    		if (((Element)my_words.item(i)).getAttribute("brigade").equals(city) ) {
			    			brigade = ((Element)my_words.item(i)).getAttribute("brigade");
			    			adresse = ((Element)my_words.item(i)).getAttribute("adresse");
						}
					}
					inStr.close();
					Intent maper = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?q="+adresse+","+brigade+",france"));
					startActivity(maper);
				   }			    
				   catch (Throwable t) {
				    	//Toast.makeText(this, "Exception: "+t.toString(),Toast.LENGTH_LONG).show();
				    }
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
					builder.setMessage("Veuillez préalablement rechercher la brigade de gendarmerie ou le commissariat de police concerné.");
					builder.setCancelable(false);
					builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			               public void onClick(final DialogInterface dialog, final int id) {
			            	   Fragment newFragment = new Brigade();
			            	   FragmentTransaction transaction = getFragmentManager().beginTransaction();
			            	   transaction.replace(R.id.main_fragment, newFragment);
			            	   transaction.addToBackStack(null);
			            	   transaction.commit();
			               }
			           })
			           .setNegativeButton("Retour", new DialogInterface.OnClickListener() {
			               public void onClick(final DialogInterface dialog, final int id) {
			                    dialog.cancel();
			               }
			           });
			    final AlertDialog alert = builder.create();
			    alert.show();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}			  

		  }
	  };
}
