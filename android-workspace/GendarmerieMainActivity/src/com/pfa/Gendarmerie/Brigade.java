package com.pfa.Gendarmerie;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


public class Brigade extends Fragment {
	TextView my_selection;
	ImageButton save;
	ImageButton gps;
	EditText entry;
	String brig = null;
	String type = null;
	String city = null;
	double longitude = 0;
	double latitude = 0;	
	View brigadeView;
	NodeList my_words;
	List<RowItem> rowItems;
	ArrayList<String> my_items = new ArrayList<String>();
	ArrayAdapter<String> files;
	
	ListView myList;
	String[] brigades = null; 
	public static final String SAVE = "saved_city"; 

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		if (container == null) {
			return null;
		}

		brigadeView = inflater.inflate(R.layout.brigadelayout, container, false);

		my_selection = (TextView) brigadeView.findViewById(R.id.brigade_info_label);
	    final AutoCompleteTextView entry = (AutoCompleteTextView) brigadeView.findViewById(R.id.editText1);
	   
	    String[] cities = null; 
	    
		
	    try {
		InputStream inStr = getResources().openRawResource(R.raw.communes);
		
		DocumentBuilder docBuilder=DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document doc = docBuilder.parse(inStr, null);
		my_words = doc.getElementsByTagName("row");
		cities = new String[my_words.getLength()];
		brigades = new String[my_words.getLength()];
		//textView.setThreshold(2);
		  
		for (int i=0; i < my_words.getLength(); i++) {
    		my_items.add(((Element)my_words.item(i)).getAttribute("nom"));
    		cities[i] = my_items.get(i);
    		brigades[i] = ((Element)my_words.item(i)).getAttribute("brigade");
			}
		 inStr.close();
	    }
	   	
	    catch (Throwable t) {
	    	//Toast.makeText(this, "Exception: "+t.toString(),Toast.LENGTH_LONG).show();
	    }
	    
	    gps = (ImageButton) brigadeView.findViewById(R.id.gpsButton);
	    
	    gps.setOnClickListener(new OnClickListener(){
			@Override		   
			public void onClick(View v) {
				LocationManager loc = (LocationManager) v.getContext().getSystemService(Context.LOCATION_SERVICE);
				if ( loc.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
					Criteria criteria = new Criteria();
					String provider = loc.getBestProvider(criteria, false);
					Location location = loc.getLastKnownLocation(provider);
					latitude = location.getLatitude();
					longitude = location.getLongitude();
					Geocoder gcd = new Geocoder(v.getContext(), Locale.getDefault());	
					try {
						List<Address> addresses = gcd.getFromLocation(latitude, longitude, 1);
						city = addresses.get(0).getLocality();
						entry.setText(city.replaceAll("-", " "));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						Toast.makeText(v.getContext(), "Impossible de se g�olocaliser. R�essayez plus tard.", Toast.LENGTH_LONG).show();
					}
				}
				else
				{
					AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
					builder.setMessage("Votre gps semble �tre d�sactiv�. Souhaitez-vous l'activer?")
					       .setCancelable(false)
					       .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
					               public void onClick(final DialogInterface dialog, final int id) {
					                   startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
					               }
					           })
					           .setNegativeButton("Non", new DialogInterface.OnClickListener() {
					               public void onClick(final DialogInterface dialog, final int id) {
					                    dialog.cancel();
					               }
					           });
					    final AlertDialog alert = builder.create();
					    alert.show();
				}
			}   		
	    });
	    
		files = new ArrayAdapter<String>(brigadeView.getContext(), R.layout.customlayoutlistacv, cities);	   
	    entry.setAdapter(files);
	
		myList = (ListView) brigadeView.findViewById(R.id.squad_list);
	    
		entry.setOnItemClickListener(new OnItemClickListener(){
			  @Override
			    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
			            long arg3) {
				  InputMethodManager imm = (InputMethodManager) brigadeView.getContext().getSystemService(
        			      Context.INPUT_METHOD_SERVICE);
        		imm.hideSoftInputFromWindow(entry.getWindowToken(), 0);
		}});
		
	    entry.addTextChangedListener(new TextWatcher() {
            // As the user types in the search field, the list is
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            			final List<RowItem> rowItems = new ArrayList<RowItem>();
            			for (int i=0; i < my_words.getLength(); i++) {
            				String tmp = ((Element)my_words.item(i)).getAttribute("nom").toLowerCase();	
            				if (tmp.contains(s.toString().toLowerCase()))
            				{
            					RowItem item = null;
            					if (((Element)my_words.item(i)).getAttribute("type").contains("ZPN"))
            					{
            						item = new RowItem(((Element)my_words.item(i)).getAttribute("CP") + " - " + ((Element)my_words.item(i)).getAttribute("nom"),
											"Poste de police correspondant : \nAdresse : " + ((Element)my_words.item(i)).getAttribute("adresse"),
											"Ville : " + ((Element)my_words.item(i)).getAttribute("nom"),
											"Tel : " + ((Element)my_words.item(i)).getAttribute("tel"),
											"Mail : " + ((Element)my_words.item(i)).getAttribute("email"));
            					}
            					else
            					{
            						item = new RowItem(((Element)my_words.item(i)).getAttribute("CP") + " - " + ((Element)my_words.item(i)).getAttribute("nom"),
            												"Brigade correspondante : \nAdresse : " + ((Element)my_words.item(i)).getAttribute("adresse"),
            												"Ville : " + ((Element)my_words.item(i)).getAttribute("brigade"),
            												"Tel : " + ((Element)my_words.item(i)).getAttribute("tel"),
            												"Mail : " + ((Element)my_words.item(i)).getAttribute("email"));							
            					}
            					brig = ((Element)my_words.item(i)).getAttribute("brigade");
        						type = ((Element)my_words.item(i)).getAttribute("type");
        						rowItems.add(item);  
            				}	
        				}
            	        CustomListViewAdapter adapter = new CustomListViewAdapter(brigadeView.getContext(), R.layout.customsquadlayout, rowItems);
            	        myList.setAdapter(adapter);
            	        myList.setOnItemClickListener(new OnItemClickListener() {
            	        	public void onItemClick(AdapterView<?> parent, View view, int pos, long id)
            				{
            	        		InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(
            	        			      Context.INPUT_METHOD_SERVICE);
            	        		imm.hideSoftInputFromWindow(entry.getWindowToken(), 0);
            	        		String label = brig;
            		    		FileOutputStream fos;
            		    		try {
            		    			fos = view.getContext().openFileOutput(SAVE, Context.MODE_PRIVATE);
            		    			fos.write(label.getBytes());
            		    			fos.close();
            		    			if (type.contains("ZPN"))
            		    				Toast.makeText(view.getContext(), "Le poste de police de " + label + " a �t� enregistr�.", Toast.LENGTH_LONG).show();
            		    			else
            		    				Toast.makeText(view.getContext(), "La brigade de " + label + " a �t� enregistr�e.", Toast.LENGTH_LONG).show();
            		    		} catch (FileNotFoundException e) {
            		    			// TODO Auto-generated catch block
            		    			e.printStackTrace();
            		    		} catch (IOException e) {
            		    			// TODO Auto-generated catch block
            		    			e.printStackTrace();
            				}
            	        }});
            			/*files = new ArrayAdapter<String>(brigadeView.getContext(), R.layout.customsquadlayout, brigades);	   
            			myList.setAdapter(files);*/
            }

            // Not uses for this program
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
		
			}
		
        });
		return brigadeView;
	}
}
