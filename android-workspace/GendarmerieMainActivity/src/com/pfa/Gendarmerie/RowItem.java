package com.pfa.Gendarmerie;

public class RowItem {
	private String label;
	private String adresse;
	private String ville;
	private String tel;
	private String mail;
	
	public RowItem(String label, String adresse, String ville, String tel, String mail) {
			this.label = label;
	        this.adresse = adresse;
	        this.ville = ville;
	        this.tel = tel;
	        this.mail = mail;
	    }
		public String getLabel() {
			return label;
		}
		public void setLabel(String label) {
			this.label = label;
		}
	    public String getAdresse() {
	        return adresse;
	    }
	    public void setAdresse(String add) {
	        this.adresse = add;
	    }
	    public String getVille() {
	        return ville;
	    }
	    public void setVille(String ville) {
	        this.ville = ville;
	    }
	    public String getTel() {
	        return tel;
	    }
	    public void setTel(String tel) {
	        this.tel = tel;
	    }
	    public String getMail() {
	        return mail;
	    }
	    public void setMail(String mail) {
	        this.mail = mail;
	    }
	    
}
