package com.pfa.Gendarmerie;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class Actualite extends Fragment {
	
	 @Override
	 public View onCreateView(LayoutInflater inflater, ViewGroup container,
	   Bundle savedInstanceState) {
	  // TODO Auto-generated method stub

	  View actuView = inflater.inflate(R.layout.actualiteslayout, container, false);
	  PagerAdapterPrevention adapter = new PagerAdapterPrevention();
	    ViewPager myPager = (ViewPager) actuView.findViewById(R.id.myfivepanelpager);
	    myPager.setAdapter(adapter);
	    myPager.setCurrentItem(0);
		TextView ribbon = (TextView)actuView.findViewById(R.id.text_ribbon);
		ribbon.setText(getString(R.string.tips_ribbon));
	    
	  return actuView;
	 }
}
