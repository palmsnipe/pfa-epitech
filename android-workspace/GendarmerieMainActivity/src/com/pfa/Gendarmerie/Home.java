package com.pfa.Gendarmerie;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


public class Home extends Fragment {

		Button bActualite, bAlerte17, bOtv;
	 @Override
	 public View onCreateView(LayoutInflater inflater, ViewGroup container,
	   Bundle savedInstanceState) {
	  View homeView = inflater.inflate(R.layout.homelayout, container, false);

      bActualite = (Button)homeView.findViewById(R.id.bactualites);
      bAlerte17 = (Button)homeView.findViewById(R.id.balerte17);
      bOtv = (Button)homeView.findViewById(R.id.botv);
	  
      android.support.v4.app.FragmentManager fragmentManager = getFragmentManager();
      FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
      
      HomeContent homecontent = new HomeContent();
      fragmentTransaction.add(R.id.topfragment, homecontent);
      fragmentTransaction.commit();

      bActualite.setOnClickListener(topMenuOnClickListener);
      bAlerte17.setOnClickListener(topMenuOnClickListener);
      bOtv.setOnClickListener(topMenuOnClickListener);
      
	  return homeView;
	 }

	 Button.OnClickListener topMenuOnClickListener
	 = new Button.OnClickListener(){

	@Override
	public void onClick(View v) {
		Fragment newFragment;
			
		if(v == bActualite){
			newFragment = new Actualite();
		}
		else if (v == bAlerte17){
			newFragment = new Alerte17();
		}
		else {
			newFragment = new Otv();
		}
		FragmentTransaction transaction = getFragmentManager().beginTransaction();
		transaction.replace(R.id.topfragment, newFragment);
		transaction.addToBackStack(null);
		transaction.commit();
	}};
}