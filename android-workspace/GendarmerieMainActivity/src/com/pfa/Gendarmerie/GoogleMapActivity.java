package com.pfa.Gendarmerie;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.StrictMode;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;


public class GoogleMapActivity extends MapActivity {
	Geocoder geoCoder;
	List<Address> addressList;
	double latitude = 0;
	double longitude = 0;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map);

		MapView mapView = (MapView) this.findViewById(R.id.mapView);
		mapView.setBuiltInZoomControls(true);
	
		MapController mc = mapView.getController();
		mc.setZoom(13);
		GeoPoint p = getGeoPoint(getLocationInfo("Ganges+france"));
		mc.animateTo(p);
		mc.setCenter(p);
		mapView.setBuiltInZoomControls(true);
		mapView.setSatellite(true);

	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}
	
	public static JSONObject getLocationInfo(String address) {
		address = address.replaceAll(" ","%20");
		HttpGet httpGet = new HttpGet("http://maps.google.com/maps/api/geocode/json?address=Ganges+france&ka&sensor=false");
		HttpClient client = new DefaultHttpClient();
		HttpResponse response;
		StringBuilder stringBuilder = new StringBuilder();

		try {
			response = client.execute(httpGet);
			System.out.println("CHATTE1");
			HttpEntity entity = response.getEntity();
			System.out.println("CHATTE2");
			InputStream stream = entity.getContent();
			System.out.println("CHATTE3");
			int b;
			while ((b = stream.read()) != -1) {
				stringBuilder.append((char) b);
			}
		} catch (ClientProtocolException e) {
			System.out.println("ZIZI");
		} catch (IOException e) {
			System.out.println("CACA");
		}
		System.out.println("TROLO");
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject = new JSONObject(stringBuilder.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsonObject;
	}
	
	public static GeoPoint getGeoPoint(JSONObject jsonObject) {

		double lon = 0;
		double lat = 0;

		try {

			lon = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
				.getJSONObject("geometry").getJSONObject("location")
				.getDouble("lng");

			lat = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
				.getJSONObject("geometry").getJSONObject("location")
				.getDouble("lat");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new GeoPoint((int) (lat * 1E6), (int) (lon * 1E6));

	}
}
