package com.pfa.Gendarmerie;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;

public class GendarmerieMainActivity extends FragmentActivity{
	
	Button bHome, bBrigade, bParametres, bAide;
	Fragment oldFragment;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        bHome = (Button)findViewById(R.id.home_button);
        bBrigade = (Button)findViewById(R.id.squad_button);
        bAide = (Button)findViewById(R.id.help_button);
       
        
        // get an instance of FragmentTransaction from your Activity 
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        //add a fragment 
        Home home = new Home();
        oldFragment = home;
        fragmentTransaction.add(R.id.main_fragment, home);
        fragmentTransaction.commit();
        
        bHome.setOnClickListener(btnFragmentOnClickListener);
        bBrigade.setOnClickListener(btnFragmentOnClickListener);
        bAide.setOnClickListener(btnFragmentOnClickListener);
    }
    
	 
    Button.OnClickListener btnFragmentOnClickListener
    = new Button.OnClickListener(){

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Fragment newFragment;
			
			// Create new fragment
			if(v == bHome){
				newFragment = new Home();
			}
			else if (v == bBrigade){
				newFragment = new Brigade();
			}
			else {
				newFragment = new Aide();
			}
			// Create new transaction
			FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

			// Replace whatever is in the fragment_container view with this fragment,
			// and add the transaction to the back stack
			transaction.replace(R.id.main_fragment, newFragment);
			transaction.addToBackStack(null);

			// Commit the transaction
			transaction.commit();
		}};
	   @Override
		  public void onBackPressed() {
		  Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.main_fragment);
		  Fragment subfragment = getSupportFragmentManager().findFragmentById(R.id.topfragment);
		  HomeContent hc = new HomeContent();
			   if (fragment.getClass() == oldFragment.getClass()) {
				   if (subfragment.getClass() == hc.getClass())
					   this.finish();
				   else
					   super.onBackPressed();
			   }
			   else
				   super.onBackPressed();	   
		}
		
}