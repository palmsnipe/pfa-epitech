package com.pfa.Gendarmerie;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class PagerAdapterPrevention extends PagerAdapter {
	 public int getCount() {
         return 3;
     }
	 ImageButton bRightArrow, bLeftArrow;
	 View coll;
     public Object instantiateItem(View collection, int position) {
    	 coll = collection;
         LayoutInflater inflater = (LayoutInflater) collection.getContext()
                 .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
     	 View view = inflater.inflate(R.layout.preventionswaplayout, null);
     	 TextView prefix = (TextView)view.findViewById(R.id.prevdom_prefix);
     	 String[] fullvalues = collection.getContext().getResources().getStringArray(R.array.msg_array);
     	 String[] values = null;
     	 bRightArrow = (ImageButton)view.findViewById(R.id.right_arrow_prev);
         bLeftArrow = (ImageButton)view.findViewById(R.id.left_arrow_prev);
     	 switch (position) {
         case 0:       	 
             prefix.setText("Prot�ger son domicile");
             bRightArrow.setImageResource(R.drawable.droite);
		     bLeftArrow.setImageResource(R.drawable.gauche_inactive);
             values = new String[7];
             for (int i = 0; i < 7; i++) {
           	  values[i] = fullvalues[i];
             }
        	  bRightArrow.setOnClickListener(new Button.OnClickListener() {
      	         public void onClick(View v) {
      	     		((ViewPager) coll).setCurrentItem(1);
      	         }
        	  }); 
             break;
         case 1:
             prefix.setText("Les bonnes pratiques");
	     	 bLeftArrow.setImageResource(R.drawable.gauche);
	     	 bRightArrow.setImageResource(R.drawable.droite);
             values = new String[19 - 7];
             for (int i = 7, j = 0; i < 19; i++, j++) {
           	  values[j] = fullvalues[i];
             }
       	  	bRightArrow.setOnClickListener(new Button.OnClickListener() {
   	        	public void onClick(View v) {
   	     			((ViewPager) coll).setCurrentItem(2); 	     		
   	         	}
       	  	});
          	bLeftArrow.setOnClickListener(new Button.OnClickListener() {
          		public void onClick(View v) {
      		     	((ViewPager) coll).setCurrentItem(0);
      	         }
         	 });
             break;
         case 2:
             prefix.setText("En cas d'absence durable");
         	 bLeftArrow.setImageResource(R.drawable.gauche);
    		 bRightArrow.setImageResource(R.drawable.droite_inactive);
             values = new String[25  - 19];
             for (int i = 19, j = 0; i < 25; i++, j++) {
           	  values[j] = fullvalues[i];
             }
           	  bLeftArrow.setOnClickListener(new Button.OnClickListener() {
      	         public void onClick(View v) {
      		     	((ViewPager) coll).setCurrentItem(1);
      	         }
         	  });
             break;
         }
         
      ArrayAdapter<String> files = new ArrayAdapter<String>(collection.getContext(), 
    		  R.layout.customlayoutlist, 
                  values);
      ListView domlist = (ListView) view.findViewById(R.id.prevdom_list);
      domlist.setAdapter(files);
     
      ((ViewPager) collection).addView(view, 0);
      return view;
     }
     @Override
     public void destroyItem(View arg0, int arg1, Object arg2) {
         ((ViewPager) arg0).removeView((View) arg2);
     }
     @Override
     public boolean isViewFromObject(View arg0, Object arg1) {
         return arg0 == ((View) arg1);
     }
     @Override
     public Parcelable saveState() {
         return null;
     }
}
