package com.pfa.Gendarmerie;

import com.pfa.Gendarmerie.R;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.ToggleButton;

public class Aide  extends Fragment {
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
          Bundle savedInstanceState) {
         // TODO Auto-generated method stub
         super.onCreate(savedInstanceState);
         View aideView = inflater.inflate(R.layout.aidelayout, container, false);
		
    	 final ToggleButton b2ML = (ToggleButton) aideView.findViewById(R.id.Button2AideML);
    	 final View tML = aideView.findViewById(R.id.TextAideML);
         
    	 final ToggleButton b2IL = (ToggleButton) aideView.findViewById(R.id.Button2AideInformationsLegale);
    	 final View tIL = aideView.findViewById(R.id.TextAideInformationsLegale);
    	  
    	 final ToggleButton b2OTV = (ToggleButton) aideView.findViewById(R.id.Button2AideOTV);
    	 final View tOTV = aideView.findViewById(R.id.AideAccueil);
    	  
    	 final ToggleButton b2MB = (ToggleButton) aideView.findViewById(R.id.Button2AideBrigade);
    	 final View tMB = aideView.findViewById(R.id.TextAideBrigade);
    	  
    	 final ToggleButton b2P = (ToggleButton) aideView.findViewById(R.id.Button2AidePrevention);
    	 final View tP = aideView.findViewById(R.id.TextAidePrevention);
    	  
    	final ToggleButton b2A = (ToggleButton) aideView.findViewById(R.id.Button2AideAlerte17);
    	final View tA = aideView.findViewById(R.id.TextAideAlerte17);
    	  
    	final ToggleButton b2AP = (ToggleButton) aideView.findViewById(R.id.Button2AideAPropos);
    	final View tAP = aideView.findViewById(R.id.TextAideAPropos);
    	
    	LinearLayout aideMentions = (LinearLayout)aideView.findViewById(R.id.aide_mention);
    	LinearLayout aidePresentation = (LinearLayout)aideView.findViewById(R.id.aide_presentation);
    	LinearLayout aideAccueil = (LinearLayout)aideView.findViewById(R.id.aide_accueil);
    	LinearLayout aidePrevention = (LinearLayout)aideView.findViewById(R.id.aide_prevention);
    	LinearLayout aideAlerte = (LinearLayout)aideView.findViewById(R.id.aide_alerte);
    	LinearLayout aideDepart = (LinearLayout)aideView.findViewById(R.id.aide_depart);
    	LinearLayout aideBrigade = (LinearLayout)aideView.findViewById(R.id.aide_brigade);
    	
    	aideMentions.setOnClickListener(new OnClickListener()
			{	 
		  			public void onClick(View v)
		  			{
		  				if (tML.getVisibility() == android.view.View.GONE)
		  				{tML.setVisibility(android.view.View.VISIBLE);
		  				b2ML.setBackgroundResource(R.drawable.toggle_down);}
		  				else
		  				{tML.setVisibility(android.view.View.GONE);
		  				b2ML.setBackgroundResource(R.drawable.toggle_right);}
		  			}
			  });
	  b2ML.setOnClickListener(new OnClickListener()
	  {	 
			public void onClick(View v)
			{
				if (tML.getVisibility() == android.view.View.GONE)
				{tML.setVisibility(android.view.View.VISIBLE);
				b2ML.setBackgroundResource(R.drawable.toggle_down);}
				else
				{tML.setVisibility(android.view.View.GONE);
				b2ML.setBackgroundResource(R.drawable.toggle_right);}
			}
	  });
    	aidePresentation.setOnClickListener(new OnClickListener()
			{	 
		  			public void onClick(View v)
		  			{
		  				if (tIL.getVisibility() == android.view.View.GONE)
		  				{tIL.setVisibility(android.view.View.VISIBLE);
		  				b2IL.setBackgroundResource(R.drawable.toggle_down);}
		  				else
		  				{tIL.setVisibility(android.view.View.GONE);
		  				b2IL.setBackgroundResource(R.drawable.toggle_right);}
		  			}
			  });
	  b2IL.setOnClickListener(new OnClickListener()
	  {	 
			public void onClick(View v)
			{
				if (tIL.getVisibility() == android.view.View.GONE)
				{tIL.setVisibility(android.view.View.VISIBLE);
				b2IL.setBackgroundResource(R.drawable.toggle_down);}
				else
				{tIL.setVisibility(android.view.View.GONE);
				b2IL.setBackgroundResource(R.drawable.toggle_right);}
			}
	  });
	  aideAccueil.setOnClickListener(new OnClickListener()
	  {	 
			public void onClick(View v)
			{
				if (tOTV.getVisibility() == android.view.View.GONE)
				{tOTV.setVisibility(android.view.View.VISIBLE);
				b2OTV.setBackgroundResource(R.drawable.toggle_down);}
				else
				{tOTV.setVisibility(android.view.View.GONE);
				b2OTV.setBackgroundResource(R.drawable.toggle_right);}
			}
	  });
	  b2OTV.setOnClickListener(new OnClickListener()
	  {	 
		public void onClick(View v)
		{
			if (tOTV.getVisibility() == android.view.View.GONE)
			{tOTV.setVisibility(android.view.View.VISIBLE);
			b2OTV.setBackgroundResource(R.drawable.toggle_down);}
			else
			{tOTV.setVisibility(android.view.View.GONE);
			b2OTV.setBackgroundResource(R.drawable.toggle_right);}
		}
	  });
	  aidePrevention.setOnClickListener(new OnClickListener()
	  {	 
			public void onClick(View v)
			{
				if (tMB.getVisibility() == android.view.View.GONE)
				{tMB.setVisibility(android.view.View.VISIBLE);
				b2MB.setBackgroundResource(R.drawable.toggle_down);}
				else
				{tMB.setVisibility(android.view.View.GONE);
				b2MB.setBackgroundResource(R.drawable.toggle_right);}
			}
	  });
	  b2MB.setOnClickListener(new OnClickListener()
	  {	 
		public void onClick(View v)
		{
			if (tMB.getVisibility() == android.view.View.GONE)
			{tMB.setVisibility(android.view.View.VISIBLE);
			b2MB.setBackgroundResource(R.drawable.toggle_down);}
			else
			{tMB.setVisibility(android.view.View.GONE);
			b2MB.setBackgroundResource(R.drawable.toggle_right);}
		}
	  });
	  aideAlerte.setOnClickListener(new OnClickListener()
	  {	 
			public void onClick(View v)
			{
				if (tP.getVisibility() == android.view.View.GONE)
				{tP.setVisibility(android.view.View.VISIBLE);
				b2P.setBackgroundResource(R.drawable.toggle_down);}
				else
				{tP.setVisibility(android.view.View.GONE);
				b2P.setBackgroundResource(R.drawable.toggle_right);}
			}
	  });
	  b2P.setOnClickListener(new OnClickListener()
	  {	 
		public void onClick(View v)
		{
			if (tP.getVisibility() == android.view.View.GONE)
			{tP.setVisibility(android.view.View.VISIBLE);
			b2P.setBackgroundResource(R.drawable.toggle_down);}
			else
			{tP.setVisibility(android.view.View.GONE);
			b2P.setBackgroundResource(R.drawable.toggle_right);}
		}
	  });
	  aideDepart.setOnClickListener(new OnClickListener()
	  {	 
			public void onClick(View v)
			{
				if (tA.getVisibility() == android.view.View.GONE)
				{tA.setVisibility(android.view.View.VISIBLE);
				b2A.setBackgroundResource(R.drawable.toggle_down);}
				else
				{tA.setVisibility(android.view.View.GONE);
				b2A.setBackgroundResource(R.drawable.toggle_right);}
			}
	  });
	  b2A.setOnClickListener(new OnClickListener()
	  {	 
		public void onClick(View v)
		{
			if (tA.getVisibility() == android.view.View.GONE)
			{tA.setVisibility(android.view.View.VISIBLE);
			b2A.setBackgroundResource(R.drawable.toggle_down);}
			else
			{tA.setVisibility(android.view.View.GONE);
			b2A.setBackgroundResource(R.drawable.toggle_right);}
		}
	  });
	  aideBrigade.setOnClickListener(new OnClickListener()
	  {	 
			public void onClick(View v)
			{
				if (tAP.getVisibility() == android.view.View.GONE)
				{tAP.setVisibility(android.view.View.VISIBLE);
				b2AP.setBackgroundResource(R.drawable.toggle_down);}
				else
				{tAP.setVisibility(android.view.View.GONE);
				b2AP.setBackgroundResource(R.drawable.toggle_right);}
			}
	  });
	  b2AP.setOnClickListener(new OnClickListener()
	  {	 
		public void onClick(View v)
		{
			if (tAP.getVisibility() == android.view.View.GONE)
			{tAP.setVisibility(android.view.View.VISIBLE);
			b2AP.setBackgroundResource(R.drawable.toggle_down);}
			else
			{tAP.setVisibility(android.view.View.GONE);
			b2AP.setBackgroundResource(R.drawable.toggle_right);}
		}
	  });  	
		
	    return aideView;
	 }	
}
