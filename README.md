# Projet de Fin d'Année Epitech

Dans le cadre du PFA de troisième année de l'Epitech, un partenariat a été mis en place avec la gendarmerie nationale. Le but de ce partenariat est de développer une application de la gendarmerie multi-plateforme.

## Liens utiles

  - Dossier partagé : [SkyDrive][1]
  - Agenda du groupe : [Calendar][2]

#### EnyoJS

  - Documentation : [API][3]
  - Mise en situation des éléments : [Sampler][4]

## Organisation du projet
EnyoJS contient les sources pour le développement de l'application multi-plateforme (iOS, ...)

## Membres du projet
  - Pierre Jacquel
  - Gaëlle Maurieres
  - Cyril Morales
  - Emmanuel NGoran
  - Florian Picot


[1]:http://groups.live.com/PFAEpitech/
[2]:https://www.google.com/calendar/render?tab=cc
[3]:http://enyojs.com/api
[4]:http://enyojs.com/sampler