﻿var bddBrigades = {
"brigades" : [{"id":1,"nom":"Capestang","compagnie":1,"type":1,"cob":null,"adresse":"33 avenue de la République","telephone":"04 67 93 30 31","fax":""},
{"id":2,"nom":"Olonzac","compagnie":1,"type":1,"cob":null,"adresse":"Avenue d'Homps","telephone":"04 68 91 20 17","fax":""},
{"id":4,"nom":"Bedarieux","compagnie":1,"type":3,"cob":1,"adresse":"Caserne Canitrot route de Saint-Pons","telephone":"04 67 95 09 29","fax":"04 67 95 86 74"},
{"id":5,"nom":"St Gervais sur Mare","compagnie":1,"type":3,"cob":1,"adresse":"16 avenue des Treilles","telephone":"04 67 23 60 25","fax":""},
{"id":6,"nom":"Le Bousquet d'Orb","compagnie":1,"type":3,"cob":1,"adresse":"39 allée Jean Bringer","telephone":"04 67 23 80 28","fax":"04 67 23 96 53"},
{"id":7,"nom":"Murviel les Béziers","compagnie":1,"type":3,"cob":2,"adresse":"23 avenue de la République","telephone":"04 67 37 83 99","fax":""},
{"id":8,"nom":"St Chinian","compagnie":1,"type":3,"cob":2,"adresse":"7 rue de l'Evêque","telephone":"04 67 38 00 13","fax":"04 67 38 00 90"},
{"id":9,"nom":"St Pons de Thomières","compagnie":1,"type":3,"cob":3,"adresse":"18 avenue de la Gare","telephone":"04 67 97 01 12","fax":"04 67 97 91 28"},
{"id":10,"nom":"La Salvetat sur Agout","compagnie":1,"type":3,"cob":3,"adresse":"Allée Saint-Etienne-de-Cavall","telephone":"04 67 97 60 42","fax":"04 67 97 64 78"},
{"id":11,"nom":"Olargues","compagnie":1,"type":3,"cob":3,"adresse":"Route de Saint-Pons","telephone":"04 67 97 70 01","fax":"04 67 97 69 57"},
{"id":12,"nom":"Valras Plage","compagnie":1,"type":3,"cob":4,"adresse":"Chemin des Cosses-sous-la-Tour","telephone":"04 67 32 02 53","fax":""},
{"id":13,"nom":"Béziers","compagnie":1,"type":3,"cob":4,"adresse":"14 boulevard du Maréchal Leclerc","telephone":"04 67 35 17 17","fax":""},
{"id":14,"nom":"Castelnau le Lez","compagnie":2,"type":1,"cob":null,"adresse":"635 avenue de la Monnaie","telephone":"04 99 74 29 50","fax":"04 99 74 29 64"},
{"id":15,"nom":"Jacou/ Clapiers","compagnie":2,"type":1,"cob":null,"adresse":"10 avenue de l'Europe","telephone":"04 99 63 68 50","fax":"04 99 63 68 51"},
{"id":16,"nom":"Montpellier","compagnie":2,"type":1,"cob":null,"adresse":"Caserne Lepic 359 rue de Font-Couverte","telephone":"04 99 53 59 34","fax":"04 99 53 59 35"},
{"id":17,"nom":"Palavas les Flots","compagnie":2,"type":1,"cob":null,"adresse":"1 rue de la Tramontane","telephone":"04 67 07 01 20","fax":"04 67 07 01 29"},
{"id":18,"nom":"St Gely du Fesc","compagnie":2,"type":1,"cob":null,"adresse":"458 rue du Devois","telephone":"04 67 91 73 00","fax":"04 67 91 73 09"},
{"id":19,"nom":"St Georges d'Orques","compagnie":2,"type":1,"cob":null,"adresse":"5 avenue de l'Occitanie","telephone":"04 67 75 18 99","fax":"04 67 75 84 17"},
{"id":20,"nom":"Saint Jean de Vedas","compagnie":2,"type":1,"cob":null,"adresse":"330 avenue de Librilla","telephone":"04 67 99 45 70","fax":"04 67 99 45 79"},
{"id":21,"nom":"Saint Mathieu de Treviers","compagnie":2,"type":1,"cob":null,"adresse":"Avenue de Montpellier","telephone":"04 67 55 20 02","fax":"04 67 55 17 35"},
{"id":22,"nom":"Villeneuve les Maguelone","compagnie":2,"type":1,"cob":null,"adresse":"364 avenue de la Gare","telephone":"04 67 69 52 69","fax":""},
{"id":23,"nom":"Ganges","compagnie":3,"type":1,"cob":null,"adresse":"1 place Joseph Boudouresques","telephone":"04 67 73 85 13","fax":"04 67 73 75 98"},
{"id":24,"nom":"Saint Martin de Londres","compagnie":3,"type":1,"cob":null,"adresse":"240 avenue des Cévennes","telephone":"04 67 55 00 01","fax":"04 67 55 05 54"},
{"id":25,"nom":"Clermont l'Hérault","compagnie":3,"type":3,"cob":5,"adresse":"8 rue Jean Moulin","telephone":"04 67 96 00 32","fax":"04 67 96 99 53"},
{"id":26,"nom":"Paulhan","compagnie":3,"type":3,"cob":5,"adresse":"78 cours National","telephone":"04 67 25 00 09","fax":"04 67 25 21 77"},
{"id":27,"nom":"Aniane","compagnie":3,"type":3,"cob":6,"adresse":"31 rue Jean Castéran","telephone":"04 67 57 70 20","fax":"04 67 57 02 72"},
{"id":28,"nom":"Gignac","compagnie":3,"type":3,"cob":6,"adresse":"576 chemin Sainte-Claire Roqueyrol","telephone":"04 67 57 50 05","fax":"04 67 57 04 48"},
{"id":29,"nom":"Le Caylar","compagnie":3,"type":3,"cob":7,"adresse":"163 avenue de Lodève","telephone":"04 67 44 50 01","fax":""},
{"id":30,"nom":"Lodève","compagnie":3,"type":3,"cob":7,"adresse":"160 boulevard du Général Leclerc","telephone":"04 67 44 00 25","fax":"04 67 44 91 14"},
{"id":31,"nom":"Castries","compagnie":4,"type":1,"cob":null,"adresse":"18 avenue du Moulin-à-Vent","telephone":"04 67 70 03 31","fax":"04 67 70 88 60"},
{"id":32,"nom":"La Grande Motte","compagnie":4,"type":1,"cob":null,"adresse":"Avenue Melgueil","telephone":"04 67 56 50 29","fax":""},
{"id":33,"nom":"Lunel","compagnie":4,"type":1,"cob":null,"adresse":"Caserne Vauban 171 avenue du Général de Gaulle","telephone":"04 67 83 06 23","fax":"04 67 83 47 84"},
{"id":34,"nom":"Mauguio","compagnie":4,"type":1,"cob":null,"adresse":"Rue Jean-Sébastien Bach","telephone":"04 67 29 30 04","fax":"04 67 29 85 72"},
{"id":35,"nom":"Mèze","compagnie":5,"type":1,"cob":null,"adresse":"1 chemin de Cagueloup","telephone":"04 67 43 80 11","fax":"04 67 43 82 85"},
{"id":36,"nom":"Agde","compagnie":5,"type":3,"cob":8,"adresse":"80 chemin de Janin","telephone":"04 67 21 10 29","fax":"04 67 21 34 56"},
{"id":37,"nom":"Florensac","compagnie":5,"type":3,"cob":8,"adresse":"Rue des Coquelicots","telephone":"04 67 77 90 48","fax":"04 67 77 90 16"},
{"id":38,"nom":"Gigean","compagnie":5,"type":3,"cob":9,"adresse":"17 avenue de Montpellier","telephone":"04 67 78 72 66","fax":""},
{"id":39,"nom":"Sète","compagnie":5,"type":3,"cob":9,"adresse":"17 avenue de Montpellier","telephone":"04 67 78 72 66","fax":"04 67 78 90 58"},
{"id":40,"nom":"Montagnac","compagnie":5,"type":3,"cob":10,"adresse":"20 rue Aspirant Lebaron","telephone":"04 67 24 06 10","fax":"04 67 24 81 85"},
{"id":41,"nom":"Pézenas","compagnie":5,"type":3,"cob":10,"adresse":"12 avenue de Plaisance","telephone":"04 67 98 13 65","fax":"04 67 98 98 56"},
{"id":42,"nom":"Roujan","compagnie":5,"type":3,"cob":11,"adresse":"Avenue de Caux","telephone":"04 67 24 60 13","fax":""},
{"id":43,"nom":"Servian","compagnie":5,"type":3,"cob":11,"adresse":"Route de La Roque","telephone":"04 67 39 10 20","fax":""},
{"id":44,"nom":"Police","compagnie":null,"type":4,"cob":null,"adresse":"","telephone":"","fax":""},
{"id":45,"nom":"Béziers","compagnie":null,"type":4,"cob":null,"adresse":"Ciat de BEZIERS, Boulevard Edouard Herriot","telephone":"04 67 49 54 00","fax":""},
{"id":46,"nom":"Agde","compagnie":null,"type":4,"cob":null,"adresse":"Ciat d'Agde, Avenue du Général de Gaulle","telephone":"04 67 01 02 00","fax":""},
{"id":47,"nom":"Cap d'Agde","compagnie":null,"type":4,"cob":null,"adresse":"Ciat Cap d'Agde, Avenue des sergents","telephone":"04 67 00 14 21","fax":""},
{"id":48,"nom":"Sète","compagnie":null,"type":4,"cob":null,"adresse":"Ciat de Sète, 50 quai bosc","telephone":"04 67 46 80 22","fax":""},
{"id":49,"nom":"Frontignan","compagnie":null,"type":4,"cob":null,"adresse":"Poste de Frontignan, 1 Avenue Fréderic Mistral (saison estivale)","telephone":"04 67 18 49 30","fax":""},
{"id":50,"nom":"Lattes","compagnie":null,"type":4,"cob":null,"adresse":"Ciat de Lattes, 1 Avenue de l'Agau","telephone":"04 99 13 67 00","fax":""},
{"id":51,"nom":"Montpellier","compagnie":null,"type":4,"cob":null,"adresse":"206 avenue du Comte de Melgueil","telephone":"04 99 13 50 00","fax":""}],
"groupe" : [{"id":1,"nom":"Bédarieux","compagnie":1},
{"id":2,"nom":"Murviel Les Béziers","compagnie":1},
{"id":3,"nom":"Saint Pons de Thomières","compagnie":1},
{"id":4,"nom":"Valras","compagnie":1},
{"id":5,"nom":"Clermont l'Hérault","compagnie":3},
{"id":6,"nom":"Gignac","compagnie":3},
{"id":7,"nom":"Lodève","compagnie":3},
{"id":8,"nom":"Agde","compagnie":5},
{"id":9,"nom":"Gigean","compagnie":5},
{"id":10,"nom":"Pézenas","compagnie":5},
{"id":11,"nom":"Servian","compagnie":5}],
"type" : [{"id":1,"nom":"BTA","detail":"Brigade Territoriale Autonome"},
{"id":3,"nom":"BTP","detail":"Brigade Territoriale de Proximité"},
{"id":4,"nom":"ZPN","detail":"Zone Police Nationale"}],
"compagnies" : [{"id":1,"nom":"Beziers"},
{"id":2,"nom":"Castelnau Le Lez"},
{"id":3,"nom":"Lodève"},
{"id":4,"nom":"Lunel"},
{"id":5,"nom":"Pézenas"}],
"villes" : [{"id":1,"nom":"Capestang","brigade":1,"CP":34310},
{"id":2,"nom":"Colombiers","brigade":1,"CP":34410},
{"id":3,"nom":"Creissan","brigade":1,"CP":34370},
{"id":4,"nom":"Maureilhan","brigade":1,"CP":34370},
{"id":5,"nom":"Montady","brigade":1,"CP":34310},
{"id":6,"nom":"Montels","brigade":1,"CP":34310},
{"id":7,"nom":"Nissan lez Enserune","brigade":1,"CP":34440},
{"id":8,"nom":"Poilhes","brigade":1,"CP":34310},
{"id":9,"nom":"Puisserguiers","brigade":1,"CP":34620},
{"id":10,"nom":"Quarante","brigade":1,"CP":34310},
{"id":11,"nom":"Aigne","brigade":2,"CP":34210},
{"id":12,"nom":"Azillanet","brigade":2,"CP":34210},
{"id":13,"nom":"Beaufort","brigade":2,"CP":34210},
{"id":14,"nom":"Cassagnoles","brigade":2,"CP":34210},
{"id":15,"nom":"Cesseras","brigade":2,"CP":34210},
{"id":16,"nom":"Felines Minervois","brigade":2,"CP":34210},
{"id":17,"nom":"Ferrals les Montagnes","brigade":2,"CP":34210},
{"id":18,"nom":"La Caunette","brigade":2,"CP":34210},
{"id":19,"nom":"La Livinière","brigade":2,"CP":34210},
{"id":20,"nom":"Minerve","brigade":2,"CP":34210},
{"id":21,"nom":"Olonzac","brigade":2,"CP":34210},
{"id":22,"nom":"Oupia","brigade":2,"CP":34210},
{"id":23,"nom":"Siran","brigade":2,"CP":34210},
{"id":24,"nom":"Bedarieux","brigade":4,"CP":34600},
{"id":25,"nom":"Camplong","brigade":4,"CP":34260},
{"id":26,"nom":"Carlencas et Levas","brigade":4,"CP":34600},
{"id":27,"nom":"Faugeres","brigade":4,"CP":34600},
{"id":28,"nom":"Graissessac","brigade":4,"CP":34260},
{"id":29,"nom":"Herepian","brigade":4,"CP":34600},
{"id":30,"nom":"La Tour sur Orb","brigade":4,"CP":34260},
{"id":31,"nom":"Le Pradal","brigade":4,"CP":34600},
{"id":32,"nom":"Pezennes les Mines","brigade":4,"CP":34600},
{"id":33,"nom":"Saint Etienne Estrechoux","brigade":4,"CP":34260},
{"id":34,"nom":"Villemagne l'Argentière","brigade":4,"CP":34600},
{"id":35,"nom":"Castanet le Haut","brigade":5,"CP":34610},
{"id":36,"nom":"Combes","brigade":5,"CP":34240},
{"id":37,"nom":"Lamalou les Bains","brigade":5,"CP":34240},
{"id":38,"nom":"Le Poujol sur Orb","brigade":5,"CP":34600},
{"id":39,"nom":"Les Aires","brigade":5,"CP":34600},
{"id":40,"nom":"Rosis","brigade":5,"CP":34610},
{"id":41,"nom":"Saint Genies de Varensal","brigade":5,"CP":34610},
{"id":42,"nom":"Saint Gervais sur Mare","brigade":5,"CP":34610},
{"id":43,"nom":"Taussac la Billière","brigade":5,"CP":34600},
{"id":44,"nom":"Avene","brigade":6,"CP":34260},
{"id":45,"nom":"Brenas","brigade":6,"CP":34650},
{"id":46,"nom":"Ceihes et Rocozels","brigade":6,"CP":34260},
{"id":47,"nom":"Dio et Valquieres","brigade":6,"CP":34650},
{"id":48,"nom":"Joncels","brigade":6,"CP":34650},
{"id":49,"nom":"Le Bousquet d'Orb","brigade":6,"CP":34260},
{"id":50,"nom":"Lunas","brigade":6,"CP":34650},
{"id":51,"nom":"Autignac","brigade":7,"CP":34480},
{"id":52,"nom":"Cabrerolles","brigade":7,"CP":34480},
{"id":53,"nom":"Causses et Veyran","brigade":7,"CP":34490},
{"id":54,"nom":"Caussiniojouls","brigade":7,"CP":34600},
{"id":55,"nom":"Cazouls les Béziers","brigade":7,"CP":34370},
{"id":56,"nom":"Corneilhan","brigade":7,"CP":34490},
{"id":57,"nom":"Laurens","brigade":7,"CP":34480},
{"id":58,"nom":"Lignan sur Orb","brigade":7,"CP":34490},
{"id":59,"nom":"Maraussan","brigade":7,"CP":34370},
{"id":60,"nom":"Murviel les Béziers","brigade":7,"CP":34490},
{"id":61,"nom":"Pailhes","brigade":7,"CP":34490},
{"id":62,"nom":"Puimisson","brigade":7,"CP":34480},
{"id":63,"nom":"Saint Genies de Fontedit","brigade":7,"CP":34480},
{"id":64,"nom":"Saint Nazaire de Ladarez","brigade":7,"CP":34490},
{"id":65,"nom":"Thezan les Béziers","brigade":7,"CP":34490},
{"id":66,"nom":"Agel","brigade":8,"CP":34210},
{"id":67,"nom":"Aigues vives","brigade":8,"CP":34210},
{"id":68,"nom":"Assignan","brigade":8,"CP":34360},
{"id":69,"nom":"Babeau Bouldoux","brigade":8,"CP":34360},
{"id":70,"nom":"Cazedarnes","brigade":8,"CP":34460},
{"id":71,"nom":"Cruzy","brigade":8,"CP":34310},
{"id":72,"nom":"Ferrieres Poussarou","brigade":8,"CP":34360},
{"id":73,"nom":"Montouliers","brigade":8,"CP":34310},
{"id":74,"nom":"Pierrerue","brigade":8,"CP":34360},
{"id":75,"nom":"Prades sur Vernazobre","brigade":8,"CP":34360},
{"id":76,"nom":"Saint Chinian","brigade":8,"CP":34360},
{"id":77,"nom":"Villespassans","brigade":8,"CP":34360},
{"id":78,"nom":"Boisset","brigade":9,"CP":34220},
{"id":79,"nom":"Courniou","brigade":9,"CP":34220},
{"id":80,"nom":"Pardailhan","brigade":9,"CP":34360},
{"id":81,"nom":"Rieussec","brigade":9,"CP":34220},
{"id":82,"nom":"Riols","brigade":9,"CP":34220},
{"id":83,"nom":"Saint Jean de Minervois","brigade":9,"CP":34360},
{"id":84,"nom":"Saint Pons de Thomières","brigade":9,"CP":34220},
{"id":85,"nom":"Velieux","brigade":9,"CP":34220},
{"id":86,"nom":"Verreries de Moussans","brigade":9,"CP":34220},
{"id":87,"nom":"Cambon et Salvergues","brigade":10,"CP":34330},
{"id":88,"nom":"Fraisse sur Agout","brigade":10,"CP":34330},
{"id":89,"nom":"La Salvetat sur Agout","brigade":10,"CP":34330},
{"id":90,"nom":"Le Soulie","brigade":10,"CP":34330},
{"id":91,"nom":"Berlou","brigade":11,"CP":34360},
{"id":92,"nom":"Colombières sur Orb","brigade":11,"CP":34390},
{"id":93,"nom":"Mons","brigade":11,"CP":34390},
{"id":94,"nom":"Olargues","brigade":11,"CP":34390},
{"id":95,"nom":"Premian\nPremian","brigade":11,"CP":34390},
{"id":96,"nom":"Roquebrun","brigade":11,"CP":34460},
{"id":97,"nom":"Saint Etienne d'Albagnan","brigade":11,"CP":34390},
{"id":98,"nom":"Saint Julien","brigade":11,"CP":34390},
{"id":99,"nom":"Saint Martin de l'Arcon","brigade":11,"CP":34390},
{"id":100,"nom":"Saint Vincent d'Olargues","brigade":11,"CP":34390},
{"id":101,"nom":"Vieussan","brigade":11,"CP":34390},
{"id":102,"nom":"Lespignan","brigade":12,"CP":34710},
{"id":103,"nom":"Portiragnes","brigade":12,"CP":34420},
{"id":104,"nom":"Sauvian","brigade":12,"CP":34410},
{"id":105,"nom":"Serignan","brigade":12,"CP":34410},
{"id":106,"nom":"Valras-Plage","brigade":12,"CP":34350},
{"id":107,"nom":"Vendres","brigade":12,"CP":34350},
{"id":108,"nom":"Castelnau le Lez","brigade":14,"CP":34170},
{"id":109,"nom":"Le Cres","brigade":14,"CP":34920},
{"id":110,"nom":"Assas","brigade":15,"CP":34820},
{"id":111,"nom":"Clapiers","brigade":15,"CP":34830},
{"id":112,"nom":"Guzargues","brigade":15,"CP":34820},
{"id":113,"nom":"Jacou","brigade":15,"CP":34830},
{"id":114,"nom":"Montferrier sur Lez","brigade":15,"CP":34980},
{"id":115,"nom":"Prades le Lez","brigade":15,"CP":34730},
{"id":116,"nom":"Saint Vincent de Barbeyargues","brigade":15,"CP":34730},
{"id":117,"nom":"Teyran","brigade":15,"CP":34820},
{"id":118,"nom":"Palavas les Flots","brigade":17,"CP":34250},
{"id":119,"nom":"Carnon (Mauguio)","brigade":17,"CP":34130},
{"id":120,"nom":"Combaillaux","brigade":18,"CP":34980},
{"id":121,"nom":"Grabels","brigade":18,"CP":34790},
{"id":122,"nom":"Les Matelles","brigade":18,"CP":34270},
{"id":123,"nom":"Murles","brigade":18,"CP":34980},
{"id":124,"nom":"Saint Clément de Rivière","brigade":18,"CP":34980},
{"id":125,"nom":"Saint Gely de Fesc","brigade":18,"CP":34480},
{"id":126,"nom":"Vailhauques","brigade":18,"CP":34750},
{"id":127,"nom":"Juvignac","brigade":19,"CP":34990},
{"id":128,"nom":"Laverune","brigade":19,"CP":34880},
{"id":129,"nom":"Montarnaud","brigade":19,"CP":34570},
{"id":130,"nom":"Murviel les Montpellier","brigade":19,"CP":34570},
{"id":131,"nom":"Pignan","brigade":19,"CP":34570},
{"id":132,"nom":"Saint Georges d'Orques","brigade":19,"CP":34680},
{"id":133,"nom":"Saint Paul et Valmalle","brigade":19,"CP":34570},
{"id":134,"nom":"Fabregues","brigade":20,"CP":34690},
{"id":135,"nom":"Saussan","brigade":20,"CP":34570},
{"id":136,"nom":"Saint Jean de Vedas","brigade":20,"CP":34430},
{"id":137,"nom":"Buzignargues","brigade":21,"CP":34160},
{"id":138,"nom":"Campagne","brigade":21,"CP":34160},
{"id":139,"nom":"Cazevieille","brigade":21,"CP":34270},
{"id":140,"nom":"Claret","brigade":21,"CP":34270},
{"id":141,"nom":"Fontanes","brigade":21,"CP":34270},
{"id":142,"nom":"Galargues","brigade":21,"CP":34160},
{"id":143,"nom":"Garrigues","brigade":21,"CP":34160},
{"id":144,"nom":"Lauret","brigade":21,"CP":34270},
{"id":145,"nom":"Le Triadou","brigade":21,"CP":34270},
{"id":146,"nom":"Montaud","brigade":21,"CP":34160},
{"id":147,"nom":"Sauteyrargues","brigade":21,"CP":34270},
{"id":148,"nom":"Saint Bauzille de Montmel","brigade":21,"CP":34160},
{"id":149,"nom":"Saint Hilaire de Beauvoir","brigade":21,"CP":34160},
{"id":150,"nom":"Saint Jean de Cornies","brigade":21,"CP":34160},
{"id":151,"nom":"Saint Jean de Cuculles","brigade":21,"CP":34270},
{"id":152,"nom":"Saint Mathieu de Treviers","brigade":21,"CP":34270},
{"id":153,"nom":"Saint Croix de Quintillargues","brigade":21,"CP":34270},
{"id":154,"nom":"Vacquieres","brigade":21,"CP":34270},
{"id":155,"nom":"Valflaunes","brigade":21,"CP":34270},
{"id":156,"nom":"Mireval","brigade":22,"CP":34110},
{"id":157,"nom":"Vic la Gardiole","brigade":22,"CP":34110},
{"id":158,"nom":"Villeneuve les Maguelone","brigade":22,"CP":34750},
{"id":159,"nom":"Agones","brigade":23,"CP":34190},
{"id":160,"nom":"Brissac","brigade":23,"CP":34190},
{"id":161,"nom":"Cazilhac","brigade":23,"CP":34190},
{"id":162,"nom":"Ganges","brigade":23,"CP":34190},
{"id":163,"nom":"Gornies","brigade":23,"CP":34190},
{"id":164,"nom":"Laroque","brigade":23,"CP":34190},
{"id":165,"nom":"Montoulieu","brigade":23,"CP":34190},
{"id":166,"nom":"Moules et Baucels","brigade":23,"CP":34190},
{"id":167,"nom":"Saint Bauzille de Putois","brigade":23,"CP":34190},
{"id":168,"nom":"Causse de la Selle","brigade":24,"CP":34380},
{"id":169,"nom":"Ferrieres les Verreries","brigade":24,"CP":34190},
{"id":170,"nom":"Mas de Londres","brigade":24,"CP":34380},
{"id":171,"nom":"Notre Dame de Londres","brigade":24,"CP":34380},
{"id":172,"nom":"Rouet","brigade":24,"CP":34380},
{"id":173,"nom":"Saint André de Bueges","brigade":24,"CP":34390},
{"id":174,"nom":"Saint Jean de Buèges","brigade":24,"CP":34380},
{"id":175,"nom":"Saint Martin de Londres","brigade":24,"CP":34380},
{"id":176,"nom":"Viols en Laval","brigade":24,"CP":34380},
{"id":177,"nom":"Viols le Fort","brigade":24,"CP":34380},
{"id":178,"nom":"Aspiran","brigade":25,"CP":34800},
{"id":179,"nom":"Brignac","brigade":25,"CP":34800},
{"id":180,"nom":"Cabrières","brigade":25,"CP":34800},
{"id":181,"nom":"Canet","brigade":25,"CP":34800},
{"id":182,"nom":"Ceyras","brigade":25,"CP":34800},
{"id":183,"nom":"Clermont l'Hérault","brigade":25,"CP":34800},
{"id":184,"nom":"Lacoste","brigade":25,"CP":34800},
{"id":185,"nom":"Liausson","brigade":25,"CP":34800},
{"id":186,"nom":"Lieuran Cabrières","brigade":25,"CP":34800},
{"id":187,"nom":"Merifons","brigade":25,"CP":34800},
{"id":188,"nom":"Moureze","brigade":25,"CP":34800},
{"id":189,"nom":"Nebian","brigade":25,"CP":34800},
{"id":190,"nom":"Peret","brigade":25,"CP":34800},
{"id":191,"nom":"Salasc","brigade":25,"CP":34800},
{"id":192,"nom":"Saint Felix de Lodez","brigade":25,"CP":34725},
{"id":193,"nom":"Valmascle","brigade":25,"CP":34800},
{"id":194,"nom":"Villeneuvette","brigade":25,"CP":34800},
{"id":195,"nom":"Belarga","brigade":26,"CP":34230},
{"id":196,"nom":"Campagnan","brigade":26,"CP":34230},
{"id":197,"nom":"Le Pouget","brigade":26,"CP":34230},
{"id":198,"nom":"Paulhan","brigade":26,"CP":34230},
{"id":199,"nom":"Plaissan","brigade":26,"CP":34230},
{"id":200,"nom":"Puilacher","brigade":26,"CP":34230},
{"id":201,"nom":"Saint Pargoire","brigade":26,"CP":34230},
{"id":202,"nom":"Tressan","brigade":26,"CP":34230},
{"id":203,"nom":"Aniane","brigade":27,"CP":34150},
{"id":204,"nom":"Argelliers","brigade":27,"CP":34380},
{"id":205,"nom":"La Boissière","brigade":27,"CP":34150},
{"id":206,"nom":"Puechabon","brigade":27,"CP":34150},
{"id":207,"nom":"Saint Guilhem de Désert","brigade":27,"CP":34150},
{"id":208,"nom":"Arboras","brigade":28,"CP":34150},
{"id":209,"nom":"Aumelas","brigade":28,"CP":34230},
{"id":210,"nom":"Gignac","brigade":28,"CP":34150},
{"id":211,"nom":"Jonquières","brigade":28,"CP":34725},
{"id":212,"nom":"Lagamas","brigade":28,"CP":34150},
{"id":213,"nom":"Montpeyroux","brigade":28,"CP":34150},
{"id":214,"nom":"Popian","brigade":28,"CP":34230},
{"id":215,"nom":"Pouzols","brigade":28,"CP":34230},
{"id":216,"nom":"Saint André de Sagonis","brigade":28,"CP":34725},
{"id":217,"nom":"Saint Bauzille de la Sylve","brigade":28,"CP":34230},
{"id":218,"nom":"Saint Guiraud","brigade":28,"CP":34725},
{"id":219,"nom":"Saint Jean de Fos","brigade":28,"CP":34150},
{"id":220,"nom":"Saint Saturnin de Lucian","brigade":28,"CP":34725},
{"id":221,"nom":"Vendemian","brigade":28,"CP":34230},
{"id":222,"nom":"Le Caylar","brigade":29,"CP":34520},
{"id":223,"nom":"Le Cros","brigade":29,"CP":34520},
{"id":224,"nom":"Les Rives","brigade":29,"CP":34520},
{"id":225,"nom":"Pegairolles de l'Escalette","brigade":29,"CP":34700},
{"id":226,"nom":"Romiguières","brigade":29,"CP":34650},
{"id":227,"nom":"Roqueredonde","brigade":29,"CP":34650},
{"id":228,"nom":"Sorbs","brigade":29,"CP":34520},
{"id":229,"nom":"Saint Felix de l'Heras","brigade":29,"CP":34520},
{"id":230,"nom":"Saint Maurice Navacelles","brigade":29,"CP":34520},
{"id":231,"nom":"Saint Michel","brigade":29,"CP":34520},
{"id":232,"nom":"Celles","brigade":30,"CP":34700},
{"id":233,"nom":"Fozieres","brigade":30,"CP":34700},
{"id":234,"nom":"La Vacquerie et St Martin","brigade":30,"CP":34520},
{"id":235,"nom":"Lauroux","brigade":30,"CP":34700},
{"id":236,"nom":"Lavalette","brigade":30,"CP":34700},
{"id":237,"nom":"Le Bosc","brigade":30,"CP":34700},
{"id":238,"nom":"Le Puech","brigade":30,"CP":34700},
{"id":239,"nom":"Les Plans","brigade":30,"CP":34700},
{"id":240,"nom":"Lodève","brigade":30,"CP":34700},
{"id":241,"nom":"Octon","brigade":30,"CP":34800},
{"id":242,"nom":"Olmet et Villecun","brigade":30,"CP":34700},
{"id":243,"nom":"Poujols","brigade":30,"CP":34700},
{"id":244,"nom":"Soubes","brigade":30,"CP":34700},
{"id":245,"nom":"Soumont","brigade":30,"CP":34700},
{"id":246,"nom":"Saint Etienne de Gourgas","brigade":30,"CP":34700},
{"id":247,"nom":"Saint Jean de la Blaquière","brigade":30,"CP":34700},
{"id":248,"nom":"Saint Pierre de la Fage","brigade":30,"CP":34520},
{"id":249,"nom":"Saint Privat","brigade":30,"CP":34700},
{"id":250,"nom":"Saint Usclas du Bosc","brigade":30,"CP":34700},
{"id":251,"nom":"Baillargues","brigade":31,"CP":34670},
{"id":252,"nom":"Beaulieu","brigade":31,"CP":34160},
{"id":253,"nom":"Castries","brigade":31,"CP":34160},
{"id":254,"nom":"Restinclieres","brigade":31,"CP":34160},
{"id":255,"nom":"Saint Bres","brigade":31,"CP":34670},
{"id":256,"nom":"Saint Drezery","brigade":31,"CP":34160},
{"id":257,"nom":"Saint Genies des Mourgues","brigade":31,"CP":34160},
{"id":258,"nom":"Sussargues","brigade":31,"CP":34160},
{"id":259,"nom":"Vendargues","brigade":31,"CP":34740},
{"id":260,"nom":"La Grande Motte","brigade":32,"CP":34280},
{"id":261,"nom":"Boisseron","brigade":33,"CP":34160},
{"id":262,"nom":"Lunel","brigade":33,"CP":34400},
{"id":263,"nom":"Lunel Viel","brigade":33,"CP":34400},
{"id":264,"nom":"Marsillargues","brigade":33,"CP":34590},
{"id":265,"nom":"Saturargues","brigade":33,"CP":34400},
{"id":266,"nom":"Saussines","brigade":33,"CP":34160},
{"id":267,"nom":"Saint Christol","brigade":33,"CP":34400},
{"id":268,"nom":"Saint Just","brigade":33,"CP":34400},
{"id":269,"nom":"Saint Nazaire de Pezan","brigade":33,"CP":34400},
{"id":270,"nom":"Saint Series","brigade":33,"CP":34400},
{"id":271,"nom":"Valergues","brigade":33,"CP":34130},
{"id":272,"nom":"Verargues","brigade":33,"CP":34400},
{"id":273,"nom":"Villetelle","brigade":33,"CP":34400},
{"id":274,"nom":"Candillargues","brigade":34,"CP":34130},
{"id":275,"nom":"Lansargues","brigade":34,"CP":34130},
{"id":276,"nom":"Mauguio","brigade":34,"CP":34130},
{"id":277,"nom":"Mudaison","brigade":34,"CP":34130},
{"id":278,"nom":"Saint Aunes","brigade":34,"CP":34130},
{"id":279,"nom":"Bouzigues","brigade":35,"CP":34140},
{"id":280,"nom":"Loupian","brigade":35,"CP":34140},
{"id":281,"nom":"Meze","brigade":35,"CP":34140},
{"id":282,"nom":"Poussan","brigade":35,"CP":34560},
{"id":283,"nom":"Villeveyrac","brigade":35,"CP":34560},
{"id":284,"nom":"Bessan","brigade":36,"CP":34550},
{"id":285,"nom":"Marseillan","brigade":36,"CP":34340},
{"id":286,"nom":"Vias","brigade":36,"CP":34450},
{"id":287,"nom":"Florensac","brigade":37,"CP":34510},
{"id":288,"nom":"Pinet","brigade":37,"CP":34850},
{"id":289,"nom":"Pomerols","brigade":37,"CP":34810},
{"id":290,"nom":"Balaruc le Vieux","brigade":38,"CP":34540},
{"id":291,"nom":"Balaruc les Bains","brigade":38,"CP":34540},
{"id":292,"nom":"Cournonsec","brigade":38,"CP":34660},
{"id":293,"nom":"Cournonterral","brigade":38,"CP":34660},
{"id":294,"nom":"Gigean","brigade":38,"CP":34770},
{"id":295,"nom":"Montbazin","brigade":38,"CP":34560},
{"id":296,"nom":"Adissan","brigade":40,"CP":34230},
{"id":297,"nom":"Aumes","brigade":40,"CP":34530},
{"id":298,"nom":"Cazouls d'Hérault","brigade":40,"CP":34120},
{"id":299,"nom":"Fontes","brigade":40,"CP":34320},
{"id":300,"nom":"Lezignan la Cebe","brigade":40,"CP":34120},
{"id":301,"nom":"Montagnac","brigade":40,"CP":34530},
{"id":302,"nom":"Nizas","brigade":40,"CP":34320},
{"id":303,"nom":"Saint Pons de Mauchiens","brigade":40,"CP":34230},
{"id":304,"nom":"Usclas d'Hérault","brigade":40,"CP":34230},
{"id":305,"nom":"Alignan du Vent","brigade":41,"CP":34290},
{"id":306,"nom":"Castelnau de Guers","brigade":41,"CP":34120},
{"id":307,"nom":"Caux","brigade":41,"CP":34720},
{"id":308,"nom":"Montblanc","brigade":41,"CP":34290},
{"id":309,"nom":"Nezignan l'Eveque","brigade":41,"CP":34120},
{"id":310,"nom":"Pezenas","brigade":41,"CP":34120},
{"id":311,"nom":"Saint Thibery","brigade":41,"CP":34630},
{"id":312,"nom":"Tourbes","brigade":41,"CP":34120},
{"id":313,"nom":"Valros","brigade":41,"CP":34290},
{"id":314,"nom":"Fos","brigade":42,"CP":34320},
{"id":315,"nom":"Fouzilhon","brigade":42,"CP":34480},
{"id":316,"nom":"Gabian","brigade":42,"CP":34320},
{"id":317,"nom":"Magalas","brigade":42,"CP":34480},
{"id":318,"nom":"Margon","brigade":42,"CP":34320},
{"id":319,"nom":"Montesquieu","brigade":42,"CP":34320},
{"id":320,"nom":"Neffies","brigade":42,"CP":34320},
{"id":321,"nom":"Pouzolles","brigade":42,"CP":34480},
{"id":322,"nom":"Roquessels","brigade":42,"CP":34320},
{"id":323,"nom":"Roujan","brigade":42,"CP":34320},
{"id":324,"nom":"Vailhan","brigade":42,"CP":34320},
{"id":325,"nom":"Abeilhan","brigade":43,"CP":34290},
{"id":326,"nom":"Bassan","brigade":43,"CP":34290},
{"id":327,"nom":"Coulobres","brigade":43,"CP":34290},
{"id":328,"nom":"Espondeilhan","brigade":43,"CP":34290},
{"id":329,"nom":"Lieuran les Béziers","brigade":43,"CP":34290},
{"id":330,"nom":"Puissalicon","brigade":43,"CP":34480},
{"id":331,"nom":"Servian","brigade":43,"CP":34290},
{"id":332,"nom":"Agde","brigade":46,"CP":34300},
{"id":333,"nom":"Béziers","brigade":45,"CP":34500},
{"id":335,"nom":"Boujan sur Libron","brigade":45,"CP":34760},
{"id":336,"nom":"Cebazan","brigade":8,"CP":34360},
{"id":337,"nom":"Cers","brigade":45,"CP":34420},
{"id":338,"nom":"Cessenon sur Orb","brigade":8,"CP":34460},
{"id":339,"nom":"Frontignan","brigade":49,"CP":34410},
{"id":340,"nom":"Lattes","brigade":50,"CP":34970},
{"id":341,"nom":"Montpellier","brigade":51,"CP":34000},
{"id":342,"nom":"Pégairolles de Buèges","brigade":24,"CP":34380},
{"id":343,"nom":"Pérols","brigade":50,"CP":34470},
{"id":344,"nom":"Sète","brigade":48,"CP":34200},
{"id":345,"nom":"Usclas du Bosc","brigade":30,"CP":34700},
{"id":346,"nom":"Villeneuve les Béziers","brigade":45,"CP":34420},
{"id":347,"nom":"Cap d'Agde","brigade":47,"CP":34300}]
};