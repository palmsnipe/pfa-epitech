﻿enyo.kind({
    name: "BrigadePanel",
	// classes: "onyx",
    components: [
		{kind: "FittableRows", classes: "enyo-fit", components: [
			{kind: "FittableColumns", classes: "brigade-input-container", components: [
				{kind: "onyx.InputDecorator", classes:"brigade-input", fit: true, components: [
					{kind: "onyx.Input", name: "commune", placeholder: "Recherchez une commune", oninput: "brigadeSearchInput"}
				]},
				{kind: "onyx.Button", classes: "brigade-geolocalisation-button", content: "G", ontap: "geolocationTap", components: [
					{kind: "onyx.Icon", classes: "icon", src: "assets/gps-1.png"}
				]}
			]},
			{content: "Tapez sur votre commune pour la mettre en préférence", classes: "brigade-info"},
			{kind: "List", name: "brigadeList", rowsPerPage: 5, multiSelect: false, onSetupItem: "setupBrigade", fit: true, components: [
				{kind: "onyx.Groupbox", classes: "brigade-item",  ontap: "brigadeTap", components: [
					{kind: "onyx.GroupboxHeader", name: "ville", content: ""},
					{components: [
						{name: "type", content: "", classes: "brigade-item-type", fit: true},
						{kind: "FittableColumns", components: [
							{content: "Adresse", classes: "label brigade-item-label"},
							{name: "adresse", content: "", fit: true, classes: "brigade-item-content"}
						]},
						{kind: "FittableColumns", showing: true, components: [
							{content: "Code Postal", classes: "label brigade-item-label"},
							{name: "codepostal", content: "", fit: true, classes: "brigade-item-content"}
						]},
						{kind: "FittableColumns", components: [
							{content: "Ville", classes: "label brigade-item-label "},
							{name: "brigade", content: "", fit: true, classes: "brigade-item-content brigade-item-ville"}
						]},
						{kind: "FittableColumns", components: [
							{content: "Téléphone", classes: "label brigade-item-label"},
							{name: "telephone", content: "", fit: true, classes: "brigade-item-content"}
						]},
						{kind: "FittableColumns", components: [
							{content: "Email", classes: "label brigade-item-label"},
							{name: "email", content: "", fit: true, classes: "brigade-item-content"}
						]},
						{classes: "brigade-item-group", showing: false, components: [
							{kind: "onyx.Button", content: "T"},
							{kind: "onyx.Button", content: "M"}
						]}
					]}
				]}
			]}
		]}
    ],
    create: function () {
        this.inherited(arguments);

		
		this.useGPS = false;
    },
    genList: function() {

    	this.$.brigadeList.setCount(enyo.communes.length);
		this.$.brigadeList.reset();
    },
	setupBrigade: function(inSender, inEvent) {
		var i = inEvent.index;
		var data = this.filter ? this.filtered : enyo.communes;
		var item = data[i];
		

		var adrTab = item["adresse"].split("$");
		var cpVille = adrTab[adrTab.length - 1];
		var cp = cpVille.substring(0, cpVille.indexOf(" "));
		// var ville = function() {
		// 	var str = "";
		// 	for (var i = 1; i < cpVille.length; i++) {
		// 		str += cpVille[i];
		// 	}
		// 	return str;
		// }();
		var ville = cpVille.substring(cpVille.indexOf(" ") + 1, cpVille.length);

		var adresse = function() {
			var str = "";
			for (var i = 0; i < adrTab.length - 1; i++) {
				if (i > 0) {
					str += ", ";
				}
				str += adrTab[i];
			}
			return str;
		}();

		var type = function() {
			var tab = item["unite surveillance"].split(" ");
			if (tab[0] == "BP") {
				return "Brigade de proximité";
			}
			else if (tab[0] == "BTA") {
				return "Brigade Territoriale Autonome";
			}
			else if (tab[0] == "ZPN") {
				return "Zone Police Nationale";
			}
			else {
				return item["unite surveillance"];
			}
		}();

		var mail = (item["mail"] == "#N/A" || item["mail"] == 0) ? "non renseigné" : item["mail"];

		this.$.ville.setContent(item["code postal"] + " - " + item["Commune"]);
		this.$.email.setContent(item["mail"]);
		this.$.telephone.setContent(item["TPH"].replace("+33 ", "0"));
		this.$.adresse.setContent(adresse);
		this.$.codepostal.setContent(cp);
		this.$.brigade.setContent(ville);
		this.$.type.setContent(type);
		this.$.email.setContent(mail);
	},
	brigadeSearchInput: function(inSender) {
		enyo.job(this.id + ":search", enyo.bind(this, "filterBrigadeList", inSender.getValue()), 200);
	},
	filterBrigadeList: function(inFilter) {
		if (inFilter != this.filter) {
			this.filter = inFilter;
			this.filtered = this.generateFilteredData(inFilter);
			this.$.brigadeList.setCount(this.filtered.length);
			this.$.brigadeList.reset();
		}
	},
	removeAccent: function(inString) {
		var tmp = inString.replace(/[àâä]/gi, "a");
		tmp = tmp.replace(/[éèêë]/gi, "e");
		tmp = tmp.replace(/[îï]/gi, "i");
		tmp = tmp.replace(/[ôö]/gi, "o");
		tmp = tmp.replace(/[ùûü]/gi, "u");
		tmp = tmp.replace(/[-]/gi, " ");

		return tmp;
	},
	generateFilteredData: function(inFilter) {
		var re = new RegExp("^" + this.removeAccent(inFilter), "i");
		var r = [];
		for (var i=0, d; d=enyo.communes[i]; i++) {
			if (this.removeAccent(d["Commune"]).match(re) || d["code postal"].toString().match(re)) {
				if (this.useGPS) {
					if (d["Commune"].length == inFilter.length) {
						this.useGPS = false;
						r.push(d);
					}
				}
				else {
					r.push(d);
				}
			}
		}
		return r;
	},
	callb: function(notification) {
		//alert("click!");
	},
	brigadeTap: function(inSender, inEvent) {
		var data = this.filter ? this.filtered : enyo.communes;
		var item = data[inEvent.index];
		
		var adresse = item["adresse"].split("$");
		localStorage.setItem("adresse", adresse.join(", "));

		enyo.$.app.message("Brigade enregistrée : ", item["unite surveillance"], 1);
	},
	getBrigade: function(id) {
		if (id > this.brigade.brigade.length) {
			return {};
		}
		var brigade = this.brigade.brigade[id];
		
		var result = {
			"ville": nomBrigade,
			"adresse": brigade.adresse
		};
		
		return result;
	},
	geolocationTap: function() {
		navigator.geolocation.getCurrentPosition(enyo.bind(this, "getPosition"), enyo.bind(this, "errorCoord"));
	},
	getPosition: function(position) {
		
		var ajax = new enyo.Ajax({
				url: "http://nominatim.openstreetmap.org/reverse?format=json&lat=" + position.coords.latitude + "&lon=" + position.coords.longitude
			});
		ajax.go();
		ajax.response(this, "putCity");
		ajax.error(this, "errorCoord");
		
	},
	putCity: function(inSender, inResponse) {
		this.useGPS = true;
		this.$.commune.setValue(inResponse.address.city);
		this.brigadeSearchInput(this.$.commune);
	},
	errorCoord: function(inSender, inResponse) {
		enyo.$.app.message("GPS : ", "Géolocalisation impossible", 1);
	}
});
