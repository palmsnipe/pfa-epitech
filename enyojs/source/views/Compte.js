﻿enyo.kind({
    name: "ComptePanel",
    fit: true,
    components: [
		{kind: "Scroller", classes: "enyo-fit", components: [
			{kind: "FittableRows", classes: "compte-scroller", components: [
				// {content: "Mon compte", classes: "compte-title", fit: true },
				{kind: "onyx.Groupbox", components: [
					{kind : "onyx.GroupboxHeader", content: "Nom et Prénom"},
					{kind: "onyx.InputDecorator", components: [
						{kind: "onyx.Input", name:"nameCompte", placeholder:"Nom", onchange:"setNameCompte"}
					]},
					{kind: "onyx.InputDecorator", components: [
						{kind: "onyx.Input", name:"firstNameCompte", placeholder: "Prénom", onchange:"setFirstNameCompte"}				
					]},
				]},
				{kind: "onyx.Groupbox", components: [
					{kind : "onyx.GroupboxHeader", content: "Adresse"},
					{kind: "onyx.InputDecorator", components: [
						{kind: "onyx.Input", name:"noStreetCompte", placeholder: "Numéro", onchange:"setNoStreetCompte"}
					]},
					{kind: "onyx.InputDecorator", components: [
						{kind: "onyx.Input", name:"nameStreetCompte", placeholder: "Libéllé de la voie", onchange:"setNameStreetCompte"}
					]},
					{kind: "onyx.InputDecorator", components: [
						{kind: "onyx.Input", name:"CPCompte", placeholder: "Code postal", onchange:"setCPCompte"}
					]},
					{kind: "onyx.InputDecorator", components: [
						{kind: "onyx.Input", name:"cityCompte", placeholder: "Ville", onchange:"setCityCompte"}
					]},
				]},
				{kind: "onyx.Groupbox", components: [
					{kind : "onyx.GroupboxHeader", content: "Informations personelles"},
					{kind: "onyx.InputDecorator", components: [
						{kind: "onyx.Input", name:"phoneCompte", placeholder: "Téléphone", onchange:"setPhoneCompte"}
					]},
				]},
			/*	{classes: "compte-button-send", components: [
					{kind: "onyx.Button", content: "Valider"}
				]}*/
				
				{kind:"FittableColumns", components: [			
					{content: "Les données seront enregistrées localement sur votre téléphonne.", classes: "compte-storage-text", fit:true}
				]},
			]}
		]}
	],
	
	create: function () {
		this.inherited(arguments);
		this.$.nameCompte.setValue(localStorage.getItem("nameCompte"));
		this.$.firstNameCompte.setValue(localStorage.getItem("firstNameCompte"));
		this.$.noStreetCompte.setValue(localStorage.getItem("noStreetCompte"));
		this.$.nameStreetCompte.setValue(localStorage.getItem("nameStreetCompte"));
		this.$.CPCompte.setValue(localStorage.getItem("CPCompte"));
		this.$.cityCompte.setValue(localStorage.getItem("cityCompte"));
		this.$.phoneCompte.setValue(localStorage.getItem("phoneCompte"));
		},
	
	setNameCompte: function(inSender, inEvent) {
		console.log(inSender.getValue());
		localStorage.setItem("nameCompte", inSender.getValue());
	},
	
	setFirstNameCompte: function(inSender, inEvent) {
		console.log(inSender.getValue());
		localStorage.setItem("firstNameCompte", inSender.getValue());
	},
	
	setNoStreetCompte: function(inSender, inEvent) {
		console.log(inSender.getValue());
		localStorage.setItem("noStreetCompte", inSender.getValue());
	},
	
	setNameStreetCompte: function(inSender, inEvent) {
		console.log(inSender.getValue());
		localStorage.setItem("nameStreetCompte", inSender.getValue());
	},
	
	setCPCompte: function(inSender, inEvent) {
		console.log(inSender.getValue());
		localStorage.setItem("CPCompte", inSender.getValue());
	},
	
	setCityCompte: function(inSender, inEvent) {
		console.log(inSender.getValue());
		localStorage.setItem("cityCompte", inSender.getValue());
	},

	setPhoneCompte: function(inSender, inEvent) {
		console.log(inSender.getValue());
		localStorage.setItem("phoneCompte", inSender.getValue());
	},	
});