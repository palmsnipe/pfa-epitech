﻿enyo.kind({
    name: "ProtegerPanel",
	fit: true,
	components:[
		{kind: "FittableRows", classes: "enyo-fit", components: [
			{kind: "onyx.Toolbar", classes: "proteger-toolbar", components: [
				{content: "Prévention"}
			]},
			{kind: "FittableColumns", classes: "proteger-navigation", components: [			
				{kind:"onyx.Icon", name:"flecheprevious", classes: "proteger-fleche", src:"assets/gauche-1.png", ontap: "prevPanel"},
				{name:"titleproteger", content: "Protéger son domicile", fit: true, classes:"proteger-title"},
				{kind:"onyx.Icon", name:"flechenext", classes: "proteger-fleche", src:"assets/droite-1.png", ontap: "nextPanel"}
			]},
			{kind: "Panels", name: "protegerPanels", fit:true, arrangerKind: "CardArranger", classes: "proteger-panels", onTransitionFinish: "titlePanel", components: [
				{kind: "Scroller", fit: true, components: [
					{kind: "Repeater", name: "protegerList", onSetupItem: "setProteger", components: [
						{kind: "onyx.Groupbox", classes: "proteger-groupbox", components: [
							{name: "protegerItem", content: ""}
						]}
					]}
				]},
				{kind: "Scroller", classes: "enyo-fit", components: [
					{kind: "Repeater", name: "gestesList", onSetupItem: "setGestes", components: [
						{kind: "onyx.Groupbox", classes: "proteger-groupbox", components: [
							{name: "gestesItem", content: ""}
						]}
					]}
				]},
				{kind: "Scroller", classes: "enyo-fit", components: [
					{kind: "Repeater", name: "absenceList", onSetupItem: "setAbsence", components: [
						{kind: "onyx.Groupbox", classes: "proteger-groupbox", components: [
							{name: "absenceItem", content: ""}
						]}
					]}
				]}
			]}
		]}
	],
	create: function() {
		this.inherited(arguments);
		
		var doc = messages.prevention;
		this.textProteger = [];
		for (var i = 0; i <= 6; i++) {
			this.textProteger.push(doc[i].texte);
		}
		this.textGestes = [];
		for (var i = 7; i <= 18; i++) {
			this.textGestes.push(doc[i].texte);
		}
		this.textAbsence = [];
		for (var i = 19; i <= 24; i++) {
			this.textAbsence.push(doc[i].texte);
		}
		
		this.$.protegerList.setCount(this.textProteger.length);
		this.$.gestesList.setCount(this.textGestes.length);
		this.$.absenceList.setCount(this.textAbsence.length);
	},
	prevPanel: function() {
		this.$.protegerPanels.previous();
		this.titlePanel();
	},
	updateArrows: function() {
		var panel = this.$.protegerPanels;
		var panels = panel.getPanels();
		var left = this.$.flecheprevious;
		var right = this.$.flechenext;

		if (panels.length == 1) {
			left.applyStyle("background-position", "left bottom");
			right.applyStyle("background-position", "left bottom");
		}
		else {
			if (panel.index == 0) {
				left.applyStyle("background-position", "left bottom");
				right.applyStyle("background-position", "left top");
			}
			else {
				left.applyStyle("background-position", "left top");
				if (panel.index == panels.length - 1) {
					right.applyStyle("background-position", "left bottom");
				}
				else {
					right.applyStyle("background-position", "left top");
				}
			}
		}
	},
	nextPanel: function() {
		this.$.protegerPanels.next();
		this.titlePanel();
	},
	titlePanel: function() {
		this.updateArrows();
		if (this.$.protegerPanels.index <= 0) {
			this.$.titleproteger.setContent("Protéger son domicile");
		}
		else if (this.$.protegerPanels.index == 1) {
			this.$.titleproteger.setContent("Les bonnes pratiques");
		}
		else if (this.$.protegerPanels.index == 2){
			this.$.titleproteger.setContent("En cas d'absence durable");
		}
	},
	setProteger: function(inSender, inEvent) {
		var index = inEvent.index;
		var item = inEvent.item;
		
		if (item) {
			item.$.protegerItem.setContent(this.textProteger[index]);
			return true;
		}
	},
	setGestes: function(inSender, inEvent) {
		var index = inEvent.index;
		var item = inEvent.item;
		
		if (item) {
			item.$.gestesItem.setContent(this.textGestes[index]);
			return true;
		}
	},
	setAbsence: function(inSender, inEvent) {
		var index = inEvent.index;
		var item = inEvent.item;
		
		if (item) {
			item.$.absenceItem.setContent(this.textAbsence[index]);
			return true;
		}
	}
	
});
