﻿enyo.kind({
    name: "PreventMsgPanel",
	fit: true,
	components:[
		{kind: "WebService", name: "flux", url: "http://www.herault.pref.gouv.fr/syndication/flux/rss_feed_6746", handleAs: "xml", onResponse: "fluxResponse"},
		{kind: "FittableRows", classes: "enyo-fit", components: [
			{kind: "onyx.Toolbar", classes: "prevent-title", components: [
				{name: "title", content: "Informations"}
			]},
			{kind: "Scroller", name: "prevent-scroller", fit: true, components: [
				{name: "preventBox", showing: true, components: [
					{name: "intro", classes: "prevent-message", content: "Pensez-y ", allowHtml: true},
					{kind: "onyx.Groupbox", classes: "prevent-groupbox", components: [
						{kind: "FittableRows", classes: "prevent-message", components: [
							{name: "message", content: "", allowHtml: true}
						]}
					]}
				]},
				{name: "fluxList", showing: false, components: [
					{content: "La gendarmerie de l’Hérault vous informe en temps réel.", style: "padding: 10px 10px 0 10px; font-size: 14px; text-align: center;"},
					{kind: "Repeater", name: "actuList", onSetupItem: "setActu", components: [
						{kind: "onyx.Groupbox", classes: "proteger-groupbox", components: [
							{kind: "onyx.GroupboxHeader", name: "actuTitle", content: ""},
							{components: [
								{name: "actuDate", content: "", style: "text-align: right; font-style: italic; font-size: 12px; margin-bottom: -10px;"},
								{name: "actuDesc", content: "", allowHtml: true}
							]}
						]}
					]}
				]}
			]}
		]}
	],
	create: function() {
		this.inherited(arguments);
		this.randomText();
		this.$.flux.send();
	},
	randomText: function() {
		var doc = messages.prevention;
		var i = Math.floor((Math.random()*24)+1);
		
		if (i <= 6) {
			this.$.intro.setContent("Pensez-y ! Pour protégez votre domicile...\n\n");
		}
		else if ( i <= 18) {
			this.$.intro.setContent("Pensez-y ! Les bons gestes...\n\n");
		}
		else {
			this.$.intro.setContent("Pensez-y ! En cas d'absence durable...\n\n");
		}
		
		this.$.message.setContent(doc[i].texte);
	},
	fluxResponse: function(inSender, inEvent) {
		this.flux = x2js.xml2json(inEvent.data).rss.channel.item;
		this.$.preventBox.hide();

		this.$.title.setContent("Flux d'actualité");
		this.$.fluxList.show();

		this.$.actuList.setCount(this.flux.length);
		//var items = document.getElementsByTagName("items");
		//console.log(items);
	},
	setActu: function(inSender, inEvent) {
		var index = inEvent.index;
		var item = inEvent.item;

		function formatTime(nb) {
			return (nb < 10) ? "0" + nb : nb;
		}
		
		if (item && index < 5) {
			var mois = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Décembre"];
			item.$.actuTitle.setContent(this.flux[index].title);
			item.$.actuDesc.setContent(this.flux[index].description);
			var dt = new Date(this.flux[index].pubDate);

			item.$.actuDate.setContent(dt.getDay() + " " + mois[dt.getMonth()] + " " + 
				dt.getFullYear() + " à " + dt.getHours() + "h" + formatTime(dt.getMinutes()));
			return true;
		}
	},
});
