﻿enyo.kind({
    name: "AlertePanel",
	fit: true,
	components:[
		{kind: "FittableRows", classes: "enyo-fit", components: [
			{kind: "onyx.Toolbar", classes: "alerte-toolbar", components: [
				{content: "Réagir - Alerter"}
			]},
			{kind: "FittableColumns", classes: "alerte-navigation", components: [			
				{kind:"onyx.Icon", name:"flecheprevious", classes: "alerte-fleche", src:"assets/gauche-1.png", ontap: "prevPanel"},
				{name:"titlealerte", allowHtml: true, content: "", fit: true, classes:"alerte-title"},
				{kind:"onyx.Icon", name:"flechenext", classes: "proteger-fleche", src:"assets/droite-1.png", ontap: "nextPanel"}
			]},
			{kind: "Panels", name: "alertePanels", fit:true, arrangerKind: "CardArranger", classes: "alerte-panels", onTransitionFinish: "titlePanel", components: [
				{kind: "Scroller", fit: true, components: [
					{kind: "Repeater", name: "cambriolageList", onSetupItem: "setCambriolage", components: [
						{kind: "onyx.Groupbox", classes: "alerte-groupbox", components: [
							{name: "cambriolageItem", content: ""}
						]}
					]}
				]},
				{kind: "Scroller", fit: true, components: [
					{kind: "Repeater", name: "volList", onSetupItem: "setVol", components: [
						{kind: "onyx.Groupbox", classes: "alerte-groupbox", components: [
							{name: "volItem", content: ""}
						]}
					]}
				]}
			]},
			{kind:"onyx.Button", classes: "alerte-button-send onyx-negative", content: "Composer le 17", ontap: "showPopup", popup: "alertPopup"}
		]},
        {kind: "onyx.Popup", name: "alertPopup", classes: "popup-light alert-popup", centered: true, modal: false, floating: true, scrim: true, components: [
            {kind: "FittableRows", components: [
				{name:"msgAlertPopup", allowHtml: true, content: "", style: "margin-bottom: 10px;"},
				{content: "Vous allez appeler le 17.<br />Ceci est un numéro d'urgence accessible gratuitement et sans couverture réseau.<br />Attention, tout abus sera puni par la loi.<br /><br /><b>Souhaitez-vous poursuivre votre appel ?</b>", style: "margin-bottom: 15px;", allowHtml: true},
				{kind: "FittableRows", components: [
					{kind:"onyx.Button", classes: "alerte-button-rounded-send", style: "margin-right: 10px;", content: "Continuer", disabled: false, ontap:"appel"},
					{kind:"onyx.Button", classes: "alerte-button-rounded-send", content: "Annuler", disabled: false, ontap:"closePopup"}
				]}
			]}
        ]},
	],
	create: function() {
		this.inherited(arguments);

		this.messages = {
			"cambriolage" : {
				"titre" : "Si vous êtes victime<br />d'un cambriolage",
				"texte" : [
					"Ne touchez à aucun objet, porte ou fenêtre.",
					"Interdisez l'accès des lieux à toute personne, sauf en cas de nécessité.",
					"Protégez les traces et les indices à l'intérieur comme à l'extérieur.",
					"Prévenez immédiatement la brigade de gendarmerie du lieu de l'infraction. Si les cambrioleurs sont encore sur place, ne prenez pas de risques inconsidérés; privilégiez le recueil d'éléments d'identification (type de véhicule, langage, stature, vêtements...).",
					"Déposez plainte à la brigade de votre choix (article 5 de la Charte d'accueil du public). Munissez-vous de votre pièce d'identité.",
					"Faites opposition auprès de votre banque, pour vos chéquiers et cartes de crédits dérobés.",
					"Déclarez le vol à votre assureur.",
					"Le dépôt de plainte après un cambriolage est essentiel. Il permet aux cellules cambriolages implantées dans chaque département de faire des recoupements et ainsi d'appréhender les malfaiteurs. Ces unités sont épaulées par des policiers ou des gendarmes formés en police technique et scientifique qui se déplacent sur chaque cambriolage pour relever les traces et indices."
				]
			}, 
			"vol" : {
				"titre" : "Si vous êtes victime<br />d'un vol à main armée",
				"texte" : [
					"Observez votre agresseur pour en donner un signalement très précis.",
					"Notez son moyen et sa direction de fuite.",
					"Mémorisez le type, le numéro et la couleur du véhicule.",
					"Maintenez les lieux en l'état jusqu'à l'arrivée de la gendarmerie nationale.",
					"Retenez ou identifiez les témoins."
				]
			}
		};
		this.$.cambriolageList.setCount(this.messages.cambriolage.texte.length);
		this.$.volList.setCount(this.messages.vol.texte.length);
		
		this.updateArrows();
	},
	updateArrows: function() {
		var panel = this.$.alertePanels;
		var panels = panel.getPanels();
		var left = this.$.flecheprevious;
		var right = this.$.flechenext;

		if (panels.length <= 1) {
			left.applyStyle("background-position", "left bottom");
			right.applyStyle("background-position", "left bottom");
		}
		else {
			if (panel.index == 0) {
				left.applyStyle("background-position", "left bottom");
				right.applyStyle("background-position", "left top");
			}
			else {
				left.applyStyle("background-position", "left top");
				if (panel.index == panels.length - 1) {
					right.applyStyle("background-position", "left bottom");
				}
				else {
					right.applyStyle("background-position", "left top");
				}
			}
		}
	},


	setCambriolage: function(inSender, inEvent) {
		var index = inEvent.index;
		var item = inEvent.item;
		
		if (item) {
			item.$.cambriolageItem.setContent(this.messages.cambriolage.texte[index]);
			return true;
		}
	},
	setVol: function(inSender, inEvent) {
		var index = inEvent.index;
		var item = inEvent.item;
		
		if (item) {
			item.$.volItem.setContent(this.messages.vol.texte[index]);
			return true;
		}
	},
	titlePanel: function() {
		this.updateArrows();
		if (this.$.alertePanels.index <= 0) {
			this.$.titlealerte.setContent(this.messages.cambriolage.titre);
		}
		else if (this.$.alertePanels.index == 1) {
			this.$.titlealerte.setContent(this.messages.vol.titre);
		}
	},
	prevPanel: function() {
		this.$.alertePanels.previous();
		this.titlePanel();
	},
	nextPanel: function() {
		this.$.alertePanels.next();
		this.titlePanel();
	},
	showPopup: function(inSender, inEvent) {
		// if (localStorage.getItem("idBrigade")) {
		// 	var brigade = enyo.$.app.$.brigade.getBrigade(localStorage.getItem("idBrigade"));
		// 	this.$.msgAlertPopup.setContent("Vous allez être mis en relation avec la brigade en charge de <b>" + brigade.nom + "</b>.");
		// }
		// else {
		// 	this.$.msgAlertPopup.setContent("Vous allez être mis en relation avec le 17 car vous n'avez pas renseigné votre commune. Renseignez votre commune dans l'onglet Paramètres ou Brigades pour être directement mis en relation avec la brigade la plus proche.");
		// }
		this.$.msgAlertPopup.setContent("Vous allez être mis en relation avec la gendarmerie.");


        var p = this.$[inSender.popup];
        if (p) {
            p.show();
        }
    },
	appel: function() {
		var numero = 17;
		if (localStorage.getItem("idBrigade")) {
			var brigade = enyo.$.app.$.brigade.getBrigade(localStorage.getItem("idBrigade"));
			numero = brigade.telephone;
		}

		// console.log("numero = " + numero);
		window.location = "tel:" + numero;
	},
	closePopup: function(inSender, inEvent) {
		this.$.alertPopup.hide();
    }
});
