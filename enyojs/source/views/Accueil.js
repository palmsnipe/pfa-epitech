﻿enyo.kind({
    name: "AccueilPanel",
	fit: true,
	components:[
		{kind: "FittableRows", classes: "enyo-fit", components: [
			{kind: "FittableColumns", classes: "accueil-menu", components: [
					{kind: "onyx.Button", name: "btnActu", ontap: "onTapMenu", components: [
						{kind: "FittableRows", components: [
							{kind: "onyx.Icon", classes: "icon", src: "assets/bouclier-1.png"},
							{content: "Prévention"}
						]}
					]},
					{kind: "onyx.Button", name: "btnAlerte", ontap: "onTapMenu", components: [
						{kind: "FittableRows", components: [
							{kind: "onyx.Icon", classes: "icon", src: "assets/alerte-1.png"},
							{content: "Alerte 17"}
						]}
					]},
					{kind: "onyx.Button", name: "btnOTV", ontap: "onTapMenu", components: [
						{kind: "FittableRows", components: [
							{kind: "onyx.Icon", classes: "iconOtv", src: "assets/parasol-1.png", style: "height: 18px;"},
							{content: "Départ"},
							{content: "vacances"}
						]}
					]}
			]},
			{kind: "Panels", name: "accueilPanels", classes: "accueil-panels", arrangerKind: "CardSlideInArranger", draggable: false, fit: true, components: [
				{kind: "PreventMsgPanel", name: "astuce"},
				{kind: "ProtegerPanel", name: "proteger"},
				{kind: "AlertePanel", name: "alerte"},
				{kind: "OTVPanel", name: "otv"}
			]}
		]}
	],
	create: function () {
        this.inherited(arguments);
		
    },
	onTapMenu: function(inEvent, inSender) {
		var name = inEvent.getName();
		this.removeActiveMenu();
		inEvent.addClass("active");
		if (name == "btnActu") {
			this.$.accueilPanels.setIndex(1);
		}
		else if (name == "btnAlerte") {
			this.$.accueilPanels.setIndex(2);
		}
		else if (name == "btnOTV") {
			this.$.accueilPanels.setIndex(3);
		}
	},
	removeActiveMenu: function() {
		this.$.astuce.randomText();
		this.$.btnActu.removeClass("active");
		this.$.btnAlerte.removeClass("active");
		this.$.btnOTV.removeClass("active");
	}
});
