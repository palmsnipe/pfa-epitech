﻿var messages = {
	"prevention" : [
	// Protégez votre domicile :
		{"texte" : "Lorsque vous prenez possession d'un nouvel appartement ou d'une maison, pensez à changer les serrures."},
		{"texte" : "Équipez votre porte d'un système de fermeture fiable et d'un entrebâilleur."},
		{"texte" : "Installez des équipements adaptés et agréés (volets, grilles, éclairage automatique intérieur/extérieur, alarmes ou protection électronique...). Demandez conseil à un professionnel."},
		{"texte" : "N'inscrivez pas vos nom et adresse sur votre trousseau de clés."},
		{"texte" : "Si vous avez perdu vos clés et que l'on peut indentifier votre adresse, changez les serrures."},
		{"texte" : "Ne laissez pas vos clés sous le paillasson ou le pot de fleurs, confiez-les plutôt à une personne de confiance."},
		{"texte" : "Si vous possédez un coffre-fort, il ne doit pas être visible des personnes qui passent chez vous."},
	//Les bons gestes	
		{"texte" : "Fermez la porte à double tour, même lorsque vous êtes chez vous. Soyez vigilant sur tous les accès, ne laissez pas une clé sur la serrure intérieure d'une porte vitrée."},
		{"texte" : "De nuit, en période estivale, évitez de laisser les fenêtres ouvertes, surtout si elles sont accessibles depuis la voie publique."},
		{"texte" : "Ne laissez pas traîner dans le jardin, une échelle, des outils, un échafaudage..."},
		{"texte" : "Avant de laisser quelqu'un pénétrer dans votre domicile, assurez-vous de son identité en utilisant l'interphone, le judas ou l'entrebâilleur de porte."},
		{"texte" : "En cas de doute, même si des cartes professionnelles vous sont présentées, appelez le service ou la société dont vos interlocuteurs se réclament."},
		{"texte" : "Ne laissez jamais une personne inconnue seule dans une pièce de votre domicile."},
		{"texte" : "Placez en lieu sûr et éloigné des accès, vos bijoux, carte de crédit, sac à main, clés de voiture et ne laissez pas d'objets de valeur qui soient visibles à travers les fenêtres."},
		{"texte" : "Photographiez vos objets de valeur pour faciliter les recherches en cas de vol."},
		{"texte" : "Notez le numéro de série et la référence des matériels, conservez vos factures, ou expertises pour les objets de très grande valeur."},
		{"texte" : "Répertoriez vos biens sur une liste."},
		{"texte" : "Lorsque vous vous absentez, fermez vos portes et fenêtres."},
		{"texte" : "Signalez à la brigade de gendarmerie tout fait suspect pouvant laisser présager la préparation ou la commission d'un cambriolage."},
	// En cas d'absence durable :
		{"texte" : "Avisez vos voisins ou le gardien de votre résidence."},
		{"texte" : "Faitez suivre votre courrier ou faites-le relever par une personne de confiance : une boite à lettres débordant de plis révèle une longue absence."},
		{"texte" : "Votre domicile doit paraître habité ; demandez que l'on ouvre régulièrement les volets le matin."},
		{"texte" : "Créez l'illusion d'une présence, à l'aide d'un programmateur pour la lumière, la télévision, la radio..."},
		{"texte" : "Ne laissez pas de message sur votre répondeur téléphonique qui indiquerait la durée de votre absence. Transférez vos appels sur votre téléphone portable ou une autre ligne."},
		{"texte" : "Dans le cadre des opérations \"Tranquillité vacances\" (OTV) organisées durant les vacances scolaires, signalez votre absence au commissariat de police ou à la brigade de gendarmerie ; des patrouilles pour surveiller votre domicile seront organisées."}
	]
};