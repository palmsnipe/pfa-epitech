﻿enyo.kind({
    name: "OTVPanel",
	fit: true,
	// classes: "onyx",
	components:[
		{kind: "FittableRows", classes: "enyo-fit", components: [
			{kind: "onyx.Toolbar", classes: "otv-title", components: [
				{content: "Opération Tranquillité Vacances"}
			]},
			{kind: "Scroller", fit:true, components: [
				{kind: "FittableRows", classes:"otv-scroller", components: [
					{kind: "gts.DividerDrawer", caption: "Qu'est-ce que le plan OTV ?", open: false, components: [
						{content: "La gendarmerie met en place un formulaire de demande individuelle vous permettant d'informer votre brigade de votre départ.<br /><br />Cette demande renseignée doit être déposée ou adressée à la brigade de gendarmerie de votre lieu de domicile.", classes: "otv-texte", allowHtml: true}
					]},
					{kind: "gts.DividerDrawer", caption: "Comment s'inscrire ?", open: false, components: [
						{content: "Pour bénéficier de ce service gratuit, inscrivez-vous avant votre départ, à votre brigade de proximité accompagné d'une pièce d'identité nationale et du formulaire de contact rempli, accessible depuis le site internet de la gendarmerie nationale.<br />Vous pouvez télécharger ci-dessous le formulaire et le transférer par mail sur votre messagerie électronique.<br /><br /><center><span style='font-size: 18px; font-weight: bold;'><a href='http://www.gendarmerie.interieur.gouv.fr/fre/content/download/585/5167/file/Demande_individuelle.pdf'>Formulaire de contact OTV</a></span></center>", classes: "otv-texte", allowHtml: true}
					]},
					{style: "text-align: center; padding: 10px;", components: [
						{kind: "onyx.Button", classes: "otv-button-map", ontap: "maps", popup: "otvPopup", components: [
							{content: "Se déplacer à la brigade<br />ou au commissariat", allowHtml: true}
						]}
					]}
				]}
			]}
		]},
        {kind: "onyx.Popup", name: "otvPopup", classes: "popup-light alert-popup", centered: true, modal: false, floating: true, scrim: true, components: [
            {kind: "FittableRows", style: "margin: 5px;", components: [
				{content: "Veuillez préalablement rechercher la<br />brigade de gendarmerie ou le<br />commissariat de police concerné", style: "margin-bottom: 15px;", allowHtml: true},
				{kind: "FittableRows", components: [
					{kind:"onyx.Button", classes: "otv-popup-buttons", content: "Oui", disabled: false, ontap:"brigades"},
					{kind:"onyx.Button", classes: "otv-popup-buttons", content: "Non", disabled: false, ontap:"closePopup"}
				]}
			]}
        ]},
		{kind: "Notification", name: "notifOTV"}
	],
	create: function() {
		this.inherited(arguments);
	},
	maps: function(inEvent, inSender) {
		var adresse = localStorage.getItem("adresse");

		if (!adresse) {
			this.$.otvPopup.show();
			//enyo.$.app.message("Erreur : ", "Choisir une brigade", 1);

			// this.$.notifOTV.sendNotification({
			// 		title: "Erreur : ",
			// 		message: "Choisir une brigade",
			// 		icon: "icon.png",
			// 		theme: "notification.MessageBar",
			// 		stay: undefined,
			// 		duration: 1
			// 	}, enyo.bind(this, "callc"));
		}
		else {
			var newAdr = function() {
				if (adresse.indexOf("Caserne", 0) != -1 || adresse.indexOf("Village", 0) != -1) {
					return adresse.substring(adresse.indexOf(", ") + 2, adresse.length);
				}
				else {
					return adresse;
				}
			}();

			var query = "?q=" + newAdr + ", France";

			var url = 'http://maps.google.com/maps';
		    var osVersion = "not detected";

		    if(/OS [2-4](_\d)(_\d)? like Mac OS X/i.test(navigator.userAgent)) { //is it ios 2-4?
		        url = 'http://maps.google.com/maps'; osVersion = "ios 2-4";
		    } else if(/CPU like Mac OS X/i.test(navigator.userAgent)) { // ios 1?!?!
		        url = 'http://maps.google.com/maps'; osVersion = "ios 1";
		    } else if(/OS [5](_\d)(_\d)? like Mac OS X/i.test(navigator.userAgent)) { //ios 5?
		        url = 'http://maps.google.com/maps'; osVersion = "ios 5";
		    } else{//ios 6 or greater
		        url = 'http://maps.apple.com/'; osVersion = "ios 6 or greater";
		    }

		    url += query;
			window.location.href = url;
		}
	},
	callc: function(notification) {
		//alert("click!");
	},
	brigades: function() {
		this.$.otvPopup.hide();
		enyo.$.app.$.menuBrigade.setActive(true);
		//console.log(enyo.$.app.$.menuBrigade);
		enyo.$.app.$.mainPanels.setIndex(1);
	},
	showPopup: function(inSender, inEvent) {
        var p = this.$[inSender.popup];
        if (p) {
            p.show();
        }
    },
	closePopup: function(inSender, inEvent) {
		this.$.otvPopup.hide();
    }
});
