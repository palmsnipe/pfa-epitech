﻿enyo.kind({
    name: "ActualitePanel",
	fit: true,
	components:[
		{kind: "FittableRows", classes: "enyo-fit", components: [
			{kind: "onyx.Toolbar", classes: "actualite-title", components: [
				{content: "Actualités"}
			]},
			{kind: "Scroller", classes: "actualite-scroller", fit: true, components: [
				{kind: "Repeater", onSetupItem : "setupNews", components: [ 
					{name: "item", components: [
						{tag:"span", name: "mess"}
					]}
				]}
			]}
		]},
	],
	create: function () {
        this.inherited(arguments);
    }
});
