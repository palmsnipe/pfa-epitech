﻿enyo.kind({
    name: "App",
    classes: "onyx",
    fit: true,
    components: [
    	{kind: "WebService", name: "wsBrigades", handleAs: "text", url: "assets/communes.csv", onResponse: "brigadesResponse"},
		{kind: "FittableRows", classes: "enyo-fit", components: [
			// {kind: "FittableColumns", components: [
				{classes: "app-header", components: [
					{classes: "app-header-content", components: [
						{content: "La gendarmerie de l'hérault", classes: "app-header-title"},
						{content: "vous protège", classes: "app-header-subtitle"}
					]},
					{kind: "Image", src: "assets/blason-1.png", style: "width: 32px; padding-bottom: 32px;"}
				]},
			// ]},
			{kind: "Panels", name: "mainPanels", arrangerKind: "CardArranger", margin: 0, classes: "app-main-panels", draggable: false, fit: true, components: [
				{kind: "AccueilPanel", name: "accueil", active: true},
				{kind: "BrigadePanel", name: "brigade"},
				{kind: "ComptePanel", name: "compte"},
				{kind: "AidePanel", name: "aide"}
			]},
			{kind: "Notification", name: "notif"},
			{classes: "app-footer-border"},
			{kind: "onyx.RadioGroup", name: "menu", onActivate:"menuActivated", classes: "app-footer-menu-group", controlClasses: "app-footer-menu", components: [
				{name: "menuAccueil", active: true, ontap: "resetAccueil", components: [
					{kind: "FittableRows", components: [
						{kind: "onyx.Icon", classes: "icon", src: "assets/accueil-1.png"},
						{content: "Accueil"}
					]}
				]},
				{name: "menuBrigade", components: [
					{kind: "FittableRows", components: [
						{kind: "onyx.Icon", classes: "icon", src: "assets/brigades-1.png"},
						{content: "Brigade"}
					]}
				]},
				// {name: "menuParametres", components: [
					// {kind: "FittableRows", components: [
						// {kind: "onyx.Icon", classes: "icon", src: "assets/parametres.png"},
						// {content: "Paramètres"}
					// ]}
				// ]},
				{name: "menuAide", components: [
					{kind: "FittableRows", components: [
						{kind: "onyx.Icon", classes: "icon", src: "assets/aide-1.png"},
						{content: "Aide"}
					]}
				]}
			]}
		]}
    ],
    create: function () {
        this.inherited(arguments);
        
        this.$.wsBrigades.send();
    },
	resetAccueil: function(inSender, inEvent) {
		this.$.accueil.$.accueilPanels.setIndex(0);
		this.$.accueil.removeActiveMenu();
	},
	menuActivated: function(inSender, inEvent) {
		//console.log(enyo.$.app.$.menu);
		if (inEvent.originator.getActive()) {
			var str = inEvent.originator.getName();
			if (str == "menuAccueil") {
				this.$.mainPanels.setIndex(0);			
			}
			else if (str == "menuBrigade") {
				this.$.mainPanels.setIndex(1);
			}
			// else if (str == "menuParametres") {
			// 	this.$.mainPanels.setIndex(2);
			// }
			else if (str == "menuAide") {
				this.$.mainPanels.setIndex(3);
			}
		}
	},
	message: function(titre, message, duree) {
		this.$.notif.sendNotification({
			title: titre,
			message: message,
			icon: "icon.png",
			theme: "notification.MessageBar",
			stay: undefined,
			duration: duree
		}, enyo.bind(this, "callb"));
	},
	callb: function(notification) {
		alert("click!");
	},
	brigadesResponse: function(inSender, inEvent) {
		enyo.communes = (csvjson.csv2json(inEvent.data, {
    		delim: ",",
    		textdelim: "\""
    	})).rows;
    	this.$.brigade.genList();
	},
	brigadesError: function(inSender, inEvent) {
		alert("La récupération des brigades a échoué");
	}
});
