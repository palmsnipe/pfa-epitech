
// minifier: path aliases

enyo.path.addPaths({layout: "/Users/palmsnipe/Documents/Epitech/pfa-epitech/enyojs/enyo/../lib/layout/", onyx: "/Users/palmsnipe/Documents/Epitech/pfa-epitech/enyojs/enyo/../lib/onyx/", onyx: "/Users/palmsnipe/Documents/Epitech/pfa-epitech/enyojs/enyo/../lib/onyx/source/", DividerDrawer: "/Users/palmsnipe/Documents/Epitech/pfa-epitech/enyojs/enyo/../lib/DividerDrawer/", notification: "/Users/palmsnipe/Documents/Epitech/pfa-epitech/enyojs/enyo/../lib/notification/", views: "source/views/"});

// FittableLayout.js

enyo.kind({
name: "enyo.FittableLayout",
kind: "Layout",
calcFitIndex: function() {
for (var e = 0, t = this.container.children, n; n = t[e]; e++) if (n.fit && n.showing) return e;
},
getFitControl: function() {
var e = this.container.children, t = e[this.fitIndex];
return t && t.fit && t.showing || (this.fitIndex = this.calcFitIndex(), t = e[this.fitIndex]), t;
},
getLastControl: function() {
var e = this.container.children, t = e.length - 1, n = e[t];
while ((n = e[t]) && !n.showing) t--;
return n;
},
_reflow: function(e, t, n, r) {
this.container.addRemoveClass("enyo-stretch", !this.container.noStretch);
var i = this.getFitControl();
if (!i) return;
var s = 0, o = 0, u = 0, a, f = this.container.hasNode();
f && (a = enyo.dom.calcPaddingExtents(f), s = f[t] - (a[n] + a[r]));
var l = i.getBounds();
o = l[n] - (a && a[n] || 0);
var c = this.getLastControl();
if (c) {
var h = enyo.dom.getComputedBoxValue(c.hasNode(), "margin", r) || 0;
if (c != i) {
var p = c.getBounds(), d = l[n] + l[e], v = p[n] + p[e] + h;
u = v - d;
} else u = h;
}
var m = s - (o + u);
i.applyStyle(e, m + "px");
},
reflow: function() {
this.orient == "h" ? this._reflow("width", "clientWidth", "left", "right") : this._reflow("height", "clientHeight", "top", "bottom");
}
}), enyo.kind({
name: "enyo.FittableColumnsLayout",
kind: "FittableLayout",
orient: "h",
layoutClass: "enyo-fittable-columns-layout"
}), enyo.kind({
name: "enyo.FittableRowsLayout",
kind: "FittableLayout",
layoutClass: "enyo-fittable-rows-layout",
orient: "v"
});

// FittableRows.js

enyo.kind({
name: "enyo.FittableRows",
layoutKind: "FittableRowsLayout",
noStretch: !1
});

// FittableColumns.js

enyo.kind({
name: "enyo.FittableColumns",
layoutKind: "FittableColumnsLayout",
noStretch: !1
});

// FlyweightRepeater.js

enyo.kind({
name: "enyo.FlyweightRepeater",
published: {
count: 0,
noSelect: !1,
multiSelect: !1,
toggleSelected: !1,
clientClasses: "",
clientStyle: "",
rowOffset: 0
},
events: {
onSetupItem: "",
onRenderRow: ""
},
bottomUp: !1,
components: [ {
kind: "Selection",
onSelect: "selectDeselect",
onDeselect: "selectDeselect"
}, {
name: "client"
} ],
create: function() {
this.inherited(arguments), this.noSelectChanged(), this.multiSelectChanged(), this.clientClassesChanged(), this.clientStyleChanged();
},
noSelectChanged: function() {
this.noSelect && this.$.selection.clear();
},
multiSelectChanged: function() {
this.$.selection.setMulti(this.multiSelect);
},
clientClassesChanged: function() {
this.$.client.setClasses(this.clientClasses);
},
clientStyleChanged: function() {
this.$.client.setStyle(this.clientStyle);
},
setupItem: function(e) {
this.doSetupItem({
index: e,
selected: this.isSelected(e)
});
},
generateChildHtml: function() {
var e = "";
this.index = null;
for (var t = 0, n = 0; t < this.count; t++) n = this.rowOffset + (this.bottomUp ? this.count - t - 1 : t), this.setupItem(n), this.$.client.setAttribute("data-enyo-index", n), e += this.inherited(arguments), this.$.client.teardownRender();
return e;
},
previewDomEvent: function(e) {
var t = this.index = this.rowForEvent(e);
e.rowIndex = e.index = t, e.flyweight = this;
},
decorateEvent: function(e, t, n) {
var r = t && t.index != null ? t.index : this.index;
t && r != null && (t.index = r, t.flyweight = this), this.inherited(arguments);
},
tap: function(e, t) {
if (this.noSelect || t.index === -1) return;
this.toggleSelected ? this.$.selection.toggle(t.index) : this.$.selection.select(t.index);
},
selectDeselect: function(e, t) {
this.renderRow(t.key);
},
getSelection: function() {
return this.$.selection;
},
isSelected: function(e) {
return this.getSelection().isSelected(e);
},
renderRow: function(e) {
if (e < this.rowOffset || e >= this.count + this.rowOffset) return;
this.setupItem(e);
var t = this.fetchRowNode(e);
t && (enyo.dom.setInnerHtml(t, this.$.client.generateChildHtml()), this.$.client.teardownChildren(), this.doRenderRow({
rowIndex: e
}));
},
fetchRowNode: function(e) {
if (this.hasNode()) return this.node.querySelector('[data-enyo-index="' + e + '"]');
},
rowForEvent: function(e) {
if (!this.hasNode()) return -1;
var t = e.target;
while (t && t !== this.node) {
var n = t.getAttribute && t.getAttribute("data-enyo-index");
if (n !== null) return Number(n);
t = t.parentNode;
}
return -1;
},
prepareRow: function(e) {
if (e < 0 || e >= this.count) return;
this.setupItem(e);
var t = this.fetchRowNode(e);
enyo.FlyweightRepeater.claimNode(this.$.client, t);
},
lockRow: function() {
this.$.client.teardownChildren();
},
performOnRow: function(e, t, n) {
if (e < 0 || e >= this.count) return;
t && (this.prepareRow(e), enyo.call(n || null, t), this.lockRow());
},
statics: {
claimNode: function(e, t) {
var n;
t && (t.id !== e.id ? n = t.querySelector("#" + e.id) : n = t), e.generated = Boolean(n || !e.tag), e.node = n, e.node && e.rendered();
for (var r = 0, i = e.children, s; s = i[r]; r++) this.claimNode(s, t);
}
}
});

// List.js

enyo.kind({
name: "enyo.List",
kind: "Scroller",
classes: "enyo-list",
published: {
count: 0,
rowsPerPage: 50,
bottomUp: !1,
noSelect: !1,
multiSelect: !1,
toggleSelected: !1,
fixedHeight: !1,
reorderable: !1,
centerReorderContainer: !0,
reorderComponents: [],
pinnedReorderComponents: [],
swipeableComponents: [],
enableSwipe: !1,
persistSwipeableItem: !1
},
events: {
onSetupItem: "",
onSetupReorderComponents: "",
onSetupPinnedReorderComponents: "",
onReorder: "",
onSetupSwipeItem: "",
onSwipeDrag: "",
onSwipe: "",
onSwipeComplete: ""
},
handlers: {
onAnimateFinish: "animateFinish",
onRenderRow: "rowRendered",
ondragstart: "dragstart",
ondrag: "drag",
ondragfinish: "dragfinish",
onup: "up",
onholdpulse: "holdpulse"
},
rowHeight: 0,
listTools: [ {
name: "port",
classes: "enyo-list-port enyo-border-box",
components: [ {
name: "generator",
kind: "FlyweightRepeater",
canGenerate: !1,
components: [ {
tag: null,
name: "client"
} ]
}, {
name: "holdingarea",
allowHtml: !0,
classes: "enyo-list-holdingarea"
}, {
name: "page0",
allowHtml: !0,
classes: "enyo-list-page"
}, {
name: "page1",
allowHtml: !0,
classes: "enyo-list-page"
}, {
name: "placeholder"
}, {
name: "swipeableComponents",
style: "position:absolute; display:block; top:-1000px; left:0;"
} ]
} ],
reorderHoldTimeMS: 600,
draggingRowIndex: -1,
draggingRowNode: null,
placeholderRowIndex: -1,
dragToScrollThreshold: .1,
prevScrollTop: 0,
autoScrollTimeoutMS: 20,
autoScrollTimeout: null,
autoscrollPageY: 0,
pinnedReorderMode: !1,
initialPinPosition: -1,
itemMoved: !1,
currentPageNumber: -1,
completeReorderTimeout: null,
swipeIndex: null,
swipeDirection: null,
persistentItemVisible: !1,
persistentItemOrigin: null,
swipeComplete: !1,
completeSwipeTimeout: null,
completeSwipeDelayMS: 500,
normalSwipeSpeedMS: 200,
fastSwipeSpeedMS: 100,
percentageDraggedThreshold: .2,
importProps: function(e) {
e && e.reorderable && (this.touch = !0), this.inherited(arguments);
},
create: function() {
this.pageHeights = [], this.inherited(arguments), this.getStrategy().translateOptimized = !0, this.bottomUpChanged(), this.noSelectChanged(), this.multiSelectChanged(), this.toggleSelectedChanged(), this.$.generator.setRowOffset(0), this.$.generator.setCount(this.count);
},
initComponents: function() {
this.createReorderTools(), this.inherited(arguments), this.createSwipeableComponents();
},
createReorderTools: function() {
this.createComponent({
name: "reorderContainer",
classes: "enyo-list-reorder-container",
ondown: "sendToStrategy",
ondrag: "sendToStrategy",
ondragstart: "sendToStrategy",
ondragfinish: "sendToStrategy",
onflick: "sendToStrategy"
});
},
createStrategy: function() {
this.controlParentName = "strategy", this.inherited(arguments), this.createChrome(this.listTools), this.controlParentName = "client", this.discoverControlParent();
},
createSwipeableComponents: function() {
for (var e = 0; e < this.swipeableComponents.length; e++) this.$.swipeableComponents.createComponent(this.swipeableComponents[e], {
owner: this.owner
});
},
rendered: function() {
this.inherited(arguments), this.$.generator.node = this.$.port.hasNode(), this.$.generator.generated = !0, this.reset();
},
resizeHandler: function() {
this.inherited(arguments), this.refresh();
},
bottomUpChanged: function() {
this.$.generator.bottomUp = this.bottomUp, this.$.page0.applyStyle(this.pageBound, null), this.$.page1.applyStyle(this.pageBound, null), this.pageBound = this.bottomUp ? "bottom" : "top", this.hasNode() && this.reset();
},
noSelectChanged: function() {
this.$.generator.setNoSelect(this.noSelect);
},
multiSelectChanged: function() {
this.$.generator.setMultiSelect(this.multiSelect);
},
toggleSelectedChanged: function() {
this.$.generator.setToggleSelected(this.toggleSelected);
},
countChanged: function() {
this.hasNode() && this.updateMetrics();
},
sendToStrategy: function(e, t) {
this.$.strategy.dispatchEvent("on" + t.type, t, e);
},
updateMetrics: function() {
this.defaultPageHeight = this.rowsPerPage * (this.rowHeight || 100), this.pageCount = Math.ceil(this.count / this.rowsPerPage), this.portSize = 0;
for (var e = 0; e < this.pageCount; e++) this.portSize += this.getPageHeight(e);
this.adjustPortSize();
},
holdpulse: function(e, t) {
if (!this.getReorderable() || this.isReordering()) return;
if (t.holdTime >= this.reorderHoldTimeMS && this.shouldStartReordering(e, t)) return t.preventDefault(), this.startReordering(t), !1;
},
dragstart: function(e, t) {
if (this.isReordering()) return !0;
if (this.isSwipeable()) return this.swipeDragStart(e, t);
},
drag: function(e, t) {
if (this.shouldDoReorderDrag(t)) return t.preventDefault(), this.reorderDrag(t), !0;
if (this.isSwipeable()) return t.preventDefault(), this.swipeDrag(e, t), !0;
},
dragfinish: function(e, t) {
this.isReordering() ? this.finishReordering(e, t) : this.isSwipeable() && this.swipeDragFinish(e, t);
},
up: function(e, t) {
this.isReordering() && this.finishReordering(e, t);
},
generatePage: function(e, t) {
this.page = e;
var n = this.rowsPerPage * this.page;
this.$.generator.setRowOffset(n);
var r = Math.min(this.count - n, this.rowsPerPage);
this.$.generator.setCount(r);
var i = this.$.generator.generateChildHtml();
t.setContent(i), this.getReorderable() && this.draggingRowIndex > -1 && this.hideReorderingRow();
var s = t.getBounds().height;
!this.rowHeight && s > 0 && (this.rowHeight = Math.floor(s / r), this.updateMetrics());
if (!this.fixedHeight) {
var o = this.getPageHeight(e);
this.pageHeights[e] = s, this.portSize += s - o;
}
},
pageForRow: function(e) {
return Math.floor(e / this.rowsPerPage);
},
preserveDraggingRowNode: function(e) {
this.draggingRowNode && this.pageForRow(this.draggingRowIndex) === e && (this.$.holdingarea.hasNode().appendChild(this.draggingRowNode), this.draggingRowNode = null, this.removedInitialPage = !0);
},
update: function(e) {
var t = !1, n = this.positionToPageInfo(e), r = n.pos + this.scrollerHeight / 2, i = Math.floor(r / Math.max(n.height, this.scrollerHeight) + .5) + n.no, s = i % 2 === 0 ? i : i - 1;
this.p0 != s && this.isPageInRange(s) && (this.preserveDraggingRowNode(this.p0), this.generatePage(s, this.$.page0), this.positionPage(s, this.$.page0), this.p0 = s, t = !0, this.p0RowBounds = this.getPageRowHeights(this.$.page0)), s = i % 2 === 0 ? Math.max(1, i - 1) : i, this.p1 != s && this.isPageInRange(s) && (this.preserveDraggingRowNode(this.p1), this.generatePage(s, this.$.page1), this.positionPage(s, this.$.page1), this.p1 = s, t = !0, this.p1RowBounds = this.getPageRowHeights(this.$.page1)), t && (this.$.generator.setRowOffset(0), this.$.generator.setCount(this.count), this.fixedHeight || (this.adjustBottomPage(), this.adjustPortSize()));
},
getPageRowHeights: function(e) {
var t = {}, n = e.hasNode().querySelectorAll("div[data-enyo-index]");
for (var r = 0, i, s; r < n.length; r++) i = n[r].getAttribute("data-enyo-index"), i !== null && (s = enyo.dom.getBounds(n[r]), t[parseInt(i, 10)] = {
height: s.height,
width: s.width
});
return t;
},
updateRowBounds: function(e) {
this.p0RowBounds[e] ? this.updateRowBoundsAtIndex(e, this.p0RowBounds, this.$.page0) : this.p1RowBounds[e] && this.updateRowBoundsAtIndex(e, this.p1RowBounds, this.$.page1);
},
updateRowBoundsAtIndex: function(e, t, n) {
var r = n.hasNode().querySelector('div[data-enyo-index="' + e + '"]'), i = enyo.dom.getBounds(r);
t[e].height = i.height, t[e].width = i.width;
},
updateForPosition: function(e) {
this.update(this.calcPos(e));
},
calcPos: function(e) {
return this.bottomUp ? this.portSize - this.scrollerHeight - e : e;
},
adjustBottomPage: function() {
var e = this.p0 >= this.p1 ? this.$.page0 : this.$.page1;
this.positionPage(e.pageNo, e);
},
adjustPortSize: function() {
this.scrollerHeight = this.getBounds().height;
var e = Math.max(this.scrollerHeight, this.portSize);
this.$.port.applyStyle("height", e + "px");
},
positionPage: function(e, t) {
t.pageNo = e;
var n = this.pageToPosition(e);
t.applyStyle(this.pageBound, n + "px");
},
pageToPosition: function(e) {
var t = 0, n = e;
while (n > 0) n--, t += this.getPageHeight(n);
return t;
},
positionToPageInfo: function(e) {
var t = -1, n = this.calcPos(e), r = this.defaultPageHeight;
while (n >= 0) t++, r = this.getPageHeight(t), n -= r;
return t = Math.max(t, 0), {
no: t,
height: r,
pos: n + r,
startRow: t * this.rowsPerPage,
endRow: Math.min((t + 1) * this.rowsPerPage - 1, this.count - 1)
};
},
isPageInRange: function(e) {
return e == Math.max(0, Math.min(this.pageCount - 1, e));
},
getPageHeight: function(e) {
var t = this.pageHeights[e];
if (!t) {
var n = this.rowsPerPage * e, r = Math.min(this.count - n, this.rowsPerPage);
t = this.defaultPageHeight * (r / this.rowsPerPage);
}
return Math.max(1, t);
},
invalidatePages: function() {
this.p0 = this.p1 = null, this.p0RowBounds = {}, this.p1RowBounds = {}, this.$.page0.setContent(""), this.$.page1.setContent("");
},
invalidateMetrics: function() {
this.pageHeights = [], this.rowHeight = 0, this.updateMetrics();
},
scroll: function(e, t) {
var n = this.inherited(arguments), r = this.getScrollTop();
return this.lastPos === r ? n : (this.lastPos = r, this.update(r), this.pinnedReorderMode && this.reorderScroll(e, t), n);
},
setScrollTop: function(e) {
this.update(e), this.inherited(arguments), this.twiddle();
},
getScrollPosition: function() {
return this.calcPos(this.getScrollTop());
},
setScrollPosition: function(e) {
this.setScrollTop(this.calcPos(e));
},
scrollToBottom: function() {
this.update(this.getScrollBounds().maxTop), this.inherited(arguments);
},
scrollToRow: function(e) {
var t = this.pageForRow(e), n = e % this.rowsPerPage, r = this.pageToPosition(t);
this.updateForPosition(r), r = this.pageToPosition(t), this.setScrollPosition(r);
if (t == this.p0 || t == this.p1) {
var i = this.$.generator.fetchRowNode(e);
if (i) {
var s = i.offsetTop;
this.bottomUp && (s = this.getPageHeight(t) - i.offsetHeight - s);
var o = this.getScrollPosition() + s;
this.setScrollPosition(o);
}
}
},
scrollToStart: function() {
this[this.bottomUp ? "scrollToBottom" : "scrollToTop"]();
},
scrollToEnd: function() {
this[this.bottomUp ? "scrollToTop" : "scrollToBottom"]();
},
refresh: function() {
this.invalidatePages(), this.update(this.getScrollTop()), this.stabilize(), enyo.platform.android === 4 && this.twiddle();
},
reset: function() {
this.getSelection().clear(), this.invalidateMetrics(), this.invalidatePages(), this.stabilize(), this.scrollToStart();
},
getSelection: function() {
return this.$.generator.getSelection();
},
select: function(e, t) {
return this.getSelection().select(e, t);
},
deselect: function(e) {
return this.getSelection().deselect(e);
},
isSelected: function(e) {
return this.$.generator.isSelected(e);
},
renderRow: function(e) {
this.$.generator.renderRow(e);
},
rowRendered: function(e, t) {
this.updateRowBounds(t.rowIndex);
},
prepareRow: function(e) {
this.$.generator.prepareRow(e);
},
lockRow: function() {
this.$.generator.lockRow();
},
performOnRow: function(e, t, n) {
this.$.generator.performOnRow(e, t, n);
},
animateFinish: function(e) {
return this.twiddle(), !0;
},
twiddle: function() {
var e = this.getStrategy();
enyo.call(e, "twiddle");
},
pageForPageNumber: function(e, t) {
return e % 2 === 0 ? !t || e === this.p0 ? this.$.page0 : null : !t || e === this.p1 ? this.$.page1 : null;
},
shouldStartReordering: function(e, t) {
return !!this.getReorderable() && t.rowIndex >= 0 && !this.pinnedReorderMode && e === this.$.strategy && t.index >= 0 ? !0 : !1;
},
startReordering: function(e) {
this.$.strategy.listReordering = !0, this.buildReorderContainer(), this.doSetupReorderComponents(e), this.styleReorderContainer(e), this.draggingRowIndex = this.placeholderRowIndex = e.rowIndex, this.draggingRowNode = e.target, this.removedInitialPage = !1, this.itemMoved = !1, this.initialPageNumber = this.currentPageNumber = this.pageForRow(e.rowIndex), this.prevScrollTop = this.getScrollTop(), this.replaceNodeWithPlaceholder(e.rowIndex);
},
buildReorderContainer: function() {
this.$.reorderContainer.destroyClientControls();
for (var e = 0; e < this.reorderComponents.length; e++) this.$.reorderContainer.createComponent(this.reorderComponents[e], {
owner: this.owner
});
this.$.reorderContainer.render();
},
styleReorderContainer: function(e) {
this.setItemPosition(this.$.reorderContainer, e.rowIndex), this.setItemBounds(this.$.reorderContainer, e.rowIndex), this.$.reorderContainer.setShowing(!0), this.centerReorderContainer && this.centerReorderContainerOnPointer(e);
},
appendNodeToReorderContainer: function(e) {
this.$.reorderContainer.createComponent({
allowHtml: !0,
content: e.innerHTML
}).render();
},
centerReorderContainerOnPointer: function(e) {
var t = enyo.dom.calcNodePosition(this.hasNode()), n = e.pageX - t.left - parseInt(this.$.reorderContainer.domStyles.width, 10) / 2, r = e.pageY - t.top + this.getScrollTop() - parseInt(this.$.reorderContainer.domStyles.height, 10) / 2;
this.getStrategyKind() != "ScrollStrategy" && (n -= this.getScrollLeft(), r -= this.getScrollTop()), this.positionReorderContainer(n, r);
},
positionReorderContainer: function(e, t) {
this.$.reorderContainer.addClass("enyo-animatedTopAndLeft"), this.$.reorderContainer.addStyles("left:" + e + "px;top:" + t + "px;"), this.setPositionReorderContainerTimeout();
},
setPositionReorderContainerTimeout: function() {
this.clearPositionReorderContainerTimeout(), this.positionReorderContainerTimeout = setTimeout(enyo.bind(this, function() {
this.$.reorderContainer.removeClass("enyo-animatedTopAndLeft"), this.clearPositionReorderContainerTimeout();
}), 100);
},
clearPositionReorderContainerTimeout: function() {
this.positionReorderContainerTimeout && (clearTimeout(this.positionReorderContainerTimeout), this.positionReorderContainerTimeout = null);
},
shouldDoReorderDrag: function() {
return !this.getReorderable() || this.draggingRowIndex < 0 || this.pinnedReorderMode ? !1 : !0;
},
reorderDrag: function(e) {
this.positionReorderNode(e), this.checkForAutoScroll(e), this.updatePlaceholderPosition(e.pageY);
},
updatePlaceholderPosition: function(e) {
var t = this.getRowIndexFromCoordinate(e);
t !== -1 && (t >= this.placeholderRowIndex ? this.movePlaceholderToIndex(Math.min(this.count, t + 1)) : this.movePlaceholderToIndex(t));
},
positionReorderNode: function(e) {
var t = this.$.reorderContainer.getBounds(), n = t.left + e.ddx, r = t.top + e.ddy;
r = this.getStrategyKind() == "ScrollStrategy" ? r + (this.getScrollTop() - this.prevScrollTop) : r, this.$.reorderContainer.addStyles("top: " + r + "px ; left: " + n + "px"), this.prevScrollTop = this.getScrollTop();
},
checkForAutoScroll: function(e) {
var t = enyo.dom.calcNodePosition(this.hasNode()), n = this.getBounds(), r;
this.autoscrollPageY = e.pageY, e.pageY - t.top < n.height * this.dragToScrollThreshold ? (r = 100 * (1 - (e.pageY - t.top) / (n.height * this.dragToScrollThreshold)), this.scrollDistance = -1 * r) : e.pageY - t.top > n.height * (1 - this.dragToScrollThreshold) ? (r = 100 * ((e.pageY - t.top - n.height * (1 - this.dragToScrollThreshold)) / (n.height - n.height * (1 - this.dragToScrollThreshold))), this.scrollDistance = 1 * r) : this.scrollDistance = 0, this.scrollDistance === 0 ? this.stopAutoScrolling() : this.autoScrollTimeout || this.startAutoScrolling();
},
stopAutoScrolling: function() {
this.autoScrollTimeout && (clearTimeout(this.autoScrollTimeout), this.autoScrollTimeout = null);
},
startAutoScrolling: function() {
this.autoScrollTimeout = setInterval(enyo.bind(this, this.autoScroll), this.autoScrollTimeoutMS);
},
autoScroll: function() {
this.scrollDistance === 0 ? this.stopAutoScrolling() : this.autoScrollTimeout || this.startAutoScrolling(), this.setScrollPosition(this.getScrollPosition() + this.scrollDistance), this.positionReorderNode({
ddx: 0,
ddy: 0
}), this.updatePlaceholderPosition(this.autoscrollPageY);
},
movePlaceholderToIndex: function(e) {
var t, n;
if (e < 0) return;
e >= this.count ? (t = null, n = this.pageForPageNumber(this.pageForRow(this.count - 1)).hasNode()) : (t = this.$.generator.fetchRowNode(e), n = t.parentNode);
var r = this.pageForRow(e);
r >= this.pageCount && (r = this.currentPageNumber), n.insertBefore(this.placeholderNode, t), this.currentPageNumber !== r && (this.updatePageHeight(this.currentPageNumber), this.updatePageHeight(r), this.updatePagePositions(r)), this.placeholderRowIndex = e, this.currentPageNumber = r, this.itemMoved = !0;
},
finishReordering: function(e, t) {
if (!this.isReordering() || this.pinnedReorderMode || this.completeReorderTimeout) return;
return this.stopAutoScrolling(), this.$.strategy.listReordering = !1, this.moveReorderedContainerToDroppedPosition(t), this.completeReorderTimeout = setTimeout(enyo.bind(this, this.completeFinishReordering, t), 100), t.preventDefault(), !0;
},
moveReorderedContainerToDroppedPosition: function() {
var e = this.getRelativeOffset(this.placeholderNode, this.hasNode()), t = this.getStrategyKind() == "ScrollStrategy" ? e.top : e.top - this.getScrollTop(), n = e.left - this.getScrollLeft();
this.positionReorderContainer(n, t);
},
completeFinishReordering: function(e) {
this.completeReorderTimeout = null, this.placeholderRowIndex > this.draggingRowIndex && (this.placeholderRowIndex = Math.max(0, this.placeholderRowIndex - 1));
if (this.draggingRowIndex == this.placeholderRowIndex && this.pinnedReorderComponents.length && !this.pinnedReorderMode && !this.itemMoved) {
this.beginPinnedReorder(e);
return;
}
this.removeDraggingRowNode(), this.removePlaceholderNode(), this.emptyAndHideReorderContainer(), this.pinnedReorderMode = !1, this.reorderRows(e), this.draggingRowIndex = this.placeholderRowIndex = -1, this.refresh();
},
beginPinnedReorder: function(e) {
this.buildPinnedReorderContainer(), this.doSetupPinnedReorderComponents(enyo.mixin(e, {
index: this.draggingRowIndex
})), this.pinnedReorderMode = !0, this.initialPinPosition = e.pageY;
},
emptyAndHideReorderContainer: function() {
this.$.reorderContainer.destroyComponents(), this.$.reorderContainer.setShowing(!1);
},
buildPinnedReorderContainer: function() {
this.$.reorderContainer.destroyClientControls();
for (var e = 0; e < this.pinnedReorderComponents.length; e++) this.$.reorderContainer.createComponent(this.pinnedReorderComponents[e], {
owner: this.owner
});
this.$.reorderContainer.render();
},
reorderRows: function(e) {
this.doReorder(this.makeReorderEvent(e)), this.positionReorderedNode(), this.updateListIndices();
},
makeReorderEvent: function(e) {
return e.reorderFrom = this.draggingRowIndex, e.reorderTo = this.placeholderRowIndex, e;
},
positionReorderedNode: function() {
if (!this.removedInitialPage) {
var e = this.$.generator.fetchRowNode(this.placeholderRowIndex);
e && (e.parentNode.insertBefore(this.hiddenNode, e), this.showNode(this.hiddenNode)), this.hiddenNode = null;
if (this.currentPageNumber != this.initialPageNumber) {
var t, n, r = this.pageForPageNumber(this.currentPageNumber), i = this.pageForPageNumber(this.currentPageNumber + 1);
this.initialPageNumber < this.currentPageNumber ? (t = r.hasNode().firstChild, i.hasNode().appendChild(t)) : (t = r.hasNode().lastChild, n = i.hasNode().firstChild, i.hasNode().insertBefore(t, n)), this.correctPageHeights(), this.updatePagePositions(this.initialPageNumber);
}
}
},
updateListIndices: function() {
if (this.shouldDoRefresh()) {
this.refresh(), this.correctPageHeights();
return;
}
var e = Math.min(this.draggingRowIndex, this.placeholderRowIndex), t = Math.max(this.draggingRowIndex, this.placeholderRowIndex), n = this.draggingRowIndex - this.placeholderRowIndex > 0 ? 1 : -1, r, i, s, o;
if (n === 1) {
r = this.$.generator.fetchRowNode(this.draggingRowIndex), r && r.setAttribute("data-enyo-index", "reordered");
for (i = t - 1, s = t; i >= e; i--) {
r = this.$.generator.fetchRowNode(i);
if (!r) continue;
o = parseInt(r.getAttribute("data-enyo-index"), 10), s = o + 1, r.setAttribute("data-enyo-index", s);
}
r = this.hasNode().querySelector('[data-enyo-index="reordered"]'), r.setAttribute("data-enyo-index", this.placeholderRowIndex);
} else {
r = this.$.generator.fetchRowNode(this.draggingRowIndex), r && r.setAttribute("data-enyo-index", this.placeholderRowIndex);
for (i = e + 1, s = e; i <= t; i++) {
r = this.$.generator.fetchRowNode(i);
if (!r) continue;
o = parseInt(r.getAttribute("data-enyo-index"), 10), s = o - 1, r.setAttribute("data-enyo-index", s);
}
}
},
shouldDoRefresh: function() {
return Math.abs(this.initialPageNumber - this.currentPageNumber) > 1;
},
getNodeStyle: function(e) {
var t = this.$.generator.fetchRowNode(e);
if (!t) return;
var n = this.getRelativeOffset(t, this.hasNode()), r = enyo.dom.getBounds(t);
return {
h: r.height,
w: r.width,
left: n.left,
top: n.top
};
},
getRelativeOffset: function(e, t) {
var n = {
top: 0,
left: 0
};
if (e !== t && e.parentNode) do n.top += e.offsetTop || 0, n.left += e.offsetLeft || 0, e = e.offsetParent; while (e && e !== t);
return n;
},
replaceNodeWithPlaceholder: function(e) {
var t = this.$.generator.fetchRowNode(e);
if (!t) {
enyo.log("No node - " + e);
return;
}
this.placeholderNode = this.createPlaceholderNode(t), this.hiddenNode = this.hideNode(t);
var n = this.pageForPageNumber(this.currentPageNumber);
n.hasNode().insertBefore(this.placeholderNode, this.hiddenNode);
},
createPlaceholderNode: function(e) {
var t = this.$.placeholder.hasNode().cloneNode(!0), n = enyo.dom.getBounds(e);
return t.style.height = n.height + "px", t.style.width = n.width + "px", t;
},
removePlaceholderNode: function() {
this.removeNode(this.placeholderNode), this.placeholderNode = null;
},
removeDraggingRowNode: function() {
this.draggingRowNode = null;
var e = this.$.holdingarea.hasNode();
e.innerHTML = "";
},
removeNode: function(e) {
if (!e || !e.parentNode) return;
e.parentNode.removeChild(e);
},
updatePageHeight: function(e) {
if (e < 0) return;
var t = this.pageForPageNumber(e, !0);
if (t) {
var n = this.pageHeights[e], r = Math.max(1, t.getBounds().height);
this.pageHeights[e] = r, this.portSize += r - n;
}
},
updatePagePositions: function(e) {
this.positionPage(this.currentPageNumber, this.pageForPageNumber(this.currentPageNumber)), this.positionPage(e, this.pageForPageNumber(e));
},
correctPageHeights: function() {
this.updatePageHeight(this.currentPageNumber), this.initialPageNumber != this.currentPageNumber && this.updatePageHeight(this.initialPageNumber);
},
hideNode: function(e) {
return e.style.display = "none", e;
},
showNode: function(e) {
return e.style.display = "block", e;
},
dropPinnedRow: function(e) {
this.moveReorderedContainerToDroppedPosition(e), this.completeReorderTimeout = setTimeout(enyo.bind(this, this.completeFinishReordering, e), 100);
return;
},
cancelPinnedMode: function(e) {
this.placeholderRowIndex = this.draggingRowIndex, this.dropPinnedRow(e);
},
getRowIndexFromCoordinate: function(e) {
var t = this.getScrollTop() + e - enyo.dom.calcNodePosition(this.hasNode()).top;
if (t < 0) return -1;
var n = this.positionToPageInfo(t), r = n.no == this.p0 ? this.p0RowBounds : this.p1RowBounds;
if (!r) return this.count;
var i = n.pos, s = this.placeholderNode ? enyo.dom.getBounds(this.placeholderNode).height : 0, o = 0;
for (var u = n.startRow; u <= n.endRow; ++u) {
if (u === this.placeholderRowIndex) {
o += s;
if (o >= i) return -1;
}
if (u !== this.draggingRowIndex) {
o += r[u].height;
if (o >= i) return u;
}
}
return u;
},
getIndexPosition: function(e) {
return enyo.dom.calcNodePosition(this.$.generator.fetchRowNode(e));
},
setItemPosition: function(e, t) {
var n = this.getNodeStyle(t), r = this.getStrategyKind() == "ScrollStrategy" ? n.top : n.top - this.getScrollTop(), i = "top:" + r + "px; left:" + n.left + "px;";
e.addStyles(i);
},
setItemBounds: function(e, t) {
var n = this.getNodeStyle(t), r = "width:" + n.w + "px; height:" + n.h + "px;";
e.addStyles(r);
},
reorderScroll: function(e, t) {
this.getStrategyKind() == "ScrollStrategy" && this.$.reorderContainer.addStyles("top:" + (this.initialPinPosition + this.getScrollTop() - this.rowHeight) + "px;"), this.updatePlaceholderPosition(this.initialPinPosition);
},
hideReorderingRow: function() {
var e = this.hasNode().querySelector('[data-enyo-index="' + this.draggingRowIndex + '"]');
e && (this.hiddenNode = this.hideNode(e));
},
isReordering: function() {
return this.draggingRowIndex > -1;
},
isSwiping: function() {
return this.swipeIndex != null && !this.swipeComplete && this.swipeDirection != null;
},
swipeDragStart: function(e, t) {
return t.index == null || t.vertical ? !0 : (this.completeSwipeTimeout && this.completeSwipe(t), this.swipeComplete = !1, this.swipeIndex != t.index && (this.clearSwipeables(), this.swipeIndex = t.index), this.swipeDirection = t.xDirection, this.persistentItemVisible || this.startSwipe(t), this.draggedXDistance = 0, this.draggedYDistance = 0, !0);
},
swipeDrag: function(e, t) {
return this.persistentItemVisible ? (this.dragPersistentItem(t), this.preventDragPropagation) : this.isSwiping() ? (this.dragSwipeableComponents(this.calcNewDragPosition(t.ddx)), this.draggedXDistance = t.dx, this.draggedYDistance = t.dy, !0) : !1;
},
swipeDragFinish: function(e, t) {
if (this.persistentItemVisible) this.dragFinishPersistentItem(t); else {
if (!this.isSwiping()) return !1;
var n = this.calcPercentageDragged(this.draggedXDistance);
n > this.percentageDraggedThreshold && t.xDirection === this.swipeDirection ? this.swipe(this.fastSwipeSpeedMS) : this.backOutSwipe(t);
}
return this.preventDragPropagation;
},
isSwipeable: function() {
return this.enableSwipe && this.$.swipeableComponents.controls.length !== 0 && !this.isReordering() && !this.pinnedReorderMode;
},
positionSwipeableContainer: function(e, t) {
var n = this.$.generator.fetchRowNode(e);
if (!n) return;
var r = this.getRelativeOffset(n, this.hasNode()), i = enyo.dom.getBounds(n), s = t == 1 ? -1 * i.width : i.width;
this.$.swipeableComponents.addStyles("top: " + r.top + "px; left: " + s + "px; height: " + i.height + "px; width: " + i.width + "px;");
},
calcNewDragPosition: function(e) {
var t = this.$.swipeableComponents.getBounds(), n = t.left, r = this.$.swipeableComponents.getBounds(), i = this.swipeDirection == 1 ? 0 : -1 * r.width, s = this.swipeDirection == 1 ? n + e > i ? i : n + e : n + e < i ? i : n + e;
return s;
},
dragSwipeableComponents: function(e) {
this.$.swipeableComponents.applyStyle("left", e + "px");
},
startSwipe: function(e) {
e.index = this.swipeIndex, this.positionSwipeableContainer(this.swipeIndex, e.xDirection), this.$.swipeableComponents.setShowing(!0), this.setPersistentItemOrigin(e.xDirection), this.doSetupSwipeItem(e);
},
dragPersistentItem: function(e) {
var t = 0, n = this.persistentItemOrigin == "right" ? Math.max(t, t + e.dx) : Math.min(t, t + e.dx);
this.$.swipeableComponents.applyStyle("left", n + "px");
},
dragFinishPersistentItem: function(e) {
var t = this.calcPercentageDragged(e.dx) > .2, n = e.dx > 0 ? "right" : e.dx < 0 ? "left" : null;
this.persistentItemOrigin == n ? t ? this.slideAwayItem() : this.bounceItem(e) : this.bounceItem(e);
},
setPersistentItemOrigin: function(e) {
this.persistentItemOrigin = e == 1 ? "left" : "right";
},
calcPercentageDragged: function(e) {
return Math.abs(e / this.$.swipeableComponents.getBounds().width);
},
swipe: function(e) {
this.swipeComplete = !0, this.animateSwipe(0, e);
},
backOutSwipe: function(e) {
var t = this.$.swipeableComponents.getBounds(), n = this.swipeDirection == 1 ? -1 * t.width : t.width;
this.animateSwipe(n, this.fastSwipeSpeedMS), this.swipeDirection = null;
},
bounceItem: function(e) {
var t = this.$.swipeableComponents.getBounds();
t.left != t.width && this.animateSwipe(0, this.normalSwipeSpeedMS);
},
slideAwayItem: function() {
var e = this.$.swipeableComponents, t = e.getBounds().width, n = this.persistentItemOrigin == "left" ? -1 * t : t;
this.animateSwipe(n, this.normalSwipeSpeedMS), this.persistentItemVisible = !1, this.setPersistSwipeableItem(!1);
},
clearSwipeables: function() {
this.$.swipeableComponents.setShowing(!1), this.persistentItemVisible = !1, this.setPersistSwipeableItem(!1);
},
completeSwipe: function(e) {
this.completeSwipeTimeout && (clearTimeout(this.completeSwipeTimeout), this.completeSwipeTimeout = null), this.getPersistSwipeableItem() ? this.persistentItemVisible = !0 : (this.$.swipeableComponents.setShowing(!1), this.swipeComplete && this.doSwipeComplete({
index: this.swipeIndex,
xDirection: this.swipeDirection
})), this.swipeIndex = null, this.swipeDirection = null;
},
animateSwipe: function(e, t) {
var n = enyo.now(), r = 0, i = this.$.swipeableComponents, s = parseInt(i.domStyles.left, 10), o = e - s;
this.stopAnimateSwipe();
var u = enyo.bind(this, function() {
var e = enyo.now() - n, r = e / t, a = s + o * Math.min(r, 1);
i.applyStyle("left", a + "px"), this.job = enyo.requestAnimationFrame(u), e / t >= 1 && (this.stopAnimateSwipe(), this.completeSwipeTimeout = setTimeout(enyo.bind(this, function() {
this.completeSwipe();
}), this.completeSwipeDelayMS));
});
this.job = enyo.requestAnimationFrame(u);
},
stopAnimateSwipe: function() {
this.job && (this.job = enyo.cancelRequestAnimationFrame(this.job));
}
});

// PulldownList.js

enyo.kind({
name: "enyo.PulldownList",
kind: "List",
touch: !0,
pully: null,
pulldownTools: [ {
name: "pulldown",
classes: "enyo-list-pulldown",
components: [ {
name: "puller",
kind: "Puller"
} ]
} ],
events: {
onPullStart: "",
onPullCancel: "",
onPull: "",
onPullRelease: "",
onPullComplete: ""
},
handlers: {
onScrollStart: "scrollStartHandler",
onScrollStop: "scrollStopHandler",
ondragfinish: "dragfinish"
},
pullingMessage: "Pull down to refresh...",
pulledMessage: "Release to refresh...",
loadingMessage: "Loading...",
pullingIconClass: "enyo-puller-arrow enyo-puller-arrow-down",
pulledIconClass: "enyo-puller-arrow enyo-puller-arrow-up",
loadingIconClass: "",
create: function() {
var e = {
kind: "Puller",
showing: !1,
text: this.loadingMessage,
iconClass: this.loadingIconClass,
onCreate: "setPully"
};
this.listTools.splice(0, 0, e), this.inherited(arguments), this.setPulling();
},
initComponents: function() {
this.createChrome(this.pulldownTools), this.accel = enyo.dom.canAccelerate(), this.translation = this.accel ? "translate3d" : "translate", this.strategyKind = this.resetStrategyKind(), this.inherited(arguments);
},
resetStrategyKind: function() {
return enyo.platform.android >= 3 ? "TranslateScrollStrategy" : "TouchScrollStrategy";
},
setPully: function(e, t) {
this.pully = t.originator;
},
scrollStartHandler: function() {
this.firedPullStart = !1, this.firedPull = !1, this.firedPullCancel = !1;
},
scroll: function(e, t) {
var n = this.inherited(arguments);
this.completingPull && this.pully.setShowing(!1);
var r = this.getStrategy().$.scrollMath || this.getStrategy(), i = -1 * this.getScrollTop();
return r.isInOverScroll() && i > 0 && (enyo.dom.transformValue(this.$.pulldown, this.translation, "0," + i + "px" + (this.accel ? ",0" : "")), this.firedPullStart || (this.firedPullStart = !0, this.pullStart(), this.pullHeight = this.$.pulldown.getBounds().height), i > this.pullHeight && !this.firedPull && (this.firedPull = !0, this.firedPullCancel = !1, this.pull()), this.firedPull && !this.firedPullCancel && i < this.pullHeight && (this.firedPullCancel = !0, this.firedPull = !1, this.pullCancel())), n;
},
scrollStopHandler: function() {
this.completingPull && (this.completingPull = !1, this.doPullComplete());
},
dragfinish: function() {
if (this.firedPull) {
var e = this.getStrategy().$.scrollMath || this.getStrategy();
e.setScrollY(-1 * this.getScrollTop() - this.pullHeight), this.pullRelease();
}
},
completePull: function() {
this.completingPull = !0;
var e = this.getStrategy().$.scrollMath || this.getStrategy();
e.setScrollY(this.pullHeight), e.start();
},
pullStart: function() {
this.setPulling(), this.pully.setShowing(!1), this.$.puller.setShowing(!0), this.doPullStart();
},
pull: function() {
this.setPulled(), this.doPull();
},
pullCancel: function() {
this.setPulling(), this.doPullCancel();
},
pullRelease: function() {
this.$.puller.setShowing(!1), this.pully.setShowing(!0), this.doPullRelease();
},
setPulling: function() {
this.$.puller.setText(this.pullingMessage), this.$.puller.setIconClass(this.pullingIconClass);
},
setPulled: function() {
this.$.puller.setText(this.pulledMessage), this.$.puller.setIconClass(this.pulledIconClass);
}
}), enyo.kind({
name: "enyo.Puller",
classes: "enyo-puller",
published: {
text: "",
iconClass: ""
},
events: {
onCreate: ""
},
components: [ {
name: "icon"
}, {
name: "text",
tag: "span",
classes: "enyo-puller-text"
} ],
create: function() {
this.inherited(arguments), this.doCreate(), this.textChanged(), this.iconClassChanged();
},
textChanged: function() {
this.$.text.setContent(this.text);
},
iconClassChanged: function() {
this.$.icon.setClasses(this.iconClass);
}
});

// AroundList.js

enyo.kind({
name: "enyo.AroundList",
kind: "enyo.List",
listTools: [ {
name: "port",
classes: "enyo-list-port enyo-border-box",
components: [ {
name: "aboveClient"
}, {
name: "generator",
kind: "FlyweightRepeater",
canGenerate: !1,
components: [ {
tag: null,
name: "client"
} ]
}, {
name: "holdingarea",
allowHtml: !0,
classes: "enyo-list-holdingarea"
}, {
name: "page0",
allowHtml: !0,
classes: "enyo-list-page"
}, {
name: "page1",
allowHtml: !0,
classes: "enyo-list-page"
}, {
name: "belowClient"
}, {
name: "placeholder"
}, {
name: "swipeableComponents",
style: "position:absolute; display:block; top:-1000px; left:0px;"
} ]
} ],
aboveComponents: null,
initComponents: function() {
this.inherited(arguments), this.aboveComponents && this.$.aboveClient.createComponents(this.aboveComponents, {
owner: this.owner
}), this.belowComponents && this.$.belowClient.createComponents(this.belowComponents, {
owner: this.owner
});
},
updateMetrics: function() {
this.defaultPageHeight = this.rowsPerPage * (this.rowHeight || 100), this.pageCount = Math.ceil(this.count / this.rowsPerPage), this.aboveHeight = this.$.aboveClient.getBounds().height, this.belowHeight = this.$.belowClient.getBounds().height, this.portSize = this.aboveHeight + this.belowHeight;
for (var e = 0; e < this.pageCount; e++) this.portSize += this.getPageHeight(e);
this.adjustPortSize();
},
positionPage: function(e, t) {
t.pageNo = e;
var n = this.pageToPosition(e), r = this.bottomUp ? this.belowHeight : this.aboveHeight;
n += r, t.applyStyle(this.pageBound, n + "px");
},
scrollToContentStart: function() {
var e = this.bottomUp ? this.belowHeight : this.aboveHeight;
this.setScrollPosition(e);
}
});

// Slideable.js

enyo.kind({
name: "enyo.Slideable",
kind: "Control",
published: {
axis: "h",
value: 0,
unit: "px",
min: 0,
max: 0,
accelerated: "auto",
overMoving: !0,
draggable: !0
},
events: {
onAnimateFinish: "",
onChange: ""
},
preventDragPropagation: !1,
tools: [ {
kind: "Animator",
onStep: "animatorStep",
onEnd: "animatorComplete"
} ],
handlers: {
ondragstart: "dragstart",
ondrag: "drag",
ondragfinish: "dragfinish"
},
kDragScalar: 1,
dragEventProp: "dx",
unitModifier: !1,
canTransform: !1,
create: function() {
this.inherited(arguments), this.acceleratedChanged(), this.transformChanged(), this.axisChanged(), this.valueChanged(), this.addClass("enyo-slideable");
},
initComponents: function() {
this.createComponents(this.tools), this.inherited(arguments);
},
rendered: function() {
this.inherited(arguments), this.canModifyUnit(), this.updateDragScalar();
},
resizeHandler: function() {
this.inherited(arguments), this.updateDragScalar();
},
canModifyUnit: function() {
if (!this.canTransform) {
var e = this.getInitialStyleValue(this.hasNode(), this.boundary);
e.match(/px/i) && this.unit === "%" && (this.unitModifier = this.getBounds()[this.dimension]);
}
},
getInitialStyleValue: function(e, t) {
var n = enyo.dom.getComputedStyle(e);
return n ? n.getPropertyValue(t) : e && e.currentStyle ? e.currentStyle[t] : "0";
},
updateBounds: function(e, t) {
var n = {};
n[this.boundary] = e, this.setBounds(n, this.unit), this.setInlineStyles(e, t);
},
updateDragScalar: function() {
if (this.unit == "%") {
var e = this.getBounds()[this.dimension];
this.kDragScalar = e ? 100 / e : 1, this.canTransform || this.updateBounds(this.value, 100);
}
},
transformChanged: function() {
this.canTransform = enyo.dom.canTransform();
},
acceleratedChanged: function() {
enyo.platform.android > 2 || enyo.dom.accelerate(this, this.accelerated);
},
axisChanged: function() {
var e = this.axis == "h";
this.dragMoveProp = e ? "dx" : "dy", this.shouldDragProp = e ? "horizontal" : "vertical", this.transform = e ? "translateX" : "translateY", this.dimension = e ? "width" : "height", this.boundary = e ? "left" : "top";
},
setInlineStyles: function(e, t) {
var n = {};
this.unitModifier ? (n[this.boundary] = this.percentToPixels(e, this.unitModifier), n[this.dimension] = this.unitModifier, this.setBounds(n)) : (t ? n[this.dimension] = t : n[this.boundary] = e, this.setBounds(n, this.unit));
},
valueChanged: function(e) {
var t = this.value;
this.isOob(t) && !this.isAnimating() && (this.value = this.overMoving ? this.dampValue(t) : this.clampValue(t)), enyo.platform.android > 2 && (this.value ? (e === 0 || e === undefined) && enyo.dom.accelerate(this, this.accelerated) : enyo.dom.accelerate(this, !1)), this.canTransform ? enyo.dom.transformValue(this, this.transform, this.value + this.unit) : this.setInlineStyles(this.value, !1), this.doChange();
},
getAnimator: function() {
return this.$.animator;
},
isAtMin: function() {
return this.value <= this.calcMin();
},
isAtMax: function() {
return this.value >= this.calcMax();
},
calcMin: function() {
return this.min;
},
calcMax: function() {
return this.max;
},
clampValue: function(e) {
var t = this.calcMin(), n = this.calcMax();
return Math.max(t, Math.min(e, n));
},
dampValue: function(e) {
return this.dampBound(this.dampBound(e, this.min, 1), this.max, -1);
},
dampBound: function(e, t, n) {
var r = e;
return r * n < t * n && (r = t + (r - t) / 4), r;
},
percentToPixels: function(e, t) {
return Math.floor(t / 100 * e);
},
pixelsToPercent: function(e) {
var t = this.unitModifier ? this.getBounds()[this.dimension] : this.container.getBounds()[this.dimension];
return e / t * 100;
},
shouldDrag: function(e) {
return this.draggable && e[this.shouldDragProp];
},
isOob: function(e) {
return e > this.calcMax() || e < this.calcMin();
},
dragstart: function(e, t) {
if (this.shouldDrag(t)) return t.preventDefault(), this.$.animator.stop(), t.dragInfo = {}, this.dragging = !0, this.drag0 = this.value, this.dragd0 = 0, this.preventDragPropagation;
},
drag: function(e, t) {
if (this.dragging) {
t.preventDefault();
var n = this.canTransform ? t[this.dragMoveProp] * this.kDragScalar : this.pixelsToPercent(t[this.dragMoveProp]), r = this.drag0 + n, i = n - this.dragd0;
return this.dragd0 = n, i && (t.dragInfo.minimizing = i < 0), this.setValue(r), this.preventDragPropagation;
}
},
dragfinish: function(e, t) {
if (this.dragging) return this.dragging = !1, this.completeDrag(t), t.preventTap(), this.preventDragPropagation;
},
completeDrag: function(e) {
this.value !== this.calcMax() && this.value != this.calcMin() && this.animateToMinMax(e.dragInfo.minimizing);
},
isAnimating: function() {
return this.$.animator.isAnimating();
},
play: function(e, t) {
this.$.animator.play({
startValue: e,
endValue: t,
node: this.hasNode()
});
},
animateTo: function(e) {
this.play(this.value, e);
},
animateToMin: function() {
this.animateTo(this.calcMin());
},
animateToMax: function() {
this.animateTo(this.calcMax());
},
animateToMinMax: function(e) {
e ? this.animateToMin() : this.animateToMax();
},
animatorStep: function(e) {
return this.setValue(e.value), !0;
},
animatorComplete: function(e) {
return this.doAnimateFinish(e), !0;
},
toggleMinMax: function() {
this.animateToMinMax(!this.isAtMin());
}
});

// Arranger.js

enyo.kind({
name: "enyo.Arranger",
kind: "Layout",
layoutClass: "enyo-arranger",
accelerated: "auto",
dragProp: "ddx",
dragDirectionProp: "xDirection",
canDragProp: "horizontal",
incrementalPoints: !1,
destroy: function() {
var e = this.container.getPanels();
for (var t = 0, n; n = e[t]; t++) n._arranger = null;
this.inherited(arguments);
},
arrange: function(e, t) {},
size: function() {},
start: function() {
var e = this.container.fromIndex, t = this.container.toIndex, n = this.container.transitionPoints = [ e ];
if (this.incrementalPoints) {
var r = Math.abs(t - e) - 2, i = e;
while (r >= 0) i += t < e ? -1 : 1, n.push(i), r--;
}
n.push(this.container.toIndex);
},
finish: function() {},
calcArrangementDifference: function(e, t, n, r) {},
canDragEvent: function(e) {
return e[this.canDragProp];
},
calcDragDirection: function(e) {
return e[this.dragDirectionProp];
},
calcDrag: function(e) {
return e[this.dragProp];
},
drag: function(e, t, n, r, i) {
var s = this.measureArrangementDelta(-e, t, n, r, i);
return s;
},
measureArrangementDelta: function(e, t, n, r, i) {
var s = this.calcArrangementDifference(t, n, r, i), o = s ? e / Math.abs(s) : 0;
return o *= this.container.fromIndex > this.container.toIndex ? -1 : 1, o;
},
_arrange: function(e) {
this.containerBounds || this.reflow();
var t = this.getOrderedControls(e);
this.arrange(t, e);
},
arrangeControl: function(e, t) {
e._arranger = enyo.mixin(e._arranger || {}, t);
},
flow: function() {
this.c$ = [].concat(this.container.getPanels()), this.controlsIndex = 0;
for (var e = 0, t = this.container.getPanels(), n; n = t[e]; e++) {
enyo.dom.accelerate(n, this.accelerated);
if (enyo.platform.safari) {
var r = n.children;
for (var i = 0, s; s = r[i]; i++) enyo.dom.accelerate(s, this.accelerated);
}
}
},
reflow: function() {
var e = this.container.hasNode();
this.containerBounds = e ? {
width: e.clientWidth,
height: e.clientHeight
} : {}, this.size();
},
flowArrangement: function() {
var e = this.container.arrangement;
if (e) for (var t = 0, n = this.container.getPanels(), r; r = n[t]; t++) this.flowControl(r, e[t]);
},
flowControl: function(e, t) {
enyo.Arranger.positionControl(e, t);
var n = t.opacity;
n != null && enyo.Arranger.opacifyControl(e, n);
},
getOrderedControls: function(e) {
var t = Math.floor(e), n = t - this.controlsIndex, r = n > 0, i = this.c$ || [];
for (var s = 0; s < Math.abs(n); s++) r ? i.push(i.shift()) : i.unshift(i.pop());
return this.controlsIndex = t, i;
},
statics: {
positionControl: function(e, t, n) {
var r = n || "px";
if (!this.updating) if (enyo.dom.canTransform() && !enyo.platform.android && enyo.platform.ie !== 10) {
var i = t.left, s = t.top;
i = enyo.isString(i) ? i : i && i + r, s = enyo.isString(s) ? s : s && s + r, enyo.dom.transform(e, {
translateX: i || null,
translateY: s || null
});
} else e.setBounds(t, n);
},
opacifyControl: function(e, t) {
var n = t;
n = n > .99 ? 1 : n < .01 ? 0 : n, enyo.platform.ie < 9 ? e.applyStyle("filter", "progid:DXImageTransform.Microsoft.Alpha(Opacity=" + n * 100 + ")") : e.applyStyle("opacity", n);
}
}
});

// CardArranger.js

enyo.kind({
name: "enyo.CardArranger",
kind: "Arranger",
layoutClass: "enyo-arranger enyo-arranger-fit",
calcArrangementDifference: function(e, t, n, r) {
return this.containerBounds.width;
},
arrange: function(e, t) {
for (var n = 0, r, i, s; r = e[n]; n++) s = n === 0 ? 1 : 0, this.arrangeControl(r, {
opacity: s
});
},
start: function() {
this.inherited(arguments);
var e = this.container.getPanels();
for (var t = 0, n; n = e[t]; t++) {
var r = n.showing;
n.setShowing(t == this.container.fromIndex || t == this.container.toIndex), n.showing && !r && n.resized();
}
},
finish: function() {
this.inherited(arguments);
var e = this.container.getPanels();
for (var t = 0, n; n = e[t]; t++) n.setShowing(t == this.container.toIndex);
},
destroy: function() {
var e = this.container.getPanels();
for (var t = 0, n; n = e[t]; t++) enyo.Arranger.opacifyControl(n, 1), n.showing || n.setShowing(!0);
this.inherited(arguments);
}
});

// CardSlideInArranger.js

enyo.kind({
name: "enyo.CardSlideInArranger",
kind: "CardArranger",
start: function() {
var e = this.container.getPanels();
for (var t = 0, n; n = e[t]; t++) {
var r = n.showing;
n.setShowing(t == this.container.fromIndex || t == this.container.toIndex), n.showing && !r && n.resized();
}
var i = this.container.fromIndex;
t = this.container.toIndex, this.container.transitionPoints = [ t + "." + i + ".s", t + "." + i + ".f" ];
},
finish: function() {
this.inherited(arguments);
var e = this.container.getPanels();
for (var t = 0, n; n = e[t]; t++) n.setShowing(t == this.container.toIndex);
},
arrange: function(e, t) {
var n = t.split("."), r = n[0], i = n[1], s = n[2] == "s", o = this.containerBounds.width;
for (var u = 0, a = this.container.getPanels(), f, l; f = a[u]; u++) l = o, i == u && (l = s ? 0 : -o), r == u && (l = s ? o : 0), i == u && i == r && (l = 0), this.arrangeControl(f, {
left: l
});
},
destroy: function() {
var e = this.container.getPanels();
for (var t = 0, n; n = e[t]; t++) enyo.Arranger.positionControl(n, {
left: null
});
this.inherited(arguments);
}
});

// CarouselArranger.js

enyo.kind({
name: "enyo.CarouselArranger",
kind: "Arranger",
size: function() {
var e = this.container.getPanels(), t = this.containerPadding = this.container.hasNode() ? enyo.dom.calcPaddingExtents(this.container.node) : {}, n = this.containerBounds, r, i, s, o, u;
n.height -= t.top + t.bottom, n.width -= t.left + t.right;
var a;
for (r = 0, s = 0; u = e[r]; r++) o = enyo.dom.calcMarginExtents(u.hasNode()), u.width = u.getBounds().width, u.marginWidth = o.right + o.left, s += (u.fit ? 0 : u.width) + u.marginWidth, u.fit && (a = u);
if (a) {
var f = n.width - s;
a.width = f >= 0 ? f : a.width;
}
for (r = 0, i = t.left; u = e[r]; r++) u.setBounds({
top: t.top,
bottom: t.bottom,
width: u.fit ? u.width : null
});
},
arrange: function(e, t) {
this.container.wrap ? this.arrangeWrap(e, t) : this.arrangeNoWrap(e, t);
},
arrangeNoWrap: function(e, t) {
var n, r, i, s, o = this.container.getPanels(), u = this.container.clamp(t), a = this.containerBounds.width;
for (n = u, i = 0; s = o[n]; n++) {
i += s.width + s.marginWidth;
if (i > a) break;
}
var f = a - i, l = 0;
if (f > 0) {
var c = u;
for (n = u - 1, r = 0; s = o[n]; n--) {
r += s.width + s.marginWidth;
if (f - r <= 0) {
l = f - r, u = n;
break;
}
}
}
var h, p;
for (n = 0, p = this.containerPadding.left + l; s = o[n]; n++) h = s.width + s.marginWidth, n < u ? this.arrangeControl(s, {
left: -h
}) : (this.arrangeControl(s, {
left: Math.floor(p)
}), p += h);
},
arrangeWrap: function(e, t) {
for (var n = 0, r = this.containerPadding.left, i, s; s = e[n]; n++) this.arrangeControl(s, {
left: r
}), r += s.width + s.marginWidth;
},
calcArrangementDifference: function(e, t, n, r) {
var i = Math.abs(e % this.c$.length);
return t[i].left - r[i].left;
},
destroy: function() {
var e = this.container.getPanels();
for (var t = 0, n; n = e[t]; t++) enyo.Arranger.positionControl(n, {
left: null,
top: null
}), n.applyStyle("top", null), n.applyStyle("bottom", null), n.applyStyle("left", null), n.applyStyle("width", null);
this.inherited(arguments);
}
});

// CollapsingArranger.js

enyo.kind({
name: "enyo.CollapsingArranger",
kind: "CarouselArranger",
peekWidth: 0,
size: function() {
this.clearLastSize(), this.inherited(arguments);
},
clearLastSize: function() {
for (var e = 0, t = this.container.getPanels(), n; n = t[e]; e++) n._fit && e != t.length - 1 && (n.applyStyle("width", null), n._fit = null);
},
constructor: function() {
this.inherited(arguments), this.peekWidth = this.container.peekWidth != null ? this.container.peekWidth : this.peekWidth;
},
arrange: function(e, t) {
var n = this.container.getPanels();
for (var r = 0, i = this.containerPadding.left, s, o, u = 0; o = n[r]; r++) o.getShowing() ? (this.arrangeControl(o, {
left: i + u * this.peekWidth
}), r >= t && (i += o.width + o.marginWidth - this.peekWidth), u++) : (this.arrangeControl(o, {
left: i
}), r >= t && (i += o.width + o.marginWidth)), r == n.length - 1 && t < 0 && this.arrangeControl(o, {
left: i - t
});
},
calcArrangementDifference: function(e, t, n, r) {
var i = this.container.getPanels().length - 1;
return Math.abs(r[i].left - t[i].left);
},
flowControl: function(e, t) {
this.inherited(arguments);
if (this.container.realtimeFit) {
var n = this.container.getPanels(), r = n.length - 1, i = n[r];
e == i && this.fitControl(e, t.left);
}
},
finish: function() {
this.inherited(arguments);
if (!this.container.realtimeFit && this.containerBounds) {
var e = this.container.getPanels(), t = this.container.arrangement, n = e.length - 1, r = e[n];
this.fitControl(r, t[n].left);
}
},
fitControl: function(e, t) {
e._fit = !0, e.applyStyle("width", this.containerBounds.width - t + "px"), e.resized();
}
});

// DockRightArranger.js

enyo.kind({
name: "enyo.DockRightArranger",
kind: "Arranger",
basePanel: !1,
overlap: 0,
layoutWidth: 0,
constructor: function() {
this.inherited(arguments), this.overlap = this.container.overlap != null ? this.container.overlap : this.overlap, this.layoutWidth = this.container.layoutWidth != null ? this.container.layoutWidth : this.layoutWidth;
},
size: function() {
var e = this.container.getPanels(), t = this.containerPadding = this.container.hasNode() ? enyo.dom.calcPaddingExtents(this.container.node) : {}, n = this.containerBounds, r, i, s;
n.width -= t.left + t.right;
var o = n.width, u = e.length;
this.container.transitionPositions = {};
for (r = 0; s = e[r]; r++) s.width = r === 0 && this.container.basePanel ? o : s.getBounds().width;
for (r = 0; s = e[r]; r++) {
r === 0 && this.container.basePanel && s.setBounds({
width: o
}), s.setBounds({
top: t.top,
bottom: t.bottom
});
for (j = 0; s = e[j]; j++) {
var a;
if (r === 0 && this.container.basePanel) a = 0; else if (j < r) a = o; else {
if (r !== j) break;
var f = o > this.layoutWidth ? this.overlap : 0;
a = o - e[r].width + f;
}
this.container.transitionPositions[r + "." + j] = a;
}
if (j < u) {
var l = !1;
for (k = r + 1; k < u; k++) {
var f = 0;
if (l) f = 0; else if (e[r].width + e[k].width - this.overlap > o) f = 0, l = !0; else {
f = e[r].width - this.overlap;
for (i = r; i < k; i++) {
var c = f + e[i + 1].width - this.overlap;
if (!(c < o)) {
f = o;
break;
}
f = c;
}
f = o - f;
}
this.container.transitionPositions[r + "." + k] = f;
}
}
}
},
arrange: function(e, t) {
var n, r, i = this.container.getPanels(), s = this.container.clamp(t);
for (n = 0; r = i[n]; n++) {
var o = this.container.transitionPositions[n + "." + s];
this.arrangeControl(r, {
left: o
});
}
},
calcArrangementDifference: function(e, t, n, r) {
var i = this.container.getPanels(), s = e < n ? i[n].width : i[e].width;
return s;
},
destroy: function() {
var e = this.container.getPanels();
for (var t = 0, n; n = e[t]; t++) enyo.Arranger.positionControl(n, {
left: null,
top: null
}), n.applyStyle("top", null), n.applyStyle("bottom", null), n.applyStyle("left", null), n.applyStyle("width", null);
this.inherited(arguments);
}
});

// OtherArrangers.js

enyo.kind({
name: "enyo.LeftRightArranger",
kind: "Arranger",
margin: 40,
axisSize: "width",
offAxisSize: "height",
axisPosition: "left",
constructor: function() {
this.inherited(arguments), this.margin = this.container.margin != null ? this.container.margin : this.margin;
},
size: function() {
var e = this.container.getPanels(), t = this.containerBounds[this.axisSize], n = t - this.margin - this.margin;
for (var r = 0, i, s; s = e[r]; r++) i = {}, i[this.axisSize] = n, i[this.offAxisSize] = "100%", s.setBounds(i);
},
start: function() {
this.inherited(arguments);
var e = this.container.fromIndex, t = this.container.toIndex, n = this.getOrderedControls(t), r = Math.floor(n.length / 2);
for (var i = 0, s; s = n[i]; i++) e > t ? i == n.length - r ? s.applyStyle("z-index", 0) : s.applyStyle("z-index", 1) : i == n.length - 1 - r ? s.applyStyle("z-index", 0) : s.applyStyle("z-index", 1);
},
arrange: function(e, t) {
var n, r, i, s;
if (this.container.getPanels().length == 1) {
s = {}, s[this.axisPosition] = this.margin, this.arrangeControl(this.container.getPanels()[0], s);
return;
}
var o = Math.floor(this.container.getPanels().length / 2), u = this.getOrderedControls(Math.floor(t) - o), a = this.containerBounds[this.axisSize] - this.margin - this.margin, f = this.margin - a * o;
for (n = 0; r = u[n]; n++) s = {}, s[this.axisPosition] = f, this.arrangeControl(r, s), f += a;
},
calcArrangementDifference: function(e, t, n, r) {
if (this.container.getPanels().length == 1) return 0;
var i = Math.abs(e % this.c$.length);
return t[i][this.axisPosition] - r[i][this.axisPosition];
},
destroy: function() {
var e = this.container.getPanels();
for (var t = 0, n; n = e[t]; t++) enyo.Arranger.positionControl(n, {
left: null,
top: null
}), enyo.Arranger.opacifyControl(n, 1), n.applyStyle("left", null), n.applyStyle("top", null), n.applyStyle("height", null), n.applyStyle("width", null);
this.inherited(arguments);
}
}), enyo.kind({
name: "enyo.TopBottomArranger",
kind: "LeftRightArranger",
dragProp: "ddy",
dragDirectionProp: "yDirection",
canDragProp: "vertical",
axisSize: "height",
offAxisSize: "width",
axisPosition: "top"
}), enyo.kind({
name: "enyo.SpiralArranger",
kind: "Arranger",
incrementalPoints: !0,
inc: 20,
size: function() {
var e = this.container.getPanels(), t = this.containerBounds, n = this.controlWidth = t.width / 3, r = this.controlHeight = t.height / 3;
for (var i = 0, s; s = e[i]; i++) s.setBounds({
width: n,
height: r
});
},
arrange: function(e, t) {
var n = this.inc;
for (var r = 0, i = e.length, s; s = e[r]; r++) {
var o = Math.cos(r / i * 2 * Math.PI) * r * n + this.controlWidth, u = Math.sin(r / i * 2 * Math.PI) * r * n + this.controlHeight;
this.arrangeControl(s, {
left: o,
top: u
});
}
},
start: function() {
this.inherited(arguments);
var e = this.getOrderedControls(this.container.toIndex);
for (var t = 0, n; n = e[t]; t++) n.applyStyle("z-index", e.length - t);
},
calcArrangementDifference: function(e, t, n, r) {
return this.controlWidth;
},
destroy: function() {
var e = this.container.getPanels();
for (var t = 0, n; n = e[t]; t++) n.applyStyle("z-index", null), enyo.Arranger.positionControl(n, {
left: null,
top: null
}), n.applyStyle("left", null), n.applyStyle("top", null), n.applyStyle("height", null), n.applyStyle("width", null);
this.inherited(arguments);
}
}), enyo.kind({
name: "enyo.GridArranger",
kind: "Arranger",
incrementalPoints: !0,
colWidth: 100,
colHeight: 100,
size: function() {
var e = this.container.getPanels(), t = this.colWidth, n = this.colHeight;
for (var r = 0, i; i = e[r]; r++) i.setBounds({
width: t,
height: n
});
},
arrange: function(e, t) {
var n = this.colWidth, r = this.colHeight, i = Math.max(1, Math.floor(this.containerBounds.width / n)), s;
for (var o = 0, u = 0; u < e.length; o++) for (var a = 0; a < i && (s = e[u]); a++, u++) this.arrangeControl(s, {
left: n * a,
top: r * o
});
},
flowControl: function(e, t) {
this.inherited(arguments), enyo.Arranger.opacifyControl(e, t.top % this.colHeight !== 0 ? .25 : 1);
},
calcArrangementDifference: function(e, t, n, r) {
return this.colWidth;
},
destroy: function() {
var e = this.container.getPanels();
for (var t = 0, n; n = e[t]; t++) enyo.Arranger.positionControl(n, {
left: null,
top: null
}), n.applyStyle("left", null), n.applyStyle("top", null), n.applyStyle("height", null), n.applyStyle("width", null);
this.inherited(arguments);
}
});

// Panels.js

enyo.kind({
name: "enyo.Panels",
classes: "enyo-panels",
published: {
index: 0,
draggable: !0,
animate: !0,
wrap: !1,
arrangerKind: "CardArranger",
narrowFit: !0
},
events: {
onTransitionStart: "",
onTransitionFinish: ""
},
handlers: {
ondragstart: "dragstart",
ondrag: "drag",
ondragfinish: "dragfinish",
onscroll: "domScroll"
},
tools: [ {
kind: "Animator",
onStep: "step",
onEnd: "completed"
} ],
fraction: 0,
create: function() {
this.transitionPoints = [], this.inherited(arguments), this.arrangerKindChanged(), this.narrowFitChanged(), this.indexChanged();
},
rendered: function() {
this.inherited(arguments), enyo.makeBubble(this, "scroll");
},
domScroll: function(e, t) {
this.hasNode() && this.node.scrollLeft > 0 && (this.node.scrollLeft = 0);
},
initComponents: function() {
this.createChrome(this.tools), this.inherited(arguments);
},
arrangerKindChanged: function() {
this.setLayoutKind(this.arrangerKind);
},
narrowFitChanged: function() {
this.addRemoveClass("enyo-panels-fit-narrow", this.narrowFit);
},
destroy: function() {
this.destroying = !0, this.inherited(arguments);
},
removeControl: function(e) {
this.inherited(arguments), this.destroying && this.controls.length > 0 && this.isPanel(e) && (this.setIndex(Math.max(this.index - 1, 0)), this.flow(), this.reflow());
},
isPanel: function() {
return !0;
},
flow: function() {
this.arrangements = [], this.inherited(arguments);
},
reflow: function() {
this.arrangements = [], this.inherited(arguments), this.refresh();
},
getPanels: function() {
var e = this.controlParent || this;
return e.children;
},
getActive: function() {
var e = this.getPanels(), t = this.index % e.length;
return t < 0 && (t += e.length), e[t];
},
getAnimator: function() {
return this.$.animator;
},
setIndex: function(e) {
this.setPropertyValue("index", e, "indexChanged");
},
setIndexDirect: function(e) {
this.setIndex(e), this.completed();
},
previous: function() {
this.setIndex(this.index - 1);
},
next: function() {
this.setIndex(this.index + 1);
},
clamp: function(e) {
var t = this.getPanels().length - 1;
return this.wrap ? e : Math.max(0, Math.min(e, t));
},
indexChanged: function(e) {
this.lastIndex = e, this.index = this.clamp(this.index), !this.dragging && this.$.animator && (this.$.animator.isAnimating() && this.completed(), this.$.animator.stop(), this.hasNode() && (this.animate ? (this.startTransition(), this.$.animator.play({
startValue: this.fraction
})) : this.refresh()));
},
step: function(e) {
this.fraction = e.value, this.stepTransition();
},
completed: function() {
this.$.animator.isAnimating() && this.$.animator.stop(), this.fraction = 1, this.stepTransition(), this.finishTransition();
},
dragstart: function(e, t) {
if (this.draggable && this.layout && this.layout.canDragEvent(t)) return t.preventDefault(), this.dragstartTransition(t), this.dragging = !0, this.$.animator.stop(), !0;
},
drag: function(e, t) {
this.dragging && (t.preventDefault(), this.dragTransition(t));
},
dragfinish: function(e, t) {
this.dragging && (this.dragging = !1, t.preventTap(), this.dragfinishTransition(t));
},
dragstartTransition: function(e) {
if (!this.$.animator.isAnimating()) {
var t = this.fromIndex = this.index;
this.toIndex = t - (this.layout ? this.layout.calcDragDirection(e) : 0);
} else this.verifyDragTransition(e);
this.fromIndex = this.clamp(this.fromIndex), this.toIndex = this.clamp(this.toIndex), this.fireTransitionStart(), this.layout && this.layout.start();
},
dragTransition: function(e) {
var t = this.layout ? this.layout.calcDrag(e) : 0, n = this.transitionPoints, r = n[0], i = n[n.length - 1], s = this.fetchArrangement(r), o = this.fetchArrangement(i), u = this.layout ? this.layout.drag(t, r, s, i, o) : 0, a = t && !u;
a, this.fraction += u;
var f = this.fraction;
if (f > 1 || f < 0 || a) (f > 0 || a) && this.dragfinishTransition(e), this.dragstartTransition(e), this.fraction = 0;
this.stepTransition();
},
dragfinishTransition: function(e) {
this.verifyDragTransition(e), this.setIndex(this.toIndex), this.dragging && this.fireTransitionFinish();
},
verifyDragTransition: function(e) {
var t = this.layout ? this.layout.calcDragDirection(e) : 0, n = Math.min(this.fromIndex, this.toIndex), r = Math.max(this.fromIndex, this.toIndex);
if (t > 0) {
var i = n;
n = r, r = i;
}
n != this.fromIndex && (this.fraction = 1 - this.fraction), this.fromIndex = n, this.toIndex = r;
},
refresh: function() {
this.$.animator && this.$.animator.isAnimating() && this.$.animator.stop(), this.startTransition(), this.fraction = 1, this.stepTransition(), this.finishTransition();
},
startTransition: function() {
this.fromIndex = this.fromIndex != null ? this.fromIndex : this.lastIndex || 0, this.toIndex = this.toIndex != null ? this.toIndex : this.index, this.layout && this.layout.start(), this.fireTransitionStart();
},
finishTransition: function() {
this.layout && this.layout.finish(), this.transitionPoints = [], this.fraction = 0, this.fromIndex = this.toIndex = null, this.fireTransitionFinish();
},
fireTransitionStart: function() {
var e = this.startTransitionInfo;
this.hasNode() && (!e || e.fromIndex != this.fromIndex || e.toIndex != this.toIndex) && (this.startTransitionInfo = {
fromIndex: this.fromIndex,
toIndex: this.toIndex
}, this.doTransitionStart(enyo.clone(this.startTransitionInfo)));
},
fireTransitionFinish: function() {
var e = this.finishTransitionInfo;
this.hasNode() && (!e || e.fromIndex != this.lastIndex || e.toIndex != this.index) && (this.finishTransitionInfo = {
fromIndex: this.lastIndex,
toIndex: this.index
}, this.doTransitionFinish(enyo.clone(this.finishTransitionInfo))), this.lastIndex = this.index;
},
stepTransition: function() {
if (this.hasNode()) {
var e = this.transitionPoints, t = (this.fraction || 0) * (e.length - 1), n = Math.floor(t);
t -= n;
var r = e[n], i = e[n + 1], s = this.fetchArrangement(r), o = this.fetchArrangement(i);
this.arrangement = s && o ? enyo.Panels.lerp(s, o, t) : s || o, this.arrangement && this.layout && this.layout.flowArrangement();
}
},
fetchArrangement: function(e) {
return e != null && !this.arrangements[e] && this.layout && (this.layout._arrange(e), this.arrangements[e] = this.readArrangement(this.getPanels())), this.arrangements[e];
},
readArrangement: function(e) {
var t = [];
for (var n = 0, r = e, i; i = r[n]; n++) t.push(enyo.clone(i._arranger));
return t;
},
statics: {
isScreenNarrow: function() {
return enyo.dom.getWindowWidth() <= 800;
},
lerp: function(e, t, n) {
var r = [];
for (var i = 0, s = enyo.keys(e), o; o = s[i]; i++) r.push(this.lerpObject(e[o], t[o], n));
return r;
},
lerpObject: function(e, t, n) {
var r = enyo.clone(e), i, s;
if (t) for (var o in e) i = e[o], s = t[o], i != s && (r[o] = i - (i - s) * n);
return r;
}
}
});

// Node.js

enyo.kind({
name: "enyo.Node",
published: {
expandable: !1,
expanded: !1,
icon: "",
onlyIconExpands: !1,
selected: !1
},
style: "padding: 0 0 0 16px;",
content: "Node",
defaultKind: "Node",
classes: "enyo-node",
components: [ {
name: "icon",
kind: "Image",
showing: !1
}, {
kind: "Control",
name: "caption",
Xtag: "span",
style: "display: inline-block; padding: 4px;",
allowHtml: !0
}, {
kind: "Control",
name: "extra",
tag: "span",
allowHtml: !0
} ],
childClient: [ {
kind: "Control",
name: "box",
classes: "enyo-node-box",
Xstyle: "border: 1px solid orange;",
components: [ {
kind: "Control",
name: "client",
classes: "enyo-node-client",
Xstyle: "border: 1px solid lightblue;"
} ]
} ],
handlers: {
ondblclick: "dblclick"
},
events: {
onNodeTap: "nodeTap",
onNodeDblClick: "nodeDblClick",
onExpand: "nodeExpand",
onDestroyed: "nodeDestroyed"
},
create: function() {
this.inherited(arguments), this.selectedChanged(), this.iconChanged();
},
destroy: function() {
this.doDestroyed(), this.inherited(arguments);
},
initComponents: function() {
this.expandable && (this.kindComponents = this.kindComponents.concat(this.childClient)), this.inherited(arguments);
},
contentChanged: function() {
this.$.caption.setContent(this.content);
},
iconChanged: function() {
this.$.icon.setSrc(this.icon), this.$.icon.setShowing(Boolean(this.icon));
},
selectedChanged: function() {
this.addRemoveClass("enyo-selected", this.selected);
},
rendered: function() {
this.inherited(arguments), this.expandable && !this.expanded && this.quickCollapse();
},
addNodes: function(e) {
this.destroyClientControls();
for (var t = 0, n; n = e[t]; t++) this.createComponent(n);
this.$.client.render();
},
addTextNodes: function(e) {
this.destroyClientControls();
for (var t = 0, n; n = e[t]; t++) this.createComponent({
content: n
});
this.$.client.render();
},
tap: function(e, t) {
return this.onlyIconExpands ? t.target == this.$.icon.hasNode() ? this.toggleExpanded() : this.doNodeTap() : (this.toggleExpanded(), this.doNodeTap()), !0;
},
dblclick: function(e, t) {
return this.doNodeDblClick(), !0;
},
toggleExpanded: function() {
this.setExpanded(!this.expanded);
},
quickCollapse: function() {
this.removeClass("enyo-animate"), this.$.box.applyStyle("height", "0");
var e = this.$.client.getBounds().height;
this.$.client.setBounds({
top: -e
});
},
_expand: function() {
this.addClass("enyo-animate");
var e = this.$.client.getBounds().height;
this.$.box.setBounds({
height: e
}), this.$.client.setBounds({
top: 0
}), setTimeout(enyo.bind(this, function() {
this.expanded && (this.removeClass("enyo-animate"), this.$.box.applyStyle("height", "auto"));
}), 225);
},
_collapse: function() {
this.removeClass("enyo-animate");
var e = this.$.client.getBounds().height;
this.$.box.setBounds({
height: e
}), setTimeout(enyo.bind(this, function() {
this.addClass("enyo-animate"), this.$.box.applyStyle("height", "0"), this.$.client.setBounds({
top: -e
});
}), 25);
},
expandedChanged: function(e) {
if (!this.expandable) this.expanded = !1; else {
var t = {
expanded: this.expanded
};
this.doExpand(t), t.wait || this.effectExpanded();
}
},
effectExpanded: function() {
this.$.client && (this.expanded ? this._expand() : this._collapse());
}
});

// ImageViewPin.js

enyo.kind({
name: "enyo.ImageViewPin",
kind: "enyo.Control",
published: {
highlightAnchorPoint: !1,
anchor: {
top: 0,
left: 0
},
position: {
top: 0,
left: 0
}
},
style: "position:absolute;z-index:1000;width:0px;height:0px;",
handlers: {
onPositionPin: "reAnchor"
},
create: function() {
this.inherited(arguments), this.styleClientControls(), this.positionClientControls(), this.highlightAnchorPointChanged(), this.anchorChanged();
},
styleClientControls: function() {
var e = this.getClientControls();
for (var t = 0; t < e.length; t++) e[t].applyStyle("position", "absolute");
},
positionClientControls: function() {
var e = this.getClientControls();
for (var t = 0; t < e.length; t++) for (var n in this.position) e[t].applyStyle(n, this.position[n] + "px");
},
highlightAnchorPointChanged: function() {
this.addRemoveClass("pinDebug", this.highlightAnchorPoint);
},
anchorChanged: function() {
var e = null, t = null;
for (t in this.anchor) {
e = this.anchor[t].toString().match(/^(\d+(?:\.\d+)?)(.*)$/);
if (!e) continue;
this.anchor[t + "Coords"] = {
value: e[1],
units: e[2] || "px"
};
}
},
reAnchor: function(e, t) {
var n = t.scale, r = t.bounds, i = this.anchor.right ? this.anchor.rightCoords.units == "px" ? r.width + r.x - this.anchor.rightCoords.value * n : r.width * (100 - this.anchor.rightCoords.value) / 100 + r.x : this.anchor.leftCoords.units == "px" ? this.anchor.leftCoords.value * n + r.x : r.width * this.anchor.leftCoords.value / 100 + r.x, s = this.anchor.bottom ? this.anchor.bottomCoords.units == "px" ? r.height + r.y - this.anchor.bottomCoords.value * n : r.height * (100 - this.anchor.bottomCoords.value) / 100 + r.y : this.anchor.topCoords.units == "px" ? this.anchor.topCoords.value * n + r.y : r.height * this.anchor.topCoords.value / 100 + r.y;
this.applyStyle("left", i + "px"), this.applyStyle("top", s + "px");
}
});

// ImageView.js

enyo.kind({
name: "enyo.ImageView",
kind: enyo.Scroller,
touchOverscroll: !1,
thumb: !1,
animate: !0,
verticalDragPropagation: !0,
horizontalDragPropagation: !0,
published: {
scale: "auto",
disableZoom: !1,
src: undefined
},
events: {
onZoom: ""
},
touch: !0,
preventDragPropagation: !1,
handlers: {
ondragstart: "dragPropagation"
},
components: [ {
name: "animator",
kind: "Animator",
onStep: "zoomAnimationStep",
onEnd: "zoomAnimationEnd"
}, {
name: "viewport",
style: "overflow:hidden;min-height:100%;min-width:100%;",
classes: "enyo-fit",
ongesturechange: "gestureTransform",
ongestureend: "saveState",
ontap: "singleTap",
ondblclick: "doubleClick",
onmousewheel: "mousewheel",
components: [ {
kind: "Image",
ondown: "down"
} ]
} ],
create: function() {
this.inherited(arguments), this.canTransform = enyo.dom.canTransform(), this.canTransform || this.$.image.applyStyle("position", "relative"), this.canAccelerate = enyo.dom.canAccelerate(), this.bufferImage = new Image, this.bufferImage.onload = enyo.bind(this, "imageLoaded"), this.bufferImage.onerror = enyo.bind(this, "imageError"), this.srcChanged(), this.getStrategy().setDragDuringGesture(!1), this.getStrategy().$.scrollMath && this.getStrategy().$.scrollMath.start();
},
down: function(e, t) {
t.preventDefault();
},
dragPropagation: function(e, t) {
var n = this.getStrategy().getScrollBounds(), r = n.top === 0 && t.dy > 0 || n.top >= n.maxTop - 2 && t.dy < 0, i = n.left === 0 && t.dx > 0 || n.left >= n.maxLeft - 2 && t.dx < 0;
return !(r && this.verticalDragPropagation || i && this.horizontalDragPropagation);
},
mousewheel: function(e, t) {
t.pageX |= t.clientX + t.target.scrollLeft, t.pageY |= t.clientY + t.target.scrollTop;
var n = (this.maxScale - this.minScale) / 10, r = this.scale;
if (t.wheelDelta > 0 || t.detail < 0) this.scale = this.limitScale(this.scale + n); else if (t.wheelDelta < 0 || t.detail > 0) this.scale = this.limitScale(this.scale - n);
return this.eventPt = this.calcEventLocation(t), this.transformImage(this.scale), r != this.scale && this.doZoom({
scale: this.scale
}), this.ratioX = this.ratioY = null, t.preventDefault(), !0;
},
srcChanged: function() {
this.src && this.src.length > 0 && this.bufferImage && this.src != this.bufferImage.src && (this.bufferImage.src = this.src);
},
imageLoaded: function(e) {
this.originalWidth = this.bufferImage.width, this.originalHeight = this.bufferImage.height, this.scaleChanged(), this.$.image.setSrc(this.bufferImage.src), enyo.dom.transformValue(this.getStrategy().$.client, "translate3d", "0px, 0px, 0"), this.positionClientControls(this.scale), this.alignImage();
},
resizeHandler: function() {
this.inherited(arguments), this.$.image.src && this.scaleChanged();
},
scaleChanged: function() {
var e = this.hasNode();
if (e) {
this.containerWidth = e.clientWidth, this.containerHeight = e.clientHeight;
var t = this.containerWidth / this.originalWidth, n = this.containerHeight / this.originalHeight;
this.minScale = Math.min(t, n), this.maxScale = this.minScale * 3 < 1 ? 1 : this.minScale * 3, this.scale == "auto" ? this.scale = this.minScale : this.scale == "width" ? this.scale = t : this.scale == "height" ? this.scale = n : this.scale == "fit" ? (this.fitAlignment = "center", this.scale = Math.max(t, n)) : (this.maxScale = Math.max(this.maxScale, this.scale), this.scale = this.limitScale(this.scale));
}
this.eventPt = this.calcEventLocation(), this.transformImage(this.scale);
},
imageError: function(e) {
enyo.error("Error loading image: " + this.src), this.bubble("onerror", e);
},
alignImage: function() {
if (this.fitAlignment && this.fitAlignment === "center") {
var e = this.getScrollBounds();
this.setScrollLeft(e.maxLeft / 2), this.setScrollTop(e.maxTop / 2);
}
},
gestureTransform: function(e, t) {
this.eventPt = this.calcEventLocation(t), this.transformImage(this.limitScale(this.scale * t.scale));
},
calcEventLocation: function(e) {
var t = {
x: 0,
y: 0
};
if (e && this.hasNode()) {
var n = this.node.getBoundingClientRect();
t.x = Math.round(e.pageX - n.left - this.imageBounds.x), t.x = Math.max(0, Math.min(this.imageBounds.width, t.x)), t.y = Math.round(e.pageY - n.top - this.imageBounds.y), t.y = Math.max(0, Math.min(this.imageBounds.height, t.y));
}
return t;
},
transformImage: function(e) {
this.tapped = !1;
var t = this.imageBounds || this.innerImageBounds(e);
this.imageBounds = this.innerImageBounds(e), this.scale > this.minScale ? this.$.viewport.applyStyle("cursor", "move") : this.$.viewport.applyStyle("cursor", null), this.$.viewport.setBounds({
width: this.imageBounds.width + "px",
height: this.imageBounds.height + "px"
}), this.ratioX = this.ratioX || (this.eventPt.x + this.getScrollLeft()) / t.width, this.ratioY = this.ratioY || (this.eventPt.y + this.getScrollTop()) / t.height;
var n, r;
this.$.animator.ratioLock ? (n = this.$.animator.ratioLock.x * this.imageBounds.width - this.containerWidth / 2, r = this.$.animator.ratioLock.y * this.imageBounds.height - this.containerHeight / 2) : (n = this.ratioX * this.imageBounds.width - this.eventPt.x, r = this.ratioY * this.imageBounds.height - this.eventPt.y), n = Math.max(0, Math.min(this.imageBounds.width - this.containerWidth, n)), r = Math.max(0, Math.min(this.imageBounds.height - this.containerHeight, r));
if (this.canTransform) {
var i = {
scale: e
};
this.canAccelerate ? i = enyo.mixin({
translate3d: Math.round(this.imageBounds.left) + "px, " + Math.round(this.imageBounds.top) + "px, 0px"
}, i) : i = enyo.mixin({
translate: this.imageBounds.left + "px, " + this.imageBounds.top + "px"
}, i), enyo.dom.transform(this.$.image, i);
} else this.$.image.setBounds({
width: this.imageBounds.width + "px",
height: this.imageBounds.height + "px",
left: this.imageBounds.left + "px",
top: this.imageBounds.top + "px"
});
this.setScrollLeft(n), this.setScrollTop(r), this.positionClientControls(e);
},
limitScale: function(e) {
return this.disableZoom ? e = this.scale : e > this.maxScale ? e = this.maxScale : e < this.minScale && (e = this.minScale), e;
},
innerImageBounds: function(e) {
var t = this.originalWidth * e, n = this.originalHeight * e, r = {
x: 0,
y: 0,
transX: 0,
transY: 0
};
return t < this.containerWidth && (r.x += (this.containerWidth - t) / 2), n < this.containerHeight && (r.y += (this.containerHeight - n) / 2), this.canTransform && (r.transX -= (this.originalWidth - t) / 2, r.transY -= (this.originalHeight - n) / 2), {
left: r.x + r.transX,
top: r.y + r.transY,
width: t,
height: n,
x: r.x,
y: r.y
};
},
saveState: function(e, t) {
var n = this.scale;
this.scale *= t.scale, this.scale = this.limitScale(this.scale), n != this.scale && this.doZoom({
scale: this.scale
}), this.ratioX = this.ratioY = null;
},
doubleClick: function(e, t) {
enyo.platform.ie == 8 && (this.tapped = !0, t.pageX = t.clientX + t.target.scrollLeft, t.pageY = t.clientY + t.target.scrollTop, this.singleTap(e, t), t.preventDefault());
},
singleTap: function(e, t) {
setTimeout(enyo.bind(this, function() {
this.tapped = !1;
}), 300), this.tapped ? (this.tapped = !1, this.smartZoom(e, t)) : this.tapped = !0;
},
smartZoom: function(e, t) {
var n = this.hasNode(), r = this.$.image.hasNode();
if (n && r && this.hasNode() && !this.disableZoom) {
var i = this.scale;
this.scale != this.minScale ? this.scale = this.minScale : this.scale = this.maxScale, this.eventPt = this.calcEventLocation(t);
if (this.animate) {
var s = {
x: (this.eventPt.x + this.getScrollLeft()) / this.imageBounds.width,
y: (this.eventPt.y + this.getScrollTop()) / this.imageBounds.height
};
this.$.animator.play({
duration: 350,
ratioLock: s,
baseScale: i,
deltaScale: this.scale - i
});
} else this.transformImage(this.scale), this.doZoom({
scale: this.scale
});
}
},
zoomAnimationStep: function(e, t) {
var n = this.$.animator.baseScale + this.$.animator.deltaScale * this.$.animator.value;
this.transformImage(n);
},
zoomAnimationEnd: function(e, t) {
this.doZoom({
scale: this.scale
}), this.$.animator.ratioLock = undefined;
},
positionClientControls: function(e) {
this.waterfallDown("onPositionPin", {
scale: e,
bounds: this.imageBounds
});
}
});

// ImageCarousel.js

enyo.kind({
name: "enyo.ImageCarousel",
kind: enyo.Panels,
arrangerKind: "enyo.CarouselArranger",
defaultScale: "auto",
disableZoom: !1,
lowMemory: !1,
published: {
images: []
},
handlers: {
onTransitionStart: "transitionStart",
onTransitionFinish: "transitionFinish"
},
create: function() {
this.inherited(arguments), this.imageCount = this.images.length, this.images.length > 0 && (this.initContainers(), this.loadNearby());
},
initContainers: function() {
for (var e = 0; e < this.images.length; e++) this.$["container" + e] || (this.createComponent({
name: "container" + e,
style: "height:100%; width:100%;"
}), this.$["container" + e].render());
for (e = this.images.length; e < this.imageCount; e++) this.$["image" + e] && this.$["image" + e].destroy(), this.$["container" + e].destroy();
this.imageCount = this.images.length;
},
loadNearby: function() {
var e = this.getBufferRange();
for (var t in e) this.loadImageView(e[t]);
},
getBufferRange: function() {
var e = [];
if (this.layout.containerBounds) {
var t = 1, n = this.layout.containerBounds, r, i, s, o, u, a;
o = this.index - 1, u = 0, a = n.width * t;
while (o >= 0 && u <= a) s = this.$["container" + o], u += s.width + s.marginWidth, e.unshift(o), o--;
o = this.index, u = 0, a = n.width * (t + 1);
while (o < this.images.length && u <= a) s = this.$["container" + o], u += s.width + s.marginWidth, e.push(o), o++;
}
return e;
},
reflow: function() {
this.inherited(arguments), this.loadNearby();
},
loadImageView: function(e) {
return this.wrap && (e = (e % this.images.length + this.images.length) % this.images.length), e >= 0 && e <= this.images.length - 1 && (this.$["image" + e] ? this.$["image" + e].src != this.images[e] && (this.$["image" + e].setSrc(this.images[e]), this.$["image" + e].setScale(this.defaultScale), this.$["image" + e].setDisableZoom(this.disableZoom)) : (this.$["container" + e].createComponent({
name: "image" + e,
kind: "ImageView",
scale: this.defaultScale,
disableZoom: this.disableZoom,
src: this.images[e],
verticalDragPropagation: !1,
style: "height:100%; width:100%;"
}, {
owner: this
}), this.$["image" + e].render())), this.$["image" + e];
},
setImages: function(e) {
this.setPropertyValue("images", e, "imagesChanged");
},
imagesChanged: function() {
this.initContainers(), this.loadNearby();
},
indexChanged: function() {
this.loadNearby(), this.lowMemory && this.cleanupMemory(), this.inherited(arguments);
},
transitionStart: function(e, t) {
if (t.fromIndex == t.toIndex) return !0;
},
transitionFinish: function(e, t) {
this.loadNearby(), this.lowMemory && this.cleanupMemory();
},
getActiveImage: function() {
return this.getImageByIndex(this.index);
},
getImageByIndex: function(e) {
return this.$["image" + e] || this.loadImageView(e);
},
cleanupMemory: function() {
var e = getBufferRange();
for (var t = 0; t < this.images.length; t++) enyo.indexOf(t, e) === -1 && this.$["image" + t] && this.$["image" + t].destroy();
}
});

// Icon.js

enyo.kind({
name: "onyx.Icon",
published: {
src: "",
disabled: !1
},
classes: "onyx-icon",
create: function() {
this.inherited(arguments), this.src && this.srcChanged(), this.disabledChanged();
},
disabledChanged: function() {
this.addRemoveClass("disabled", this.disabled);
},
srcChanged: function() {
this.applyStyle("background-image", "url(" + enyo.path.rewrite(this.src) + ")");
}
});

// Button.js

enyo.kind({
name: "onyx.Button",
kind: "enyo.Button",
classes: "onyx-button enyo-unselectable",
create: function() {
enyo.platform.firefoxOS && (this.handlers.ondown = "down", this.handlers.onleave = "leave"), this.inherited(arguments);
},
down: function(e, t) {
this.addClass("pressed");
},
leave: function(e, t) {
this.removeClass("pressed");
}
});

// IconButton.js

enyo.kind({
name: "onyx.IconButton",
kind: "onyx.Icon",
published: {
active: !1
},
classes: "onyx-icon-button",
create: function() {
enyo.platform.firefoxOS && (this.handlers.ondown = "down", this.handlers.onleave = "leave"), this.inherited(arguments);
},
down: function(e, t) {
this.addClass("pressed");
},
leave: function(e, t) {
this.removeClass("pressed");
},
rendered: function() {
this.inherited(arguments), this.activeChanged();
},
tap: function() {
if (this.disabled) return !0;
this.setActive(!0);
},
activeChanged: function() {
this.bubble("onActivate");
}
});

// Checkbox.js

enyo.kind({
name: "onyx.Checkbox",
classes: "onyx-checkbox",
kind: enyo.Checkbox,
tag: "div",
handlers: {
onclick: ""
},
tap: function(e, t) {
return this.disabled || (this.setChecked(!this.getChecked()), this.bubble("onchange")), !this.disabled;
},
dragstart: function() {}
});

// Drawer.js

enyo.kind({
name: "onyx.Drawer",
published: {
open: !0,
orient: "v",
animated: !0
},
style: "overflow: hidden; position: relative;",
tools: [ {
kind: "Animator",
onStep: "animatorStep",
onEnd: "animatorEnd"
}, {
name: "client",
style: "position: relative;",
classes: "enyo-border-box"
} ],
create: function() {
this.inherited(arguments), this.animatedChanged(), this.openChanged();
},
initComponents: function() {
this.createChrome(this.tools), this.inherited(arguments);
},
animatedChanged: function() {
!this.animated && this.hasNode() && this.$.animator.isAnimating() && (this.$.animator.stop(), this.animatorEnd());
},
openChanged: function() {
this.$.client.show();
if (this.hasNode()) if (this.$.animator.isAnimating()) this.$.animator.reverse(); else {
var e = this.orient == "v", t = e ? "height" : "width", n = e ? "top" : "left";
this.applyStyle(t, null);
var r = this.hasNode()[e ? "scrollHeight" : "scrollWidth"];
this.animated ? this.$.animator.play({
startValue: this.open ? 0 : r,
endValue: this.open ? r : 0,
dimension: t,
position: n
}) : this.animatorEnd();
} else this.$.client.setShowing(this.open);
},
animatorStep: function(e) {
if (this.hasNode()) {
var t = e.dimension;
this.node.style[t] = this.domStyles[t] = e.value + "px";
}
var n = this.$.client.hasNode();
if (n) {
var r = e.position, i = this.open ? e.endValue : e.startValue;
n.style[r] = this.$.client.domStyles[r] = e.value - i + "px";
}
this.container && this.container.resized();
},
animatorEnd: function() {
if (!this.open) this.$.client.hide(); else {
this.$.client.domCssText = enyo.Control.domStylesToCssText(this.$.client.domStyles);
var e = this.orient == "v", t = e ? "height" : "width", n = e ? "top" : "left", r = this.$.client.hasNode();
r && (r.style[n] = this.$.client.domStyles[n] = null), this.node && (this.node.style[t] = this.domStyles[t] = null);
}
this.container && this.container.resized();
}
});

// Grabber.js

enyo.kind({
name: "onyx.Grabber",
classes: "onyx-grabber"
});

// Groupbox.js

enyo.kind({
name: "onyx.Groupbox",
classes: "onyx-groupbox"
}), enyo.kind({
name: "onyx.GroupboxHeader",
classes: "onyx-groupbox-header"
});

// Input.js

enyo.kind({
name: "onyx.Input",
kind: "enyo.Input",
classes: "onyx-input"
});

// Popup.js

enyo.kind({
name: "onyx.Popup",
kind: "Popup",
classes: "onyx-popup",
published: {
scrimWhenModal: !0,
scrim: !1,
scrimClassName: ""
},
statics: {
count: 0
},
defaultZ: 120,
showingChanged: function() {
this.showing ? (onyx.Popup.count++, this.applyZIndex()) : onyx.Popup.count > 0 && onyx.Popup.count--, this.showHideScrim(this.showing), this.inherited(arguments);
},
showHideScrim: function(e) {
if (this.floating && (this.scrim || this.modal && this.scrimWhenModal)) {
var t = this.getScrim();
if (e) {
var n = this.getScrimZIndex();
this._scrimZ = n, t.showAtZIndex(n);
} else t.hideAtZIndex(this._scrimZ);
enyo.call(t, "addRemoveClass", [ this.scrimClassName, t.showing ]);
}
},
getScrimZIndex: function() {
return this.findZIndex() - 1;
},
getScrim: function() {
return this.modal && this.scrimWhenModal && !this.scrim ? onyx.scrimTransparent.make() : onyx.scrim.make();
},
applyZIndex: function() {
this._zIndex = onyx.Popup.count * 2 + this.findZIndex() + 1, this.applyStyle("z-index", this._zIndex);
},
findZIndex: function() {
var e = this.defaultZ;
return this._zIndex ? e = this._zIndex : this.hasNode() && (e = Number(enyo.dom.getComputedStyleValue(this.node, "z-index")) || e), this._zIndex = e;
}
});

// TextArea.js

enyo.kind({
name: "onyx.TextArea",
kind: "enyo.TextArea",
classes: "onyx-textarea"
});

// RichText.js

enyo.kind({
name: "onyx.RichText",
kind: "enyo.RichText",
classes: "onyx-richtext"
});

// InputDecorator.js

enyo.kind({
name: "onyx.InputDecorator",
kind: "enyo.ToolDecorator",
tag: "label",
classes: "onyx-input-decorator",
published: {
alwaysLooksFocused: !1
},
handlers: {
onDisabledChange: "disabledChange",
onfocus: "receiveFocus",
onblur: "receiveBlur"
},
create: function() {
this.inherited(arguments), this.updateFocus(!1);
},
alwaysLooksFocusedChanged: function(e) {
this.updateFocus(this.focus);
},
updateFocus: function(e) {
this.focused = e, this.addRemoveClass("onyx-focused", this.alwaysLooksFocused || this.focused);
},
receiveFocus: function() {
this.updateFocus(!0);
},
receiveBlur: function() {
this.updateFocus(!1);
},
disabledChange: function(e, t) {
this.addRemoveClass("onyx-disabled", t.originator.disabled);
}
});

// Tooltip.js

enyo.kind({
name: "onyx.Tooltip",
kind: "onyx.Popup",
classes: "onyx-tooltip below left-arrow",
autoDismiss: !1,
showDelay: 500,
defaultLeft: -6,
handlers: {
onRequestShowTooltip: "requestShow",
onRequestHideTooltip: "requestHide"
},
requestShow: function() {
return this.showJob = setTimeout(enyo.bind(this, "show"), this.showDelay), !0;
},
cancelShow: function() {
clearTimeout(this.showJob);
},
requestHide: function() {
return this.cancelShow(), this.inherited(arguments);
},
showingChanged: function() {
this.cancelShow(), this.adjustPosition(!0), this.inherited(arguments);
},
applyPosition: function(e) {
var t = "";
for (var n in e) t += n + ":" + e[n] + (isNaN(e[n]) ? "; " : "px; ");
this.addStyles(t);
},
adjustPosition: function(e) {
if (this.showing && this.hasNode()) {
var t = this.node.getBoundingClientRect();
t.top + t.height > window.innerHeight ? (this.addRemoveClass("below", !1), this.addRemoveClass("above", !0)) : (this.addRemoveClass("above", !1), this.addRemoveClass("below", !0)), t.left + t.width > window.innerWidth && (this.applyPosition({
"margin-left": -t.width,
bottom: "auto"
}), this.addRemoveClass("left-arrow", !1), this.addRemoveClass("right-arrow", !0));
}
},
resizeHandler: function() {
this.applyPosition({
"margin-left": this.defaultLeft,
bottom: "auto"
}), this.addRemoveClass("left-arrow", !0), this.addRemoveClass("right-arrow", !1), this.adjustPosition(!0), this.inherited(arguments);
}
});

// TooltipDecorator.js

enyo.kind({
name: "onyx.TooltipDecorator",
defaultKind: "onyx.Button",
classes: "onyx-popup-decorator",
handlers: {
onenter: "enter",
onleave: "leave"
},
enter: function() {
this.requestShowTooltip();
},
leave: function() {
this.requestHideTooltip();
},
tap: function() {
this.requestHideTooltip();
},
requestShowTooltip: function() {
this.waterfallDown("onRequestShowTooltip");
},
requestHideTooltip: function() {
this.waterfallDown("onRequestHideTooltip");
}
});

// MenuDecorator.js

enyo.kind({
name: "onyx.MenuDecorator",
kind: "onyx.TooltipDecorator",
defaultKind: "onyx.Button",
classes: "onyx-popup-decorator enyo-unselectable",
handlers: {
onActivate: "activated",
onHide: "menuHidden"
},
activated: function(e, t) {
this.requestHideTooltip(), t.originator.active && (this.menuActive = !0, this.activator = t.originator, this.activator.addClass("active"), this.requestShowMenu());
},
requestShowMenu: function() {
this.waterfallDown("onRequestShowMenu", {
activator: this.activator
});
},
requestHideMenu: function() {
this.waterfallDown("onRequestHideMenu");
},
menuHidden: function() {
this.menuActive = !1, this.activator && (this.activator.setActive(!1), this.activator.removeClass("active"));
},
enter: function(e) {
this.menuActive || this.inherited(arguments);
},
leave: function(e, t) {
this.menuActive || this.inherited(arguments);
}
});

// Menu.js

enyo.kind({
name: "onyx.Menu",
kind: "onyx.Popup",
modal: !0,
defaultKind: "onyx.MenuItem",
classes: "onyx-menu",
published: {
maxHeight: 200,
scrolling: !0
},
handlers: {
onActivate: "itemActivated",
onRequestShowMenu: "requestMenuShow",
onRequestHideMenu: "requestHide"
},
childComponents: [ {
name: "client",
kind: "enyo.Scroller",
strategyKind: "TouchScrollStrategy"
} ],
showOnTop: !1,
scrollerName: "client",
create: function() {
this.inherited(arguments), this.maxHeightChanged();
},
initComponents: function() {
this.scrolling && this.createComponents(this.childComponents, {
isChrome: !0
}), this.inherited(arguments);
},
getScroller: function() {
return this.$[this.scrollerName];
},
maxHeightChanged: function() {
this.scrolling && this.getScroller().setMaxHeight(this.maxHeight + "px");
},
itemActivated: function(e, t) {
return t.originator.setActive(!1), !0;
},
showingChanged: function() {
this.inherited(arguments), this.scrolling && this.getScroller().setShowing(this.showing), this.adjustPosition(!0);
},
requestMenuShow: function(e, t) {
if (this.floating) {
var n = t.activator.hasNode();
if (n) {
var r = this.activatorOffset = this.getPageOffset(n);
this.applyPosition({
top: r.top + (this.showOnTop ? 0 : r.height),
left: r.left,
width: r.width
});
}
}
return this.show(), !0;
},
applyPosition: function(e) {
var t = "";
for (var n in e) t += n + ":" + e[n] + (isNaN(e[n]) ? "; " : "px; ");
this.addStyles(t);
},
getPageOffset: function(e) {
var t = e.getBoundingClientRect(), n = window.pageYOffset === undefined ? document.documentElement.scrollTop : window.pageYOffset, r = window.pageXOffset === undefined ? document.documentElement.scrollLeft : window.pageXOffset, i = t.height === undefined ? t.bottom - t.top : t.height, s = t.width === undefined ? t.right - t.left : t.width;
return {
top: t.top + n,
left: t.left + r,
height: i,
width: s
};
},
adjustPosition: function() {
if (this.showing && this.hasNode()) {
this.scrolling && !this.showOnTop && this.getScroller().setMaxHeight(this.maxHeight + "px"), this.removeClass("onyx-menu-up"), this.floating || this.applyPosition({
left: "auto"
});
var e = this.node.getBoundingClientRect(), t = e.height === undefined ? e.bottom - e.top : e.height, n = window.innerHeight === undefined ? document.documentElement.clientHeight : window.innerHeight, r = window.innerWidth === undefined ? document.documentElement.clientWidth : window.innerWidth;
this.menuUp = e.top + t > n && n - e.bottom < e.top - t, this.addRemoveClass("onyx-menu-up", this.menuUp);
if (this.floating) {
var i = this.activatorOffset;
this.menuUp ? this.applyPosition({
top: i.top - t + (this.showOnTop ? i.height : 0),
bottom: "auto"
}) : e.top < i.top && i.top + (this.showOnTop ? 0 : i.height) + t < n && this.applyPosition({
top: i.top + (this.showOnTop ? 0 : i.height),
bottom: "auto"
});
}
e.right > r && (this.floating ? this.applyPosition({
left: r - e.width
}) : this.applyPosition({
left: -(e.right - r)
})), e.left < 0 && (this.floating ? this.applyPosition({
left: 0,
right: "auto"
}) : this.getComputedStyleValue("right") == "auto" ? this.applyPosition({
left: -e.left
}) : this.applyPosition({
right: e.left
}));
if (this.scrolling && !this.showOnTop) {
e = this.node.getBoundingClientRect();
var s;
this.menuUp ? s = this.maxHeight < e.bottom ? this.maxHeight : e.bottom : s = e.top + this.maxHeight < n ? this.maxHeight : n - e.top, this.getScroller().setMaxHeight(s + "px");
}
}
},
resizeHandler: function() {
this.inherited(arguments), this.adjustPosition();
},
requestHide: function() {
this.setShowing(!1);
}
});

// MenuItem.js

enyo.kind({
name: "onyx.MenuItem",
kind: "enyo.Button",
events: {
onSelect: "",
onItemContentChange: ""
},
classes: "onyx-menu-item",
tag: "div",
create: function() {
this.inherited(arguments), this.active && this.bubble("onActivate");
},
tap: function(e) {
this.inherited(arguments), this.bubble("onRequestHideMenu"), this.doSelect({
selected: this,
content: this.content
});
},
contentChanged: function(e) {
this.inherited(arguments), this.doItemContentChange({
content: this.content
});
}
});

// PickerDecorator.js

enyo.kind({
name: "onyx.PickerDecorator",
kind: "onyx.MenuDecorator",
classes: "onyx-picker-decorator",
defaultKind: "onyx.PickerButton",
handlers: {
onChange: "change"
},
change: function(e, t) {
this.waterfallDown("onChange", t);
}
});

// PickerButton.js

enyo.kind({
name: "onyx.PickerButton",
kind: "onyx.Button",
handlers: {
onChange: "change"
},
change: function(e, t) {
t.content !== undefined && this.setContent(t.content);
}
});

// Picker.js

enyo.kind({
name: "onyx.Picker",
kind: "onyx.Menu",
classes: "onyx-picker enyo-unselectable",
published: {
selected: null
},
events: {
onChange: ""
},
handlers: {
onItemContentChange: "itemContentChange"
},
floating: !0,
showOnTop: !0,
initComponents: function() {
this.setScrolling(!0), this.inherited(arguments);
},
showingChanged: function() {
this.getScroller().setShowing(this.showing), this.inherited(arguments), this.showing && this.selected && this.scrollToSelected();
},
scrollToSelected: function() {
this.getScroller().scrollToControl(this.selected, !this.menuUp);
},
itemActivated: function(e, t) {
return this.processActivatedItem(t.originator), this.inherited(arguments);
},
processActivatedItem: function(e) {
e.active && this.setSelected(e);
},
selectedChanged: function(e) {
e && e.removeClass("selected"), this.selected && (this.selected.addClass("selected"), this.doChange({
selected: this.selected,
content: this.selected.content
}));
},
itemContentChange: function(e, t) {
t.originator == this.selected && this.doChange({
selected: this.selected,
content: this.selected.content
});
},
resizeHandler: function() {
this.inherited(arguments), this.adjustPosition();
}
});

// FlyweightPicker.js

enyo.kind({
name: "onyx.FlyweightPicker",
kind: "onyx.Picker",
classes: "onyx-flyweight-picker",
published: {
count: 0
},
events: {
onSetupItem: "",
onSelect: ""
},
handlers: {
onSelect: "itemSelect"
},
components: [ {
name: "scroller",
kind: "enyo.Scroller",
strategyKind: "TouchScrollStrategy",
components: [ {
name: "flyweight",
kind: "FlyweightRepeater",
noSelect: !0,
ontap: "itemTap"
} ]
} ],
scrollerName: "scroller",
initComponents: function() {
this.controlParentName = "flyweight", this.inherited(arguments), this.$.flyweight.$.client.children[0].setActive(!0);
},
create: function() {
this.inherited(arguments), this.countChanged();
},
rendered: function() {
this.inherited(arguments), this.selectedChanged();
},
scrollToSelected: function() {
var e = this.$.flyweight.fetchRowNode(this.selected);
this.getScroller().scrollToNode(e, !this.menuUp);
},
countChanged: function() {
this.$.flyweight.count = this.count;
},
processActivatedItem: function(e) {
this.item = e;
},
selectedChanged: function(e) {
if (!this.item) return;
e != null && (this.item.removeClass("selected"), this.$.flyweight.renderRow(e));
var t;
this.selected != null && (this.item.addClass("selected"), this.$.flyweight.renderRow(this.selected), this.item.removeClass("selected"), t = this.$.flyweight.fetchRowNode(this.selected)), this.doChange({
selected: this.selected,
content: t && t.textContent || this.item.content
});
},
itemTap: function(e, t) {
this.setSelected(t.rowIndex), this.doSelect({
selected: this.item,
content: this.item.content
});
},
itemSelect: function(e, t) {
if (t.originator != this) return !0;
}
});

// DatePicker.js

enyo.kind({
name: "onyx.DatePicker",
classes: "onyx-toolbar-inline",
published: {
disabled: !1,
locale: "en_us",
dayHidden: !1,
monthHidden: !1,
yearHidden: !1,
minYear: 1900,
maxYear: 2099,
value: null
},
events: {
onSelect: ""
},
create: function() {
this.inherited(arguments), enyo.g11n && (this.locale = enyo.g11n.currentLocale().getLocale()), this.initDefaults();
},
initDefaults: function() {
var e = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC" ];
enyo.g11n && (this._tf = new enyo.g11n.Fmts({
locale: this.locale
}), e = this._tf.getMonthFields()), this.setupPickers(this._tf ? this._tf.getDateFieldOrder() : "mdy"), this.dayHiddenChanged(), this.monthHiddenChanged(), this.yearHiddenChanged();
var t = this.value = this.value || new Date;
for (var n = 0, r; r = e[n]; n++) this.$.monthPicker.createComponent({
content: r,
value: n,
active: n == t.getMonth()
});
var i = t.getFullYear();
this.$.yearPicker.setSelected(i - this.minYear);
for (n = 1; n <= this.monthLength(t.getYear(), t.getMonth()); n++) this.$.dayPicker.createComponent({
content: n,
value: n,
active: n == t.getDate()
});
},
monthLength: function(e, t) {
return 32 - (new Date(e, t, 32)).getDate();
},
setupYear: function(e, t) {
this.$.year.setContent(this.minYear + t.index);
},
setupPickers: function(e) {
var t = e.split(""), n, r, i;
for (r = 0, i = t.length; r < i; r++) {
n = t[r];
switch (n) {
case "d":
this.createDay();
break;
case "m":
this.createMonth();
break;
case "y":
this.createYear();
break;
default:
}
}
},
createYear: function() {
var e = this.maxYear - this.minYear;
this.createComponent({
kind: "onyx.PickerDecorator",
onSelect: "updateYear",
components: [ {
classes: "onyx-datepicker-year",
name: "yearPickerButton",
disabled: this.disabled
}, {
name: "yearPicker",
kind: "onyx.FlyweightPicker",
count: ++e,
onSetupItem: "setupYear",
components: [ {
name: "year"
} ]
} ]
});
},
createMonth: function() {
this.createComponent({
kind: "onyx.PickerDecorator",
onSelect: "updateMonth",
components: [ {
classes: "onyx-datepicker-month",
name: "monthPickerButton",
disabled: this.disabled
}, {
name: "monthPicker",
kind: "onyx.Picker"
} ]
});
},
createDay: function() {
this.createComponent({
kind: "onyx.PickerDecorator",
onSelect: "updateDay",
components: [ {
classes: "onyx-datepicker-day",
name: "dayPickerButton",
disabled: this.disabled
}, {
name: "dayPicker",
kind: "onyx.Picker"
} ]
});
},
localeChanged: function() {
this.refresh();
},
dayHiddenChanged: function() {
this.$.dayPicker.getParent().setShowing(this.dayHidden ? !1 : !0);
},
monthHiddenChanged: function() {
this.$.monthPicker.getParent().setShowing(this.monthHidden ? !1 : !0);
},
yearHiddenChanged: function() {
this.$.yearPicker.getParent().setShowing(this.yearHidden ? !1 : !0);
},
minYearChanged: function() {
this.refresh();
},
maxYearChanged: function() {
this.refresh();
},
valueChanged: function() {
this.refresh();
},
disabledChanged: function() {
this.$.yearPickerButton.setDisabled(this.disabled), this.$.monthPickerButton.setDisabled(this.disabled), this.$.dayPickerButton.setDisabled(this.disabled);
},
updateDay: function(e, t) {
var n = this.calcDate(this.value.getFullYear(), this.value.getMonth(), t.selected.value);
return this.doSelect({
name: this.name,
value: n
}), this.setValue(n), !0;
},
updateMonth: function(e, t) {
var n = this.calcDate(this.value.getFullYear(), t.selected.value, this.value.getDate());
return this.doSelect({
name: this.name,
value: n
}), this.setValue(n), !0;
},
updateYear: function(e, t) {
if (t.originator.selected != -1) {
var n = this.calcDate(this.minYear + t.originator.selected, this.value.getMonth(), this.value.getDate());
this.doSelect({
name: this.name,
value: n
}), this.setValue(n);
}
return !0;
},
calcDate: function(e, t, n) {
return new Date(e, t, n, this.value.getHours(), this.value.getMinutes(), this.value.getSeconds(), this.value.getMilliseconds());
},
refresh: function() {
this.destroyClientControls(), this.initDefaults(), this.render();
}
});

// TimePicker.js

enyo.kind({
name: "onyx.TimePicker",
classes: "onyx-toolbar-inline",
published: {
disabled: !1,
locale: "en_us",
is24HrMode: null,
value: null
},
events: {
onSelect: ""
},
create: function() {
this.inherited(arguments), enyo.g11n && (this.locale = enyo.g11n.currentLocale().getLocale()), this.initDefaults();
},
initDefaults: function() {
var e = "AM", t = "PM";
this.is24HrMode == null && (this.is24HrMode = !1), enyo.g11n && (this._tf = new enyo.g11n.Fmts({
locale: this.locale
}), e = this._tf.getAmCaption(), t = this._tf.getPmCaption(), this.is24HrMode == null && (this.is24HrMode = !this._tf.isAmPm())), this.setupPickers(this._tf ? this._tf.getTimeFieldOrder() : "hma");
var n = this.value = this.value || new Date, r;
if (!this.is24HrMode) {
var i = n.getHours();
i = i === 0 ? 12 : i;
for (r = 1; r <= 12; r++) this.$.hourPicker.createComponent({
content: r,
value: r,
active: r == (i > 12 ? i % 12 : i)
});
} else for (r = 0; r < 24; r++) this.$.hourPicker.createComponent({
content: r,
value: r,
active: r == n.getHours()
});
for (r = 0; r <= 59; r++) this.$.minutePicker.createComponent({
content: r < 10 ? "0" + r : r,
value: r,
active: r == n.getMinutes()
});
n.getHours() >= 12 ? this.$.ampmPicker.createComponents([ {
content: e
}, {
content: t,
active: !0
} ]) : this.$.ampmPicker.createComponents([ {
content: e,
active: !0
}, {
content: t
} ]), this.$.ampmPicker.getParent().setShowing(!this.is24HrMode);
},
setupPickers: function(e) {
var t = e.split(""), n, r, i;
for (r = 0, i = t.length; r < i; r++) {
n = t[r];
switch (n) {
case "h":
this.createHour();
break;
case "m":
this.createMinute();
break;
case "a":
this.createAmPm();
break;
default:
}
}
},
createHour: function() {
this.createComponent({
kind: "onyx.PickerDecorator",
onSelect: "updateHour",
components: [ {
classes: "onyx-timepicker-hour",
name: "hourPickerButton",
disabled: this.disabled
}, {
name: "hourPicker",
kind: "onyx.Picker"
} ]
});
},
createMinute: function() {
this.createComponent({
kind: "onyx.PickerDecorator",
onSelect: "updateMinute",
components: [ {
classes: "onyx-timepicker-minute",
name: "minutePickerButton",
disabled: this.disabled
}, {
name: "minutePicker",
kind: "onyx.Picker"
} ]
});
},
createAmPm: function() {
this.createComponent({
kind: "onyx.PickerDecorator",
onSelect: "updateAmPm",
components: [ {
classes: "onyx-timepicker-ampm",
name: "ampmPickerButton",
disabled: this.disabled
}, {
name: "ampmPicker",
kind: "onyx.Picker"
} ]
});
},
disabledChanged: function() {
this.$.hourPickerButton.setDisabled(this.disabled), this.$.minutePickerButton.setDisabled(this.disabled), this.$.ampmPickerButton.setDisabled(this.disabled);
},
localeChanged: function() {
this.is24HrMode = null, this.refresh();
},
is24HrModeChanged: function() {
this.refresh();
},
valueChanged: function() {
this.refresh();
},
updateHour: function(e, t) {
var n = t.selected.value;
if (!this.is24HrMode) {
var r = this.$.ampmPicker.getParent().controlAtIndex(0).content;
n = n + (n == 12 ? -12 : 0) + (this.isAm(r) ? 0 : 12);
}
return this.value = this.calcTime(n, this.value.getMinutes()), this.doSelect({
name: this.name,
value: this.value
}), !0;
},
updateMinute: function(e, t) {
return this.value = this.calcTime(this.value.getHours(), t.selected.value), this.doSelect({
name: this.name,
value: this.value
}), !0;
},
updateAmPm: function(e, t) {
var n = this.value.getHours();
return this.is24HrMode || (n += n > 11 ? this.isAm(t.content) ? -12 : 0 : this.isAm(t.content) ? 0 : 12), this.value = this.calcTime(n, this.value.getMinutes()), this.doSelect({
name: this.name,
value: this.value
}), !0;
},
calcTime: function(e, t) {
return new Date(this.value.getFullYear(), this.value.getMonth(), this.value.getDate(), e, t, this.value.getSeconds(), this.value.getMilliseconds());
},
isAm: function(e) {
var t, n, r;
try {
t = this._tf.getAmCaption(), n = this._tf.getPmCaption();
} catch (i) {
t = "AM", n = "PM";
}
return e == t ? !0 : !1;
},
refresh: function() {
this.destroyClientControls(), this.initDefaults(), this.render();
}
});

// RadioButton.js

enyo.kind({
name: "onyx.RadioButton",
kind: "Button",
classes: "onyx-radiobutton"
});

// RadioGroup.js

enyo.kind({
name: "onyx.RadioGroup",
kind: "Group",
defaultKind: "onyx.RadioButton",
highlander: !0
});

// ToggleButton.js

enyo.kind({
name: "onyx.ToggleButton",
classes: "onyx-toggle-button",
published: {
active: !1,
value: !1,
onContent: "On",
offContent: "Off",
disabled: !1
},
events: {
onChange: ""
},
handlers: {
ondragstart: "dragstart",
ondrag: "drag",
ondragfinish: "dragfinish"
},
components: [ {
name: "contentOn",
classes: "onyx-toggle-content on"
}, {
name: "contentOff",
classes: "onyx-toggle-content off"
}, {
classes: "onyx-toggle-button-knob"
} ],
create: function() {
this.inherited(arguments), this.value = Boolean(this.value || this.active), this.onContentChanged(), this.offContentChanged(), this.disabledChanged();
},
rendered: function() {
this.inherited(arguments), this.updateVisualState();
},
updateVisualState: function() {
this.addRemoveClass("off", !this.value), this.$.contentOn.setShowing(this.value), this.$.contentOff.setShowing(!this.value), this.setActive(this.value);
},
valueChanged: function() {
this.updateVisualState(), this.doChange({
value: this.value
});
},
activeChanged: function() {
this.setValue(this.active), this.bubble("onActivate");
},
onContentChanged: function() {
this.$.contentOn.setContent(this.onContent || ""), this.$.contentOn.addRemoveClass("empty", !this.onContent);
},
offContentChanged: function() {
this.$.contentOff.setContent(this.offContent || ""), this.$.contentOff.addRemoveClass("empty", !this.onContent);
},
disabledChanged: function() {
this.addRemoveClass("disabled", this.disabled);
},
updateValue: function(e) {
this.disabled || this.setValue(e);
},
tap: function() {
this.updateValue(!this.value);
},
dragstart: function(e, t) {
if (t.horizontal) return t.preventDefault(), this.dragging = !0, this.dragged = !1, !0;
},
drag: function(e, t) {
if (this.dragging) {
var n = t.dx;
return Math.abs(n) > 10 && (this.updateValue(n > 0), this.dragged = !0), !0;
}
},
dragfinish: function(e, t) {
this.dragging = !1, this.dragged && t.preventTap();
}
});

// ToggleIconButton.js

enyo.kind({
name: "onyx.ToggleIconButton",
kind: "onyx.Icon",
published: {
active: !1,
value: !1
},
events: {
onChange: ""
},
classes: "onyx-icon-button onyx-icon-toggle",
activeChanged: function() {
this.addRemoveClass("active", this.value), this.bubble("onActivate");
},
updateValue: function(e) {
this.disabled || (this.setValue(e), this.doChange({
value: this.value
}));
},
tap: function() {
this.updateValue(!this.value);
},
valueChanged: function() {
this.setActive(this.value);
},
create: function() {
this.inherited(arguments), this.value = Boolean(this.value || this.active);
},
rendered: function() {
this.inherited(arguments), this.valueChanged(), this.removeClass("onyx-icon");
}
});

// Toolbar.js

enyo.kind({
name: "onyx.Toolbar",
classes: "onyx onyx-toolbar onyx-toolbar-inline",
create: function() {
this.inherited(arguments), this.hasClass("onyx-menu-toolbar") && enyo.platform.android >= 4 && this.applyStyle("position", "static");
}
});

// Tooltip.js

enyo.kind({
name: "onyx.Tooltip",
kind: "onyx.Popup",
classes: "onyx-tooltip below left-arrow",
autoDismiss: !1,
showDelay: 500,
defaultLeft: -6,
handlers: {
onRequestShowTooltip: "requestShow",
onRequestHideTooltip: "requestHide"
},
requestShow: function() {
return this.showJob = setTimeout(enyo.bind(this, "show"), this.showDelay), !0;
},
cancelShow: function() {
clearTimeout(this.showJob);
},
requestHide: function() {
return this.cancelShow(), this.inherited(arguments);
},
showingChanged: function() {
this.cancelShow(), this.adjustPosition(!0), this.inherited(arguments);
},
applyPosition: function(e) {
var t = "";
for (var n in e) t += n + ":" + e[n] + (isNaN(e[n]) ? "; " : "px; ");
this.addStyles(t);
},
adjustPosition: function(e) {
if (this.showing && this.hasNode()) {
var t = this.node.getBoundingClientRect();
t.top + t.height > window.innerHeight ? (this.addRemoveClass("below", !1), this.addRemoveClass("above", !0)) : (this.addRemoveClass("above", !1), this.addRemoveClass("below", !0)), t.left + t.width > window.innerWidth && (this.applyPosition({
"margin-left": -t.width,
bottom: "auto"
}), this.addRemoveClass("left-arrow", !1), this.addRemoveClass("right-arrow", !0));
}
},
resizeHandler: function() {
this.applyPosition({
"margin-left": this.defaultLeft,
bottom: "auto"
}), this.addRemoveClass("left-arrow", !0), this.addRemoveClass("right-arrow", !1), this.adjustPosition(!0), this.inherited(arguments);
}
});

// TooltipDecorator.js

enyo.kind({
name: "onyx.TooltipDecorator",
defaultKind: "onyx.Button",
classes: "onyx-popup-decorator",
handlers: {
onenter: "enter",
onleave: "leave"
},
enter: function() {
this.requestShowTooltip();
},
leave: function() {
this.requestHideTooltip();
},
tap: function() {
this.requestHideTooltip();
},
requestShowTooltip: function() {
this.waterfallDown("onRequestShowTooltip");
},
requestHideTooltip: function() {
this.waterfallDown("onRequestHideTooltip");
}
});

// ProgressBar.js

enyo.kind({
name: "onyx.ProgressBar",
classes: "onyx-progress-bar",
published: {
progress: 0,
min: 0,
max: 100,
barClasses: "",
showStripes: !0,
animateStripes: !0,
increment: 0
},
events: {
onAnimateProgressFinish: ""
},
components: [ {
name: "progressAnimator",
kind: "Animator",
onStep: "progressAnimatorStep",
onEnd: "progressAnimatorComplete"
}, {
name: "bar",
classes: "onyx-progress-bar-bar"
} ],
create: function() {
this.inherited(arguments), this.progressChanged(), this.barClassesChanged(), this.showStripesChanged(), this.animateStripesChanged();
},
barClassesChanged: function(e) {
this.$.bar.removeClass(e), this.$.bar.addClass(this.barClasses);
},
showStripesChanged: function() {
this.$.bar.addRemoveClass("striped", this.showStripes);
},
animateStripesChanged: function() {
this.$.bar.addRemoveClass("animated", this.animateStripes);
},
progressChanged: function() {
this.progress = this.clampValue(this.min, this.max, this.progress);
var e = this.calcPercent(this.progress);
this.updateBarPosition(e);
},
calcIncrement: function(e) {
return Math.round(e / this.increment) * this.increment;
},
clampValue: function(e, t, n) {
return Math.max(e, Math.min(n, t));
},
calcRatio: function(e) {
return (e - this.min) / (this.max - this.min);
},
calcPercent: function(e) {
return this.calcRatio(e) * 100;
},
updateBarPosition: function(e) {
this.$.bar.applyStyle("width", e + "%");
},
animateProgressTo: function(e) {
this.$.progressAnimator.play({
startValue: this.progress,
endValue: e,
node: this.hasNode()
});
},
progressAnimatorStep: function(e) {
return this.setProgress(e.value), !0;
},
progressAnimatorComplete: function(e) {
return this.doAnimateProgressFinish(e), !0;
}
});

// ProgressButton.js

enyo.kind({
name: "onyx.ProgressButton",
kind: "onyx.ProgressBar",
classes: "onyx-progress-button",
events: {
onCancel: ""
},
components: [ {
name: "progressAnimator",
kind: "Animator",
onStep: "progressAnimatorStep",
onEnd: "progressAnimatorComplete"
}, {
name: "bar",
classes: "onyx-progress-bar-bar onyx-progress-button-bar"
}, {
name: "client",
classes: "onyx-progress-button-client"
}, {
kind: "onyx.Icon",
src: "$lib/onyx/images/progress-button-cancel.png",
classes: "onyx-progress-button-icon",
ontap: "cancelTap"
} ],
cancelTap: function() {
this.doCancel();
}
});

// Scrim.js

enyo.kind({
name: "onyx.Scrim",
showing: !1,
classes: "onyx-scrim enyo-fit",
floating: !1,
create: function() {
this.inherited(arguments), this.zStack = [], this.floating && this.setParent(enyo.floatingLayer);
},
showingChanged: function() {
this.floating && this.showing && !this.hasNode() && this.render(), this.inherited(arguments);
},
addZIndex: function(e) {
enyo.indexOf(e, this.zStack) < 0 && this.zStack.push(e);
},
removeZIndex: function(e) {
enyo.remove(e, this.zStack);
},
showAtZIndex: function(e) {
this.addZIndex(e), e !== undefined && this.setZIndex(e), this.show();
},
hideAtZIndex: function(e) {
this.removeZIndex(e);
if (!this.zStack.length) this.hide(); else {
var t = this.zStack[this.zStack.length - 1];
this.setZIndex(t);
}
},
setZIndex: function(e) {
this.zIndex = e, this.applyStyle("z-index", e);
},
make: function() {
return this;
}
}), enyo.kind({
name: "onyx.scrimSingleton",
kind: null,
constructor: function(e, t) {
this.instanceName = e, enyo.setObject(this.instanceName, this), this.props = t || {};
},
make: function() {
var e = new onyx.Scrim(this.props);
return enyo.setObject(this.instanceName, e), e;
},
showAtZIndex: function(e) {
var t = this.make();
t.showAtZIndex(e);
},
hideAtZIndex: enyo.nop,
show: function() {
var e = this.make();
e.show();
}
}), new onyx.scrimSingleton("onyx.scrim", {
floating: !0,
classes: "onyx-scrim-translucent"
}), new onyx.scrimSingleton("onyx.scrimTransparent", {
floating: !0,
classes: "onyx-scrim-transparent"
});

// Slider.js

enyo.kind({
name: "onyx.Slider",
kind: "onyx.ProgressBar",
classes: "onyx-slider",
published: {
value: 0,
lockBar: !0,
tappable: !0
},
events: {
onChange: "",
onChanging: "",
onAnimateFinish: ""
},
showStripes: !1,
handlers: {
ondragstart: "dragstart",
ondrag: "drag",
ondragfinish: "dragfinish"
},
moreComponents: [ {
kind: "Animator",
onStep: "animatorStep",
onEnd: "animatorComplete"
}, {
classes: "onyx-slider-taparea"
}, {
name: "knob",
classes: "onyx-slider-knob"
} ],
create: function() {
this.inherited(arguments), enyo.platform.firefoxOS && (this.moreComponents[2].ondown = "down", this.moreComponents[2].onleave = "leave"), this.createComponents(this.moreComponents), this.valueChanged();
},
valueChanged: function() {
this.value = this.clampValue(this.min, this.max, this.value);
var e = this.calcPercent(this.value);
this.updateKnobPosition(e), this.lockBar && this.setProgress(this.value);
},
updateKnobPosition: function(e) {
this.$.knob.applyStyle("left", e + "%");
},
calcKnobPosition: function(e) {
var t = e.clientX - this.hasNode().getBoundingClientRect().left;
return t / this.getBounds().width * (this.max - this.min) + this.min;
},
dragstart: function(e, t) {
if (t.horizontal) return t.preventDefault(), this.dragging = !0, !0;
},
drag: function(e, t) {
if (this.dragging) {
var n = this.calcKnobPosition(t);
return n = this.increment ? this.calcIncrement(n) : n, this.setValue(n), this.doChanging({
value: this.value
}), !0;
}
},
dragfinish: function(e, t) {
return this.dragging = !1, t.preventTap(), this.doChange({
value: this.value
}), !0;
},
tap: function(e, t) {
if (this.tappable) {
var n = this.calcKnobPosition(t);
return n = this.increment ? this.calcIncrement(n) : n, this.tapped = !0, this.animateTo(n), !0;
}
},
down: function(e, t) {
this.addClass("pressed");
},
leave: function(e, t) {
this.removeClass("pressed");
},
animateTo: function(e) {
this.$.animator.play({
startValue: this.value,
endValue: e,
node: this.hasNode()
});
},
animatorStep: function(e) {
return this.setValue(e.value), !0;
},
animatorComplete: function(e) {
return this.tapped && (this.tapped = !1, this.doChange({
value: this.value
})), this.doAnimateFinish(e), !0;
}
});

// RangeSlider.js

enyo.kind({
name: "onyx.RangeSlider",
kind: "onyx.ProgressBar",
classes: "onyx-slider",
published: {
rangeMin: 0,
rangeMax: 100,
rangeStart: 0,
rangeEnd: 100,
beginValue: 0,
endValue: 0
},
events: {
onChange: "",
onChanging: ""
},
showStripes: !1,
showLabels: !1,
handlers: {
ondragstart: "dragstart",
ondrag: "drag",
ondragfinish: "dragfinish",
ondown: "down"
},
moreComponents: [ {
name: "startKnob",
classes: "onyx-slider-knob"
}, {
name: "endKnob",
classes: "onyx-slider-knob onyx-range-slider-knob"
} ],
create: function() {
this.inherited(arguments), this.createComponents(this.moreComponents), this.initControls();
},
rendered: function() {
this.inherited(arguments);
var e = this.calcPercent(this.beginValue);
this.updateBarPosition(e);
},
initControls: function() {
this.$.bar.applyStyle("position", "relative"), this.refreshRangeSlider(), this.showLabels && (this.$.startKnob.createComponent({
name: "startLabel",
kind: "onyx.RangeSliderKnobLabel"
}), this.$.endKnob.createComponent({
name: "endLabel",
kind: "onyx.RangeSliderKnobLabel"
}));
},
refreshRangeSlider: function() {
this.beginValue = this.calcKnobPercent(this.rangeStart), this.endValue = this.calcKnobPercent(this.rangeEnd), this.beginValueChanged(), this.endValueChanged();
},
calcKnobRatio: function(e) {
return (e - this.rangeMin) / (this.rangeMax - this.rangeMin);
},
calcKnobPercent: function(e) {
return this.calcKnobRatio(e) * 100;
},
beginValueChanged: function(e) {
if (e === undefined) {
var t = this.calcPercent(this.beginValue);
this.updateKnobPosition(t, this.$.startKnob);
}
},
endValueChanged: function(e) {
if (e === undefined) {
var t = this.calcPercent(this.endValue);
this.updateKnobPosition(t, this.$.endKnob);
}
},
calcKnobPosition: function(e) {
var t = e.clientX - this.hasNode().getBoundingClientRect().left;
return t / this.getBounds().width * (this.max - this.min) + this.min;
},
updateKnobPosition: function(e, t) {
t.applyStyle("left", e + "%"), this.updateBarPosition();
},
updateBarPosition: function() {
if (this.$.startKnob !== undefined && this.$.endKnob !== undefined) {
var e = this.calcKnobPercent(this.rangeStart), t = this.calcKnobPercent(this.rangeEnd) - e;
this.$.bar.applyStyle("left", e + "%"), this.$.bar.applyStyle("width", t + "%");
}
},
calcRangeRatio: function(e) {
return e / 100 * (this.rangeMax - this.rangeMin) + this.rangeMin - this.increment / 2;
},
swapZIndex: function(e) {
e === "startKnob" ? (this.$.startKnob.applyStyle("z-index", 1), this.$.endKnob.applyStyle("z-index", 0)) : e === "endKnob" && (this.$.startKnob.applyStyle("z-index", 0), this.$.endKnob.applyStyle("z-index", 1));
},
down: function(e, t) {
this.swapZIndex(e.name);
},
dragstart: function(e, t) {
if (t.horizontal) return t.preventDefault(), this.dragging = !0, !0;
},
drag: function(e, t) {
if (this.dragging) {
var n = this.calcKnobPosition(t), r, i, s;
if (e.name === "startKnob" && n >= 0) {
if (!(n <= this.endValue && t.xDirection === -1 || n <= this.endValue)) return this.drag(this.$.endKnob, t);
this.setBeginValue(n), r = this.calcRangeRatio(this.beginValue), i = this.increment ? this.calcIncrement(r + .5 * this.increment) : r, s = this.calcKnobPercent(i), this.updateKnobPosition(s, this.$.startKnob), this.setRangeStart(i), this.doChanging({
value: i
});
} else if (e.name === "endKnob" && n <= 100) {
if (!(n >= this.beginValue && t.xDirection === 1 || n >= this.beginValue)) return this.drag(this.$.startKnob, t);
this.setEndValue(n), r = this.calcRangeRatio(this.endValue), i = this.increment ? this.calcIncrement(r + .5 * this.increment) : r, s = this.calcKnobPercent(i), this.updateKnobPosition(s, this.$.endKnob), this.setRangeEnd(i), this.doChanging({
value: i
});
}
return !0;
}
},
dragfinish: function(e, t) {
this.dragging = !1, t.preventTap();
var n;
return e.name === "startKnob" ? (n = this.calcRangeRatio(this.beginValue), this.doChange({
value: n,
startChanged: !0
})) : e.name === "endKnob" && (n = this.calcRangeRatio(this.endValue), this.doChange({
value: n,
startChanged: !1
})), !0;
},
rangeMinChanged: function() {
this.refreshRangeSlider();
},
rangeMaxChanged: function() {
this.refreshRangeSlider();
},
rangeStartChanged: function() {
this.refreshRangeSlider();
},
rangeEndChanged: function() {
this.refreshRangeSlider();
},
setStartLabel: function(e) {
this.$.startKnob.waterfallDown("onSetLabel", e);
},
setEndLabel: function(e) {
this.$.endKnob.waterfallDown("onSetLabel", e);
}
}), enyo.kind({
name: "onyx.RangeSliderKnobLabel",
classes: "onyx-range-slider-label",
handlers: {
onSetLabel: "setLabel"
},
setLabel: function(e, t) {
this.setContent(t);
}
});

// Item.js

enyo.kind({
name: "onyx.Item",
classes: "onyx-item",
tapHighlight: !0,
handlers: {
onhold: "hold",
onrelease: "release"
},
hold: function(e, t) {
this.tapHighlight && onyx.Item.addRemoveFlyweightClass(this.controlParent || this, "onyx-highlight", !0, t);
},
release: function(e, t) {
this.tapHighlight && onyx.Item.addRemoveFlyweightClass(this.controlParent || this, "onyx-highlight", !1, t);
},
statics: {
addRemoveFlyweightClass: function(e, t, n, r, i) {
var s = r.flyweight;
if (s) {
var o = i !== undefined ? i : r.index;
s.performOnRow(o, function() {
e.addRemoveClass(t, n);
});
}
}
}
});

// Spinner.js

enyo.kind({
name: "onyx.Spinner",
classes: "onyx-spinner",
stop: function() {
this.setShowing(!1);
},
start: function() {
this.setShowing(!0);
},
toggle: function() {
this.setShowing(!this.getShowing());
}
});

// MoreToolbar.js

enyo.kind({
name: "onyx.MoreToolbar",
classes: "onyx-toolbar onyx-more-toolbar",
menuClass: "",
movedClass: "",
layoutKind: "FittableColumnsLayout",
noStretch: !0,
handlers: {
onHide: "reflow"
},
published: {
clientLayoutKind: "FittableColumnsLayout"
},
tools: [ {
name: "client",
noStretch: !0,
fit: !0,
classes: "onyx-toolbar-inline"
}, {
name: "nard",
kind: "onyx.MenuDecorator",
showing: !1,
onActivate: "activated",
components: [ {
kind: "onyx.IconButton",
classes: "onyx-more-button"
}, {
name: "menu",
kind: "onyx.Menu",
scrolling: !1,
classes: "onyx-more-menu"
} ]
} ],
initComponents: function() {
this.menuClass && this.menuClass.length > 0 && !this.$.menu.hasClass(this.menuClass) && this.$.menu.addClass(this.menuClass), this.createChrome(this.tools), this.inherited(arguments), this.$.client.setLayoutKind(this.clientLayoutKind);
},
clientLayoutKindChanged: function() {
this.$.client.setLayoutKind(this.clientLayoutKind);
},
reflow: function() {
this.inherited(arguments), this.isContentOverflowing() ? (this.$.nard.show(), this.popItem() && this.reflow()) : this.tryPushItem() ? this.reflow() : this.$.menu.children.length || (this.$.nard.hide(), this.$.menu.hide());
},
activated: function(e, t) {
this.addRemoveClass("active", t.originator.active);
},
popItem: function() {
var e = this.findCollapsibleItem();
if (e) {
this.movedClass && this.movedClass.length > 0 && !e.hasClass(this.movedClass) && e.addClass(this.movedClass), this.$.menu.addChild(e, null);
var t = this.$.menu.hasNode();
return t && e.hasNode() && e.insertNodeInParent(t), !0;
}
},
pushItem: function() {
var e = this.$.menu.children, t = e[0];
if (t) {
this.movedClass && this.movedClass.length > 0 && t.hasClass(this.movedClass) && t.removeClass(this.movedClass), this.$.client.addChild(t);
var n = this.$.client.hasNode();
if (n && t.hasNode()) {
var r, i;
for (var s = 0; s < this.$.client.children.length; s++) {
var o = this.$.client.children[s];
if (o.toolbarIndex !== undefined && o.toolbarIndex != s) {
r = o, i = s;
break;
}
}
if (r && r.hasNode()) {
t.insertNodeInParent(n, r.node);
var u = this.$.client.children.pop();
this.$.client.children.splice(i, 0, u);
} else t.appendNodeToParent(n);
}
return !0;
}
},
tryPushItem: function() {
if (this.pushItem()) {
if (!this.isContentOverflowing()) return !0;
this.popItem();
}
},
isContentOverflowing: function() {
if (this.$.client.hasNode()) {
var e = this.$.client.children, t = e[e.length - 1].hasNode();
if (t) return this.$.client.reflow(), t.offsetLeft + t.offsetWidth > this.$.client.node.clientWidth;
}
},
findCollapsibleItem: function() {
var e = this.$.client.children;
for (var t = e.length - 1; c = e[t]; t--) {
if (!c.unmoveable) return c;
c.toolbarIndex === undefined && (c.toolbarIndex = t);
}
}
});

// IntegerPicker.js

enyo.kind({
name: "onyx.IntegerPicker",
kind: "onyx.Picker",
published: {
value: 0,
min: 0,
max: 9
},
create: function() {
this.inherited(arguments), this.rangeChanged();
},
minChanged: function() {
this.destroyClientControls(), this.rangeChanged(), this.render();
},
maxChanged: function() {
this.destroyClientControls(), this.rangeChanged(), this.render();
},
rangeChanged: function() {
for (var e = this.min; e <= this.max; e++) this.createComponent({
content: e,
active: e === this.value ? !0 : !1
});
},
valueChanged: function(e) {
var t = this.getClientControls(), n = t.length;
this.value = this.value >= this.min && this.value <= this.max ? this.value : this.min;
for (var r = 0; r < n; r++) if (this.value === parseInt(t[r].content)) {
this.setSelected(t[r]);
break;
}
},
selectedChanged: function(e) {
e && e.removeClass("selected"), this.selected && (this.selected.addClass("selected"), this.doChange({
selected: this.selected,
content: this.selected.content
})), this.value = parseInt(this.selected.content);
}
});

// ContextualPopup.js

enyo.kind({
name: "onyx.ContextualPopup",
kind: "enyo.Popup",
modal: !0,
autoDismiss: !0,
floating: !1,
classes: "onyx-contextual-popup enyo-unselectable",
published: {
maxHeight: 100,
scrolling: !0,
title: undefined,
actionButtons: []
},
vertFlushMargin: 60,
horizFlushMargin: 50,
widePopup: 200,
longPopup: 200,
horizBuffer: 16,
events: {
onTap: ""
},
handlers: {
onActivate: "itemActivated",
onRequestShowMenu: "requestShow",
onRequestHideMenu: "requestHide"
},
components: [ {
name: "title",
classes: "onyx-contextual-popup-title"
}, {
classes: "onyx-contextual-popup-scroller",
components: [ {
name: "client",
kind: "enyo.Scroller",
vertical: "auto",
classes: "enyo-unselectable",
thumb: !1,
strategyKind: "TouchScrollStrategy"
} ]
}, {
name: "actionButtons",
classes: "onyx-contextual-popup-action-buttons"
} ],
scrollerName: "client",
create: function() {
this.inherited(arguments), this.maxHeightChanged(), this.titleChanged(), this.actionButtonsChanged();
},
getScroller: function() {
return this.$[this.scrollerName];
},
titleChanged: function() {
this.$.title.setContent(this.title);
},
actionButtonsChanged: function() {
for (var e = 0; e < this.actionButtons.length; e++) this.$.actionButtons.createComponent({
kind: "onyx.Button",
content: this.actionButtons[e].content,
classes: this.actionButtons[e].classes + " onyx-contextual-popup-action-button",
name: this.actionButtons[e].name ? this.actionButtons[e].name : "ActionButton" + e,
index: e,
tap: enyo.bind(this, this.tapHandler)
});
},
tapHandler: function(e, t) {
return t.actionButton = !0, t.popup = this, this.bubble("ontap", t), !0;
},
maxHeightChanged: function() {
this.scrolling && this.getScroller().setMaxHeight(this.maxHeight + "px");
},
itemActivated: function(e, t) {
return t.originator.setActive(!1), !0;
},
showingChanged: function() {
this.inherited(arguments), this.scrolling && this.getScroller().setShowing(this.showing), this.adjustPosition();
},
requestShow: function(e, t) {
var n = t.activator.hasNode();
return n && (this.activatorOffset = this.getPageOffset(n)), this.show(), !0;
},
applyPosition: function(e) {
var t = "";
for (var n in e) t += n + ":" + e[n] + (isNaN(e[n]) ? "; " : "px; ");
this.addStyles(t);
},
getPageOffset: function(e) {
var t = this.getBoundingRect(e), n = window.pageYOffset === undefined ? document.documentElement.scrollTop : window.pageYOffset, r = window.pageXOffset === undefined ? document.documentElement.scrollLeft : window.pageXOffset, i = t.height === undefined ? t.bottom - t.top : t.height, s = t.width === undefined ? t.right - t.left : t.width;
return {
top: t.top + n,
left: t.left + r,
height: i,
width: s
};
},
adjustPosition: function() {
if (this.showing && this.hasNode()) {
this.resetPositioning();
var e = this.getViewWidth(), t = this.getViewHeight(), n = this.vertFlushMargin, r = t - this.vertFlushMargin, i = this.horizFlushMargin, s = e - this.horizFlushMargin;
if (this.activatorOffset.top + this.activatorOffset.height < n || this.activatorOffset.top > r) {
if (this.applyVerticalFlushPositioning(i, s)) return;
if (this.applyHorizontalFlushPositioning(i, s)) return;
if (this.applyVerticalPositioning()) return;
} else if (this.activatorOffset.left + this.activatorOffset.width < i || this.activatorOffset.left > s) if (this.applyHorizontalPositioning()) return;
var o = this.getBoundingRect(this.node);
if (o.width > this.widePopup) {
if (this.applyVerticalPositioning()) return;
} else if (o.height > this.longPopup && this.applyHorizontalPositioning()) return;
if (this.applyVerticalPositioning()) return;
if (this.applyHorizontalPositioning()) return;
}
},
initVerticalPositioning: function() {
this.resetPositioning(), this.addClass("vertical");
var e = this.getBoundingRect(this.node), t = this.getViewHeight();
return this.floating ? this.activatorOffset.top < t / 2 ? (this.applyPosition({
top: this.activatorOffset.top + this.activatorOffset.height,
bottom: "auto"
}), this.addClass("below")) : (this.applyPosition({
top: this.activatorOffset.top - e.height,
bottom: "auto"
}), this.addClass("above")) : e.top + e.height > t && t - e.bottom < e.top - e.height ? this.addClass("above") : this.addClass("below"), e = this.getBoundingRect(this.node), e.top + e.height > t || e.top < 0 ? !1 : !0;
},
applyVerticalPositioning: function() {
if (!this.initVerticalPositioning()) return !1;
var e = this.getBoundingRect(this.node), t = this.getViewWidth();
if (this.floating) {
var n = this.activatorOffset.left + this.activatorOffset.width / 2 - e.width / 2;
n + e.width > t ? (this.applyPosition({
left: this.activatorOffset.left + this.activatorOffset.width - e.width
}), this.addClass("left")) : n < 0 ? (this.applyPosition({
left: this.activatorOffset.left
}), this.addClass("right")) : this.applyPosition({
left: n
});
} else {
var r = this.activatorOffset.left + this.activatorOffset.width / 2 - e.left - e.width / 2;
e.right + r > t ? (this.applyPosition({
left: this.activatorOffset.left + this.activatorOffset.width - e.right
}), this.addRemoveClass("left", !0)) : e.left + r < 0 ? this.addRemoveClass("right", !0) : this.applyPosition({
left: r
});
}
return !0;
},
applyVerticalFlushPositioning: function(e, t) {
if (!this.initVerticalPositioning()) return !1;
var n = this.getBoundingRect(this.node), r = this.getViewWidth();
return this.activatorOffset.left + this.activatorOffset.width / 2 < e ? (this.activatorOffset.left + this.activatorOffset.width / 2 < this.horizBuffer ? this.applyPosition({
left: this.horizBuffer + (this.floating ? 0 : -n.left)
}) : this.applyPosition({
left: this.activatorOffset.width / 2 + (this.floating ? this.activatorOffset.left : 0)
}), this.addClass("right"), this.addClass("corner"), !0) : this.activatorOffset.left + this.activatorOffset.width / 2 > t ? (this.activatorOffset.left + this.activatorOffset.width / 2 > r - this.horizBuffer ? this.applyPosition({
left: r - this.horizBuffer - n.right
}) : this.applyPosition({
left: this.activatorOffset.left + this.activatorOffset.width / 2 - n.right
}), this.addClass("left"), this.addClass("corner"), !0) : !1;
},
initHorizontalPositioning: function() {
this.resetPositioning();
var e = this.getBoundingRect(this.node), t = this.getViewWidth();
return this.floating ? this.activatorOffset.left + this.activatorOffset.width < t / 2 ? (this.applyPosition({
left: this.activatorOffset.left + this.activatorOffset.width
}), this.addRemoveClass("left", !0)) : (this.applyPosition({
left: this.activatorOffset.left - e.width
}), this.addRemoveClass("right", !0)) : this.activatorOffset.left - e.width > 0 ? (this.applyPosition({
left: this.activatorOffset.left - e.left - e.width
}), this.addRemoveClass("right", !0)) : (this.applyPosition({
left: this.activatorOffset.width
}), this.addRemoveClass("left", !0)), this.addRemoveClass("horizontal", !0), e = this.getBoundingRect(this.node), e.left < 0 || e.left + e.width > t ? !1 : !0;
},
applyHorizontalPositioning: function() {
if (!this.initHorizontalPositioning()) return !1;
var e = this.getBoundingRect(this.node), t = this.getViewHeight(), n = this.activatorOffset.top + this.activatorOffset.height / 2;
return this.floating ? n >= t / 2 - .05 * t && n <= t / 2 + .05 * t ? this.applyPosition({
top: this.activatorOffset.top + this.activatorOffset.height / 2 - e.height / 2,
bottom: "auto"
}) : this.activatorOffset.top + this.activatorOffset.height < t / 2 ? (this.applyPosition({
top: this.activatorOffset.top - this.activatorOffset.height,
bottom: "auto"
}), this.addRemoveClass("high", !0)) : (this.applyPosition({
top: this.activatorOffset.top - e.height + this.activatorOffset.height * 2,
bottom: "auto"
}), this.addRemoveClass("low", !0)) : n >= t / 2 - .05 * t && n <= t / 2 + .05 * t ? this.applyPosition({
top: (this.activatorOffset.height - e.height) / 2
}) : this.activatorOffset.top + this.activatorOffset.height < t / 2 ? (this.applyPosition({
top: -this.activatorOffset.height
}), this.addRemoveClass("high", !0)) : (this.applyPosition({
top: e.top - e.height - this.activatorOffset.top + this.activatorOffset.height
}), this.addRemoveClass("low", !0)), !0;
},
applyHorizontalFlushPositioning: function(e, t) {
if (!this.initHorizontalPositioning()) return !1;
var n = this.getBoundingRect(this.node), r = this.getViewWidth();
return this.floating ? this.activatorOffset.top < innerHeight / 2 ? (this.applyPosition({
top: this.activatorOffset.top + this.activatorOffset.height / 2
}), this.addRemoveClass("high", !0)) : (this.applyPosition({
top: this.activatorOffset.top + this.activatorOffset.height / 2 - n.height
}), this.addRemoveClass("low", !0)) : n.top + n.height > innerHeight && innerHeight - n.bottom < n.top - n.height ? (this.applyPosition({
top: n.top - n.height - this.activatorOffset.top - this.activatorOffset.height / 2
}), this.addRemoveClass("low", !0)) : (this.applyPosition({
top: this.activatorOffset.height / 2
}), this.addRemoveClass("high", !0)), this.activatorOffset.left + this.activatorOffset.width < e ? (this.addClass("left"), this.addClass("corner"), !0) : this.activatorOffset.left > t ? (this.addClass("right"), this.addClass("corner"), !0) : !1;
},
getBoundingRect: function(e) {
var t = e.getBoundingClientRect();
return !t.width || !t.height ? {
left: t.left,
right: t.right,
top: t.top,
bottom: t.bottom,
width: t.right - t.left,
height: t.bottom - t.top
} : t;
},
getViewHeight: function() {
return window.innerHeight === undefined ? document.documentElement.clientHeight : window.innerHeight;
},
getViewWidth: function() {
return window.innerWidth === undefined ? document.documentElement.clientWidth : window.innerWidth;
},
resetPositioning: function() {
this.removeClass("right"), this.removeClass("left"), this.removeClass("high"), this.removeClass("low"), this.removeClass("corner"), this.removeClass("below"), this.removeClass("above"), this.removeClass("vertical"), this.removeClass("horizontal"), this.applyPosition({
left: "auto"
}), this.applyPosition({
top: "auto"
});
},
resizeHandler: function() {
this.inherited(arguments), this.adjustPosition();
},
requestHide: function() {
this.setShowing(!1);
}
});

// DividerDrawer.js

enyo.kind({
name: "gts.DividerDrawer",
classes: "gts-DividerDrawer",
published: {
caption: "",
open: !0
},
events: {
onChange: ""
},
components: [ {
name: "base",
kind: "enyo.FittableColumns",
noStretch: !0,
classes: "base-bar",
ontap: "toggleOpen",
components: [ {
classes: "end-cap"
}, {
name: "caption",
classes: "caption"
}, {
classes: "bar",
fit: !0
}, {
name: "switch",
classes: "toggle",
value: !1
}, {
classes: "end-cap bar"
} ]
}, {
name: "client",
kind: "onyx.Drawer"
} ],
rendered: function() {
this.inherited(arguments), this.captionChanged(), this.openChanged();
},
reflow: function() {
this.$.base.reflow();
},
openChanged: function() {
this.$["switch"].value = this.open, this.$.client.setOpen(this.$["switch"].value), this.$["switch"].addRemoveClass("checked", this.$["switch"].value), this.reflow();
},
captionChanged: function() {
this.$.caption.setContent(this.caption), this.$.caption.applyStyle("display", this.caption ? "" : "none"), this.reflow();
},
toggleOpen: function(e, t) {
return this.open = !this.$["switch"].value, this.$["switch"].value = this.open, this.openChanged(), this.doChange(this, {
caption: this.getCaption(),
open: this.getOpen()
}), !0;
}
});

// Notification.js

enyo.kind({
name: "enyo.Notification",
kind: "enyo.Control",
published: {
defaultTheme: "notification.Bezel"
},
events: {
onNotify: "",
onTap: ""
},
pending: [],
themes: [],
uid: 0,
sendNotification: function(e, t) {
this.pending.push({
uid: this.uid,
notification: e,
callback: typeof t != "function" ? enyo.nop : t
}), this.getTheme(e.theme || this.defaultTheme).newNotification(e, this.uid), this.doNotify(e), this.uid++;
},
getTheme: function(e) {
for (var t = 0, n = this.themes.length; t < n; t++) if (this.themes[t].name == e) return this.themes[t].component;
var r = this.createComponent({
kind: e,
onTap: "notifTap",
onClose: "notifClose"
}, {
owner: this
});
return this.themes.push({
name: e,
component: r
}), this.themes[this.themes.length - 1].component;
},
notifTap: function(e, t) {
for (var n = 0, r = this.pending.length; n < r; n++) if (this.pending[n].uid == t.uid) return this.pending[n].callback(this.pending[n].notification), this.doTap(this.pending[n].notification), enyo.remove(this.pending[n], this.pending), !0;
},
notifClose: function(e, t) {
for (var n = 0, r = this.pending.length; n < r; n++) if (this.pending[n].uid == t.uid) {
enyo.remove(this.pending[n], this.pending);
return;
}
}
});

// Badged.js

enyo.kind({
name: "notification.Badged",
kind: "enyo.Control",
published: {
defaultDuration: 2e3
},
events: {
onTap: "",
onClose: ""
},
pending: [],
fifo: !1,
components: [ {
kind: "enyo.Control",
ontap: "notifTap",
name: "bubble",
classes: "notification-badged-bubble",
showing: !1,
components: [ {
kind: "enyo.Control",
name: "icon",
classes: "notification-badged-icon"
}, {
kind: "enyo.Control",
name: "title",
classes: "notification-badged-title"
}, {
kind: "enyo.Control",
name: "message",
classes: "notification-badged-message"
}, {
kind: "enyo.Control",
name: "badge",
classes: "notification-badged-badge",
showing: !1
} ]
} ],
create: function() {
this.inherited(arguments), this.render();
},
newNotification: function(e, t) {
this.pending.push({
uid: t,
notification: e
}), this.pending.length == 1 && this.$.bubble.show(), this.displayNotification();
},
displayNotification: function() {
if (this.pending.length == 0) return;
var e = this.upNotif().notification;
this.fifo || enyo.job.stop("hideNotification-Badged"), this.$.badge.setContent(this.pending.length), this.$.badge.setShowing(this.pending.length > 1), this.$.title.setContent(e.title), this.$.message.setContent(e.message), this.$.icon.applyStyle("background-image", "url('" + e.icon + "')"), e.stay || enyo.job("hideNotification-Badged", enyo.bind(this, "hideNotification"), e.duration ? e.duration * 1e3 : this.defaultDuration);
},
hideNotification: function(e) {
enyo.job.stop("hideNotification-Badged"), e || this.doClose({
notification: this.upNotif().notification,
uid: this.upNotif().uid
}), enyo.remove(this.upNotif(), this.pending), this.pending.length > 0 ? this.displayNotification() : this.$.bubble.hide();
},
notifTap: function() {
this.doTap({
notification: this.upNotif().notification,
uid: this.upNotif().uid
}), this.hideNotification();
},
upNotif: function() {
return this.fifo ? this.pending[0] : this.pending[this.pending.length - 1];
}
});

// Bezel.js

enyo.kind({
name: "notification.Bezel",
kind: "enyo.Control",
published: {
defaultDuration: 2e3
},
events: {
onTap: "",
onClose: ""
},
pending: [],
isShow: !0,
components: [ {
kind: "enyo.Control",
classes: "notification-bezel-main",
name: "bubble",
showing: !1,
components: [ {
kind: "enyo.Control",
name: "icon",
classes: "notification-bezel-icon"
}, {
kind: "enyo.Control",
name: "title",
classes: "notification-bezel-title"
}, {
kind: "enyo.Control",
name: "message",
classes: "notification-bezel-message"
} ],
ontap: "notifTap"
}, {
kind: "enyo.Animator",
duration: 1e3,
endValue: 1,
onStep: "fadeAnimation",
onEnd: "animationEnd",
name: "animator"
} ],
create: function() {
this.inherited(arguments), this.render(), this.$.bubble.applyStyle("opacity", 0);
},
newNotification: function(e, t) {
this.pending.push({
uid: t,
notification: e
}), this.pending.length == 1 && (this.$.bubble.show(), this.displayNotification());
},
displayNotification: function() {
if (this.pending.length == 0) {
this.$.bubble.hide();
return;
}
var e = this.pending[0].notification;
this.$.title.setContent(e.title), this.$.message.setContent(e.message), this.$.icon.applyStyle("background-image", "url('" + e.icon + "')"), this.$.animator.stop(), this.isShow = !0, this.$.animator.play();
},
hideNotification: function(e) {
enyo.job.stop("hideNotification-Bezel"), e || this.doClose({
notification: this.pending[0].notification,
uid: this.pending[0].uid
}), this.$.animator.stop(), this.isShow = !1, this.$.animator.play();
},
notifTap: function() {
this.doTap({
notification: this.pending[0].notification,
uid: this.pending[0].uid
}), this.hideNotification();
},
fadeAnimation: function(e) {
var t;
this.isShow ? t = e.value : t = 1 - e.value, this.$.bubble.applyStyle("opacity", t.toFixed(8));
},
animationEnd: function() {
this.isShow ? this.pending[0].notification.stay || enyo.job("hideNotification-Bezel", enyo.bind(this, "hideNotification"), this.pending[0].notification.duration ? this.pending[0].notification.duration * 1e3 : this.defaultDuration) : (this.pending.shift(), enyo.asyncMethod(this, "displayNotification"));
}
});

// MessageBar.js

enyo.kind({
name: "notification.MessageBar",
kind: "enyo.Control",
published: {
defaultDuration: 2e3
},
events: {
onTap: "",
onClose: ""
},
pending: [],
hasDisplayedNotif: !1,
inShow: null,
inHide: null,
components: [ {
kind: "enyo.Animator",
duration: 1e3,
endValue: 1,
onStep: "msgStep",
onEnd: "animationEnd",
name: "showNotif"
}, {
kind: "enyo.Animator",
duration: 1e3,
startValue: 1,
endValue: 0,
onStep: "msgStep",
onEnd: "animationEnd",
name: "hideNotif"
}, {
kind: "enyo.Animator",
duration: 1e3,
endValue: 30,
onStep: "barStep",
onEnd: "animationEnd",
name: "barAnimator"
}, {
kind: "enyo.Control",
name: "bar",
classes: "notification-messagebar-bar"
} ],
create: function() {
this.inherited(arguments), this.render();
},
newNotification: function(e, t) {
this.pending.push({
uid: t,
notification: e,
node: null
}), this.pending.length == 1 && this.$.barAnimator.play();
},
displayNotification: function() {
if (this.pending.length == 0) return;
var e = this.inShow.notification;
this.inShow.node = this.$.bar.createComponent({
kind: "enyo.Control",
classes: "notification-messagebar-notification",
components: [ {
kind: "enyo.Control",
classes: "notification-messagebar-icon",
style: "background-image: url('" + e.icon + "')"
}, {
kind: "enyo.Control",
classes: "notification-messagebar-title",
content: e.title
}, {
kind: "enyo.Control",
classes: "notification-messagebar-message",
content: e.message
} ],
ontap: "notifTap"
}, {
owner: this
}), this.inShow.node.applyStyle("top", "30px"), this.inShow.node.applyStyle("opacity", "0"), this.inShow.node.render(), this.hasDisplayedNotif = !0, this.$.showNotif.play();
},
hideNotification: function(e) {
enyo.job.stop("hideNotification-MessageBar"), e || this.doClose({
notification: this.pending[0].notification,
uid: this.pending[0].uid
}), this.pending.length > 1 ? (this.inShow = this.pending[1], this.displayNotification()) : this.hasDisplayedNotif = !1, this.inHide = this.pending[0], this.$.hideNotif.play();
},
notifTap: function() {
this.doTap({
notification: this.pending[0].notification,
uid: this.pending[0].uid
}), this.hideNotification();
},
msgStep: function(e) {
var t, n;
e.name == "showNotif" ? n = this.inShow.node : n = this.inHide.node, t = e.value, n.applyStyle("opacity", t.toFixed(8)), n.applyStyle("top", (30 - t * 30).toFixed(8) + "px");
},
barStep: function(e) {
var t, n;
this.pending.length == 0 ? t = -e.value : t = -30 + e.value, this.$.bar.applyStyle("bottom", t.toFixed(8) + "px");
},
animationEnd: function(e) {
e.name == "hideNotif" ? (this.inHide.node.destroy(), this.pending.shift(), this.inHide = null, this.pending.length == 0 ? this.$.barAnimator.play() : this.hasDisplayedNotif || (this.inShow = this.pending[0], this.displayNotification())) : e.name == "barAnimator" && this.pending.length ? (this.inShow = this.pending[0], this.displayNotification()) : e.name == "showNotif" && (this.inShow.notification.stay || enyo.job("hideNotification-MessageBar", enyo.bind(this, "hideNotification"), this.inShow.notification.duration ? this.inShow.notification.duration * 1e3 : this.defaultDuration));
}
});

// Pop.js

enyo.kind({
name: "notification.Pop",
kind: "enyo.Control",
published: {
defaultDuration: 2e3
},
events: {
onTap: "",
onClose: ""
},
pending: [],
components: [ {
kind: "enyo.Control",
ontap: "notifTap",
showing: !1,
name: "bubble",
classes: "notification-pop-bubble",
components: [ {
kind: "enyo.Control",
name: "master",
components: [ {
kind: "enyo.Control",
name: "title",
classes: "notification-pop-title"
}, {
kind: "enyo.Control",
name: "message",
classes: "notification-pop-message"
}, {
kind: "enyo.Control",
name: "icon",
classes: "notification-pop-icon"
} ]
} ]
}, {
kind: "enyo.Animator",
easingFunction: enyo.easing.cubicIn,
name: "popUp1",
startValue: 0,
endValue: 1.1,
duration: 900,
onStep: "zoomEffect",
onEnd: "endEffect"
}, {
kind: "enyo.Animator",
name: "popUp2",
startValue: 1.1,
endValue: 1,
duration: 300,
onStep: "zoomEffect",
onEnd: "endEffect"
}, {
kind: "enyo.Animator",
easingFunction: enyo.easing.cubicIn,
name: "zoom",
startValue: 0,
endValue: 1,
duration: 750,
onStep: "zoomOutEffect",
onEnd: "endEffect"
} ],
create: function() {
this.inherited(arguments), this.render(), this.$.bubble.applyStyle("opacity", 0);
},
newNotification: function(e, t) {
this.pending.push({
uid: t,
notification: e
}), this.pending.length == 1 && (this.$.bubble.show(), this.displayNotification());
},
displayNotification: function() {
if (this.pending.length == 0) return;
var e = this.pending[0].notification;
this.$.title.setContent(e.title), this.$.message.setContent(e.message), this.$.icon.applyStyle("background-image", "url('" + e.icon + "')"), this.$.popUp1.play();
},
hideNotification: function(e) {
enyo.job.stop("hideNotification-Pop"), e || this.doClose({
notification: this.pending[0].notification,
uid: this.pending[0].uid
}), this.$.zoom.play();
},
zoomEffect: function(e) {
var t = e.value.toFixed(8);
this.$.bubble.applyStyle("opacity", t), enyo.Layout.transform ? enyo.Layout.transform(this.$.bubble, {
scale: t
}) : enyo.dom.transformValue && enyo.dom.transformValue(this.$.bubble, "scale", t);
},
zoomOutEffect: function(e) {
var t = e.value.toFixed(8);
this.$.bubble.applyStyle("opacity", 1 - t), enyo.Layout.transform ? enyo.Layout.transform(this.$.bubble, {
scale: 1 + t * 5
}) : enyo.dom.transformValue && enyo.dom.transformValue(this.$.bubble, "scale", 1 + t * 5);
var n = 1 - t * 2;
n < 0 && (n = 0), this.$.master.applyStyle("opacity", n.toFixed(8));
},
notifTap: function() {
this.doTap({
notification: this.pending[0].notification,
uid: this.pending[0].uid
}), this.hideNotification();
},
endEffect: function(e) {
if (e.name == "popUp1") this.$.popUp2.play(); else if (e.name == "popUp2") {
var t = this.pending[0].notification;
t.stay || enyo.job("hideNotification-Pop", enyo.bind(this, "hideNotification"), t.duration ? t.duration * 1e3 : this.defaultDuration);
} else this.pending.shift(), this.$.master.applyStyle("opacity", 1), this.pending.length == 0 ? this.$.bubble.hide() : this.displayNotification();
}
});

// PageCurl.js

enyo.kind({
name: "notification.PageCurl",
kind: "enyo.Control",
published: {
defaultDuration: 2e3
},
events: {
onTap: "",
onClose: ""
},
pending: [],
inShow: null,
cornerVisible: !1,
barVisible: !1,
components: [ {
kind: "enyo.Animator",
duration: 1e3,
startValue: 0,
endValue: 1,
onStep: "stepCornerShow",
onEnd: "animationEnd",
name: "showCornerAnimation"
}, {
kind: "enyo.Animator",
duration: 1e3,
startValue: 0,
endValue: 1,
onStep: "stepCornerAll",
onEnd: "animationEnd",
name: "allCornerAnimation"
}, {
kind: "enyo.Animator",
duration: 1e3,
startValue: 0,
endValue: 100,
onStep: "stepBarShow",
onEnd: "animationEnd",
name: "showBarAnimation"
}, {
kind: "enyo.Animator",
duration: 1e3,
startValue: 0,
endValue: 80,
onStep: "stepBarHide",
onEnd: "animationEnd",
name: "hideBarAnimation"
}, {
kind: "enyo.Animator",
duration: 1e3,
startValue: 0,
endValue: 1,
onStep: "stepLiveShow",
onEnd: "animationEnd",
name: "showLiveAnimation"
}, {
kind: "enyo.Animator",
duration: 1e3,
startValue: 1,
endValue: 0,
onStep: "stepLiveHide",
onEnd: "animationEnd",
name: "hideLiveAnimation"
}, {
kind: "enyo.Control",
name: "curl",
classes: "notification-pagecurl-curl",
ontap: "curlTap",
components: [ {
kind: "enyo.Control",
name: "count",
content: "0",
classes: "notification-pagecurl-curl-count"
} ]
}, {
kind: "enyo.Control",
name: "bar",
classes: "notification-pagecurl-messagebar",
components: [ {
kind: "onyx.IconButton",
classes: "notification-pagecurl-messagebar-close",
ontap: "barCloseTap"
}, {
kind: "enyo.Scroller",
name: "scroller",
classes: "notification-pagecurl-messagebar-scroller",
vertical: "hidden",
horizontal: "auto"
} ]
}, {
kind: "enyo.Control",
showing: !1,
name: "message",
classes: "notification-pagecurl-message",
ontap: "liveClose",
components: [ {
kind: "enyo.Image",
name: "icon"
}, {
kind: "enyo.Control",
name: "title",
content: "My title",
classes: "notification-pagecurl-message-title"
}, {
kind: "enyo.Control",
name: "text",
content: "My text",
classes: "notification-pagecurl-message-text"
} ]
} ],
create: function() {
this.inherited(arguments), this.render();
},
newNotification: function(e, t) {
this.pending.push({
uid: t,
notification: e,
node: null
}), e.stay ? this.newStayNotif(this.pending[this.pending.length - 1]) : this.newLiveNotif();
},
getStayCount: function() {
var e = 0;
for (var t = 0; t < this.pending.length; t++) this.pending[t].notification.stay && e++;
return e;
},
showCorner: function() {
var e = this.getStayCount();
this.$.count.setContent(e), e > 0 && !this.cornerVisible && !this.barVisible && this.$.showCornerAnimation.play();
},
newStayNotif: function(e) {
e.node = this.$.scroller.createComponent({
kind: "enyo.Control",
classes: "notification-pagecurl-messagebar-message",
ontap: "stayClose",
refNotification: e,
components: [ {
kind: "enyo.Image",
attributes: {
src: e.notification.icon
}
}, {
kind: "enyo.Control",
content: e.notification.title,
classes: "notification-pagecurl-messagebar-message-title"
}, {
kind: "enyo.Control",
content: e.notification.message,
classes: "notification-pagecurl-messagebar-message-text"
} ]
}, {
owner: this
}), this.$.scroller.render(), this.showCorner();
},
newLiveNotif: function() {
if (this.inShow != null) return;
for (var e = 0; e < this.pending.length; e++) this.inShow == null && !this.pending[e].notification.stay && (this.inShow = this.pending[e]);
if (this.inShow == null) return;
this.$.title.setContent(this.inShow.notification.title), this.$.text.setContent(this.inShow.notification.message), this.$.icon.setAttribute("src", this.inShow.notification.icon), this.$.message.applyStyle("opacity", 0), this.$.message.applyStyle("display", "block"), this.$.message.applyStyle("top", this.barVisible ? "80px" : "0px"), this.$.showLiveAnimation.play();
},
hideNotification: function(e) {
enyo.job.stop("hideNotification-PageCurl"), e || this.doClose({
notification: this.inShow.notification,
uid: this.inShow.uid
}), this.$.showLiveAnimation.stop(), this.$.hideLiveAnimation.play();
},
stepCornerShow: function(e) {
var t = {
x: -150,
y: -120
}, n = {
x: 115,
y: 120
};
this.$.curl.applyStyle("right", t.x + n.x * e.value + "px"), this.$.curl.applyStyle("top", t.y + n.y * e.value + "px");
},
stepCornerAll: function(e) {
var t = -35, n = 35;
this.$.curl.applyStyle("right", t + n * e.value + "px");
},
stepBarShow: function(e) {
this.$.curl.applyStyle("right", e.value + "%"), this.$.bar.applyStyle("left", 100 - e.value + "%");
},
stepBarHide: function(e) {
this.$.bar.applyStyle("top", -e.value + "px");
},
stepLiveShow: function(e) {
this.$.message.applyStyle("opacity", e.value.toFixed(8));
},
stepLiveHide: function(e) {
this.$.message.applyStyle("opacity", e.value.toFixed(8));
},
animationEnd: function(e) {
e == this.$.allCornerAnimation ? this.$.showBarAnimation.play() : e == this.$.showBarAnimation ? (this.cornerVisible = !1, this.barVisible = !0) : e == this.$.hideBarAnimation ? (this.barVisible = !1, this.$.bar.applyStyle("left", "100%"), this.$.bar.applyStyle("top", "0px"), this.showCorner()) : e == this.$.showLiveAnimation ? enyo.job("hideNotification-PageCurl", enyo.bind(this, "hideNotification"), this.inShow.notification.duration ? this.inShow.notification.duration * 1e3 : this.defaultDuration) : e == this.$.hideLiveAnimation ? (enyo.remove(this.inShow, this.pending), this.inShow = null, this.$.message.applyStyle("display", "none"), this.newLiveNotif()) : e == this.$.showCornerAnimation && (this.cornerVisible = !0);
},
curlTap: function(e, t) {
this.$.showCornerAnimation.isAnimating() && (this.$.showCornerAnimation.stop(), this.$.curl.applyStyle("top", "0px")), this.$.showBarAnimation.play();
},
liveClose: function() {
return this.doTap({
notification: this.inShow.notification,
uid: this.inShow.uid
}), this.hideNotification(!0, this.inShow), !0;
},
stayClose: function(e) {
var t = e.refNotification;
return this.doTap({
notification: t.notification,
uid: t.uid
}), enyo.remove(t, this.pending), e.destroy(), this.getStayCount() == 0 && this.barCloseTap(), !0;
},
barCloseTap: function() {
this.$.allCornerAnimation.stop(), this.$.hideBarAnimation.play();
}
});

// App.js

enyo.kind({
name: "App",
classes: "onyx",
fit: !0,
components: [ {
kind: "FittableRows",
classes: "enyo-fit",
components: [ {
classes: "app-header",
components: [ {
classes: "app-header-content",
components: [ {
content: "La gendarmerie de l'h\u00e9rault",
classes: "app-header-title"
}, {
content: "vous prot\u00e8ge",
classes: "app-header-subtitle"
} ]
}, {
kind: "Image",
src: "assets/blason-1.png",
style: "width: 32px; padding-bottom: 32px;"
} ]
}, {
kind: "Panels",
name: "mainPanels",
arrangerKind: "CardArranger",
margin: 0,
classes: "app-main-panels",
draggable: !1,
fit: !0,
components: [ {
kind: "AccueilPanel",
name: "accueil",
active: !0
}, {
kind: "BrigadePanel",
name: "brigade"
}, {
kind: "ComptePanel",
name: "compte"
}, {
kind: "AidePanel",
name: "aide"
} ]
}, {
kind: "Notification",
name: "notif"
}, {
classes: "app-footer-border"
}, {
kind: "onyx.RadioGroup",
name: "menu",
onActivate: "menuActivated",
classes: "app-footer-menu-group",
controlClasses: "app-footer-menu",
components: [ {
name: "menuAccueil",
active: !0,
ontap: "resetAccueil",
components: [ {
kind: "FittableRows",
components: [ {
kind: "onyx.Icon",
classes: "icon",
src: "assets/accueil-1.png"
}, {
content: "Accueil"
} ]
} ]
}, {
name: "menuBrigade",
components: [ {
kind: "FittableRows",
components: [ {
kind: "onyx.Icon",
classes: "icon",
src: "assets/brigades-1.png"
}, {
content: "Brigade"
} ]
} ]
}, {
name: "menuAide",
components: [ {
kind: "FittableRows",
components: [ {
kind: "onyx.Icon",
classes: "icon",
src: "assets/aide-1.png"
}, {
content: "Aide"
} ]
} ]
} ]
} ]
} ],
create: function() {
this.inherited(arguments);
},
resetAccueil: function(e, t) {
this.$.accueil.$.accueilPanels.setIndex(0), this.$.accueil.removeActiveMenu();
},
menuActivated: function(e, t) {
if (t.originator.getActive()) {
var n = t.originator.getName();
n == "menuAccueil" ? this.$.mainPanels.setIndex(0) : n == "menuBrigade" ? this.$.mainPanels.setIndex(1) : n == "menuAide" && this.$.mainPanels.setIndex(3);
}
},
message: function(e, t, n) {
this.$.notif.sendNotification({
title: e,
message: t,
icon: "icon.png",
theme: "notification.MessageBar",
stay: undefined,
duration: n
}, enyo.bind(this, "callb"));
},
callb: function(e) {
alert("click!");
}
});

// Accueil.js

enyo.kind({
name: "AccueilPanel",
fit: !0,
components: [ {
kind: "FittableRows",
classes: "enyo-fit",
components: [ {
kind: "FittableColumns",
classes: "accueil-menu",
components: [ {
kind: "onyx.Button",
name: "btnActu",
ontap: "onTapMenu",
components: [ {
kind: "FittableRows",
components: [ {
kind: "onyx.Icon",
classes: "icon",
src: "assets/bouclier-1.png"
}, {
content: "Pr\u00e9vention"
} ]
} ]
}, {
kind: "onyx.Button",
name: "btnAlerte",
ontap: "onTapMenu",
components: [ {
kind: "FittableRows",
components: [ {
kind: "onyx.Icon",
classes: "icon",
src: "assets/alerte-1.png"
}, {
content: "Alerte 17"
} ]
} ]
}, {
kind: "onyx.Button",
name: "btnOTV",
ontap: "onTapMenu",
components: [ {
kind: "FittableRows",
components: [ {
kind: "onyx.Icon",
classes: "iconOtv",
src: "assets/parasol-1.png",
style: "height: 18px;"
}, {
content: "D\u00e9part"
}, {
content: "vacances"
} ]
} ]
} ]
}, {
kind: "Panels",
name: "accueilPanels",
classes: "accueil-panels",
arrangerKind: "CardSlideInArranger",
draggable: !1,
fit: !0,
components: [ {
kind: "PreventMsgPanel",
name: "astuce"
}, {
kind: "ProtegerPanel",
name: "proteger"
}, {
kind: "AlertePanel",
name: "alerte"
}, {
kind: "OTVPanel",
name: "otv"
} ]
} ]
} ],
create: function() {
this.inherited(arguments);
},
onTapMenu: function(e, t) {
var n = e.getName();
this.removeActiveMenu(), e.addClass("active"), n == "btnActu" ? this.$.accueilPanels.setIndex(1) : n == "btnAlerte" ? this.$.accueilPanels.setIndex(2) : n == "btnOTV" && this.$.accueilPanels.setIndex(3);
},
removeActiveMenu: function() {
this.$.astuce.randomText(), this.$.btnActu.removeClass("active"), this.$.btnAlerte.removeClass("active"), this.$.btnOTV.removeClass("active");
}
});

// Actualite.js

enyo.kind({
name: "ActualitePanel",
fit: !0,
components: [ {
kind: "FittableRows",
classes: "enyo-fit",
components: [ {
kind: "onyx.Toolbar",
classes: "actualite-title",
components: [ {
content: "Actualit\u00e9s"
} ]
}, {
kind: "Scroller",
classes: "actualite-scroller",
fit: !0,
components: [ {
kind: "Repeater",
onSetupItem: "setupNews",
components: [ {
name: "item",
components: [ {
tag: "span",
name: "mess"
} ]
} ]
} ]
} ]
} ],
create: function() {
this.inherited(arguments);
}
});

// Brigade.js

enyo.kind({
name: "BrigadePanel",
components: [ {
kind: "FittableRows",
classes: "enyo-fit",
components: [ {
kind: "FittableColumns",
classes: "brigade-input-container",
components: [ {
kind: "onyx.InputDecorator",
classes: "brigade-input",
fit: !0,
components: [ {
kind: "onyx.Input",
name: "commune",
placeholder: "Recherchez une commune",
oninput: "brigadeSearchInput"
} ]
}, {
kind: "onyx.Button",
classes: "brigade-geolocalisation-button",
content: "G",
ontap: "geolocationTap",
components: [ {
kind: "onyx.Icon",
classes: "icon",
src: "assets/gps-1.png"
} ]
} ]
}, {
content: "Tappez sur votre commune pour la mettre en pr\u00e9f\u00e9rence",
classes: "brigade-info"
}, {
kind: "List",
name: "brigadeList",
rowsPerPage: 5,
multiSelect: !1,
onSetupItem: "setupBrigade",
fit: !0,
components: [ {
kind: "onyx.Groupbox",
classes: "brigade-item",
ontap: "brigadeTap",
components: [ {
kind: "onyx.GroupboxHeader",
name: "ville",
content: ""
}, {
components: [ {
name: "type",
content: "",
classes: "brigade-item-type",
fit: !0
}, {
kind: "FittableColumns",
components: [ {
content: "Adresse",
classes: "label brigade-item-label"
}, {
name: "adresse",
content: "",
fit: !0,
classes: "brigade-item-content"
} ]
}, {
kind: "FittableColumns",
showing: !1,
components: [ {
content: "Code Postal",
classes: "label brigade-item-label"
}, {
name: "codepostal",
content: "",
fit: !0,
classes: "brigade-item-content"
} ]
}, {
kind: "FittableColumns",
components: [ {
content: "Ville",
classes: "label brigade-item-label"
}, {
name: "brigade",
content: "",
fit: !0,
classes: "brigade-item-content"
} ]
}, {
kind: "FittableColumns",
components: [ {
content: "T\u00e9l\u00e9phone",
classes: "label brigade-item-label"
}, {
name: "telephone",
content: "",
fit: !0,
classes: "brigade-item-content"
} ]
}, {
kind: "FittableColumns",
name: "faxGroup",
showing: !1,
components: [ {
content: "Fax",
classes: "label brigade-item-label"
}, {
name: "fax",
content: "",
fit: !0,
classes: "brigade-item-content"
} ]
}, {
kind: "FittableColumns",
components: [ {
content: "Email",
classes: "label brigade-item-label"
}, {
name: "email",
content: "",
fit: !0,
classes: "brigade-item-content"
} ]
}, {
classes: "brigade-item-group",
showing: !1,
components: [ {
kind: "onyx.Button",
content: "T"
}, {
kind: "onyx.Button",
content: "M"
} ]
} ]
} ]
} ]
} ]
} ],
create: function() {
this.inherited(arguments);
var e = new Object;
for (var t = 0; t < bddBrigades.type.length; t++) e[bddBrigades.type[t].id] = {
nom: bddBrigades.type[t].nom,
detail: bddBrigades.type[t].detail
};
var n = new Object;
for (var t = 0; t < bddBrigades.brigades.length; t++) n[bddBrigades.brigades[t].id] = {
nom: bddBrigades.brigades[t].nom,
compagnie: bddBrigades.brigades[t].compagnie,
type: bddBrigades.brigades[t].type,
cob: bddBrigades.brigades[t].cob,
adresse: bddBrigades.brigades[t].adresse,
telephone: bddBrigades.brigades[t].telephone,
fax: bddBrigades.brigades[t].fax
};
var r = new Object;
for (var t = 0; t < bddBrigades.groupe.length; t++) r[bddBrigades.groupe[t].id] = {
nom: bddBrigades.groupe[t].nom,
compagnie: bddBrigades.groupe[t].compagnie
};
this.ville = bddBrigades.villes, this.ville.sort(function(e, t) {
return e.nom < t.nom ? -1 : e.nom > t.nom ? 1 : 0;
}), this.brigade = {
brigade: n,
type: e,
groupe: r
}, this.compagnie = bddBrigades.compagnies, this.$.brigadeList.setCount(this.ville.length), this.$.brigadeList.reset();
},
setupBrigade: function(e, t) {
var n = t.index, r = this.filter ? this.filtered : this.ville, i = r[n], s = this.brigade.brigade[i.brigade], o = this.brigade.type[s.type].nom, u = "", a = "";
u = s.type == 1 || s.type == 4 ? s.nom : this.brigade.groupe[s.cob].nom, s.type != 4 && (a = s.type == 1 ? o.toLowerCase() : "cob", a += "." + this.removeAccent(u).replace(/[ '//]/gi, "-").toLowerCase() + "@gendarmerie.interieur.gouv.fr"), this.$.ville.setContent(i.CP + " - " + i.nom), this.$.type.setContent(this.brigade.type[s.type].detail), this.$.adresse.setContent(s.adresse), this.$.brigade.setContent(u), this.$.telephone.setContent(s.telephone), this.$.fax.setContent(s.fax), s.fax ? this.$.faxGroup.setShowing(!0) : this.$.faxGroup.setShowing(!1), this.$.email.setContent(a.replace("--", "-"));
},
brigadeSearchInput: function(e) {
enyo.job(this.id + ":search", enyo.bind(this, "filterBrigadeList", e.getValue()), 200);
},
filterBrigadeList: function(e) {
e != this.filter && (this.filter = e, this.filtered = this.generateFilteredData(e), this.$.brigadeList.setCount(this.filtered.length), this.$.brigadeList.reset());
},
removeAccent: function(e) {
var t = e.replace(/[\u00e0\u00e2\u00e4]/gi, "a");
return t = t.replace(/[\u00e9\u00e8\u00ea\u00eb]/gi, "e"), t = t.replace(/[\u00ee\u00ef]/gi, "i"), t = t.replace(/[\u00f4\u00f6]/gi, "o"), t = t.replace(/[\u00f9\u00fb\u00fc]/gi, "u"), t;
},
generateFilteredData: function(e) {
var t = new RegExp("^" + this.removeAccent(e), "i"), n = [];
for (var r = 0, i; i = this.ville[r]; r++) (this.removeAccent(i.nom).match(t) || i.CP.toString().match(t)) && n.push(i);
return n;
},
callb: function(e) {},
brigadeTap: function(e, t) {
var n = this.filter ? this.filtered : this.ville, r = n[t.index];
localStorage.setItem("idBrigade", r.brigade), localStorage.setItem("CPCompte", r.CP), localStorage.setItem("cityCompte", r.nom), enyo.$.app.$.compte.$.CPCompte.setValue(r.CP), enyo.$.app.$.compte.$.cityCompte.setValue(r.nom), enyo.$.app.message("Brigade enregistr\u00e9e : ", this.getBrigade(r.brigade).nom, 1);
},
getBrigade: function(e) {
if (e > this.brigade.brigade.length) return {};
var t = this.brigade.brigade[e], n = this.brigade.type[t.type].nom, r = t.type == 1 || t.type == 4 ? t.nom : this.brigade.groupe[t.cob].nom, i = t.type == 1 ? n.toLowerCase() : "cob";
i += "." + this.removeAccent(r).replace("/", "").replace(/ /gi, "-").toLowerCase() + "@gendarmerie.interieur.gouv.fr";
var s = {
type: this.brigade.type[t.type].detail,
nom: r,
email: i.replace("--", "-"),
adresse: t.adresse,
telephone: t.telephone,
fax: t.fax
};
return s;
},
geolocationTap: function() {
navigator.geolocation.getCurrentPosition(enyo.bind(this, "getPosition"), enyo.bind(this, "errorCoord"));
},
getPosition: function(e) {
var t = new enyo.Ajax({
url: "http://nominatim.openstreetmap.org/reverse?format=json&lat=" + e.coords.latitude + "&lon=" + e.coords.longitude
});
t.go(), t.response(this, "putCity"), t.error(this, "errorCoord");
},
putCity: function(e, t) {
this.$.commune.setValue(t.address.city), this.brigadeSearchInput(this.$.commune);
},
errorCoord: function(e, t) {
enyo.$.app.message("GPS : ", "G\u00e9olocalisation impossible", 1);
},
parseCsv: function(e, t) {
strDelimiter = strDelimiter || ",";
var n = new RegExp("(\\" + strDelimiter + "|\\r?\\n|\\r|^)" + '(?:"([^"]*(?:""[^"]*)*)"|' + '([^"\\' + strDelimiter + "\\r\\n]*))", "gi"), r = [ [] ], i = null;
while (i = n.exec(strData)) {
var s = i[1];
s.length && s != strDelimiter && r.push([]);
if (i[2]) var o = i[2].replace(new RegExp('""', "g"), '"'); else var o = i[3];
r[r.length - 1].push(o);
}
return r;
}
});

// Aide.js

enyo.kind({
name: "AidePanel",
fit: !0,
components: [ {
kind: "Scroller",
classes: "enyo-fit",
components: [ {
kind: "FittableRows",
classes: "aide-scroller",
components: [ {
kind: "gts.DividerDrawer",
caption: "Mentions l\u00e9gales",
open: !1,
components: [ {
content: "L'application pour Smartphone \u00ab Stop cambriolages \u00bb est \u00e9dit\u00e9e par le groupement de gendarmerie d\u00e9partementale de l'H\u00e9rault, 359 rue de Fontcouverte, 34056 Montpellier, en partenariat avec EPITECH, 16 boulevard du Jeu de Paume 34000 Montpellier.<br /><br />Le groupement de gendarmerie de l'H\u00e9rault met tout en \u0153uvre pour offrir aux utilisateurs de cette application des services et des outils disponibles et conformes \u00e0 la r\u00e9glementation. Il ne saurait \u00eatre tenu pour responsable de l'impossibilit\u00e9 pour l'utilisateur, pour quelque raison que ce soit, d'acc\u00e9der \u00e0 l'application. Le groupement de gendarmerie de l'H\u00e9rault ne saurait \u00e9galement \u00eatre tenu pour responsable de toute perte d'informations et/ou de donn\u00e9es par l'utilisateur.<br /><br />Les informations et les donn\u00e9es contenues dans l'application sont fournies \u00e0 titre indicatif ; en aucun cas, elles ne peuvent engager la responsabilit\u00e9 de la gendarmerie nationale. Elles peuvent \u00eatre modifi\u00e9es ou mises \u00e0 jour \u00e0 tout moment, sans pr\u00e9avis. Le groupement de gendarmerie de l'H\u00e9rault se r\u00e9serve le droit, \u00e0 tout moment et sans pr\u00e9avis, d'apporter des am\u00e9liorations et/ou des modifications \u00e0 l'application.<br /><br />L'int\u00e9gralit\u00e9 de l'application et des liens qui la composent est prot\u00e9g\u00e9e par la l\u00e9gislation relative \u00e0 la propri\u00e9t\u00e9 intellectuelle. La gendarmerie nationale est titulaire de l'ensemble des droits de propri\u00e9t\u00e9 intellectuelle aff\u00e9rent \u00e0 cette application ainsi qu'aux \u00e9l\u00e9ments qui la composent. Toute utilisation non autoris\u00e9e d'un ou plusieurs \u00e9l\u00e9ments composant cette application fera syst\u00e9matiquement l'objet de poursuites judiciaires.<br /><br />Il est interdit de t\u00e9l\u00e9phoner ou de consulter son t\u00e9l\u00e9phone en conduisant. En cas d'accident du \u00e0 l'utilisation de cette application en conduisant, le groupement de gendarmerie de l'H\u00e9rault se d\u00e9charge de toute responsabilit\u00e9.<br /><br />L'application a \u00e9t\u00e9 d\u00e9velopp\u00e9e par MAURIERES Gaelle, PICOT Florian, JACQUEL Pierre, MORAL\u00c8S Cyril, NGORAN Emmanuel sous la direction de LAURENT Edouard et de FRAISSE Thomas en liaison avec le lieutenant colonel MERIAUX Hubert, officier adjoint au commandement de groupement de gendarmerie d\u00e9partementale de l'H\u00e9rault.",
allowHtml: !0,
classes: "format-aide"
} ]
}, {
kind: "gts.DividerDrawer",
caption: "Pr\u00e9sentation g\u00e9n\u00e9rale",
open: !1,
components: [ {
content: "Le dispositif \u00ab Stop cambriolages \u00bb est optimis\u00e9 pour un large public et est disponible sur \u00ab l'app store \u00bb et sur \u00ab Google play \u00bb. Les services propos\u00e9s supposent au pr\u00e9alable d'avoir t\u00e9l\u00e9charg\u00e9 et activ\u00e9 l'application sur un terminal mobile compatible IOS ou ANDROID.<br /><br />L'application \u00ab Stop cambriolages \u00bb est un outil ayant pour but de vous donner un maximum de conseils pour vous pr\u00e9munir contre les cambriolages. Vous vous absentez de chez vous...Avez-vous pens\u00e9 \u00e0 tout ? <br />Si, vous \u00eates victime d'un cambriolage ou d'une agression arm\u00e9e, vous trouverez les conseils pour vous aider \u00e0 r\u00e9agir en vous donnant le maximum de chance de retrouver vos biens d\u00e9rob\u00e9s et de faire arr\u00eater les auteurs. Un bouton d'appel du \u00ab 17 \u00bb vous oriente automatiquement vers le service comp\u00e9tent de la police ou de la gendarmerie. <br />Si vous vous absentez durablement, l'application \u00ab Stop cambriolages \u00bb vous aidera \u00e0 effectuer  votre d\u00e9marche aupr\u00e8s de la gendarmerie dans le cadre de l'Op\u00e9ration Tranquillit\u00e9 Vacances \u2013 OTV- afin qu'une patrouille se rende r\u00e9guli\u00e8rement \u00e0 votre domicile.",
allowHtml: !0,
classes: "format-aide"
} ]
}, {
kind: "gts.DividerDrawer",
caption: "Page d'accueil",
open: !1,
components: [ {
content: "En activant l'application, vous obtiendrez \u00e0 \u00ab l'accueil \u00bb des messages de pr\u00e9vention vari\u00e9s. Si vous poss\u00e9dez un acc\u00e8s internet, vous obtiendrez en compl\u00e9ment des messages de pr\u00e9vention actualis\u00e9s, diffus\u00e9s en temps r\u00e9el et des informations vari\u00e9es relatives \u00e0 votre s\u00e9curit\u00e9. Pour cela vous devez accepter les mises \u00e0 jour de l'application.",
allowHtml: !0,
classes: "format-aide"
} ]
}, {
kind: "gts.DividerDrawer",
caption: "Pr\u00e9vention",
open: !1,
components: [ {
content: "L'application \u00ab Stop cambriolages \u00bb vous offre un grand nombre de conseils pour mieux prot\u00e9ger votre domicile. Elle rappelle les bons r\u00e9flexes \u00e0 adopter pour emp\u00eacher les cambriolages du mieux possible, et les choses \u00e0 effectuer si vous vous absentez durablement.",
classes: "format-aide"
} ]
}, {
kind: "gts.DividerDrawer",
caption: "Alerte",
open: !1,
components: [ {
content: "Si vous \u00eates victime d'un cambriolage, l'application \u00ab Stop cambriolages \u00bb vous donne les conseils pour pr\u00e9server les traces et les indices permettant de retrouver les auteurs et par cons\u00e9quent r\u00e9cup\u00e9rer vos biens vol\u00e9s. Si vous \u00eates victime d'une agression \u00e0 main arm\u00e9e, vous obtiendrez des conseils pratiques pour faciliter l'identification de l'agresseur. Un bouton d'appel du \u00ab 17 \u00bb vous permet d'alerter imm\u00e9diatement les services de police ou de gendarmerie comp\u00e9tents.",
classes: "format-aide"
} ]
}, {
kind: "gts.DividerDrawer",
caption: "D\u00e9part en vacances",
open: !1,
components: [ {
content: "Si vous vous absentez durablement, notamment en p\u00e9riode de vacances scolaires, vous pouvez vous inscrire au dispositif \u00abOp\u00e9ration Tranquillit\u00e9 Vacances \u00bb (OTV) \u00e0 la brigade de gendarmerie dont vous relevez, afin d'obtenir une protection particuli\u00e8re de votre domicile. L'application \u00ab Stop cambriolages \u00bb facilite vos d\u00e9marches en vous indiquant la marche \u00e0 suivre pour votre inscription.",
classes: "format-aide"
} ]
}, {
kind: "gts.DividerDrawer",
caption: "Brigades",
open: !1,
components: [ {
content: "L'application \u00ab Stop cambriolages \u00bb vous offre un outil permettant de conna\u00eetre la brigade dont vous d\u00e9pendez. En tapant le nom de votre commune, vous acc\u00e9derez aux informations de localisation de votre brigade.",
classes: "format-aide"
} ]
} ]
} ]
} ]
});

// Proteger.js

enyo.kind({
name: "ProtegerPanel",
fit: !0,
components: [ {
kind: "FittableRows",
classes: "enyo-fit",
components: [ {
kind: "onyx.Toolbar",
classes: "proteger-toolbar",
components: [ {
content: "Pr\u00e9vention"
} ]
}, {
kind: "FittableColumns",
classes: "proteger-navigation",
components: [ {
kind: "onyx.Icon",
name: "flecheprevious",
classes: "proteger-fleche",
src: "assets/gauche-1.png",
ontap: "prevPanel"
}, {
name: "titleproteger",
content: "Prot\u00e9ger son domicile",
fit: !0,
classes: "proteger-title"
}, {
kind: "onyx.Icon",
name: "flechenext",
classes: "proteger-fleche",
src: "assets/droite-1.png",
ontap: "nextPanel"
} ]
}, {
kind: "Panels",
name: "protegerPanels",
fit: !0,
arrangerKind: "CardArranger",
classes: "proteger-panels",
onTransitionFinish: "titlePanel",
components: [ {
kind: "Scroller",
fit: !0,
components: [ {
kind: "Repeater",
name: "protegerList",
onSetupItem: "setProteger",
components: [ {
kind: "onyx.Groupbox",
classes: "proteger-groupbox",
components: [ {
name: "protegerItem",
content: ""
} ]
} ]
} ]
}, {
kind: "Scroller",
classes: "enyo-fit",
components: [ {
kind: "Repeater",
name: "gestesList",
onSetupItem: "setGestes",
components: [ {
kind: "onyx.Groupbox",
classes: "proteger-groupbox",
components: [ {
name: "gestesItem",
content: ""
} ]
} ]
} ]
}, {
kind: "Scroller",
classes: "enyo-fit",
components: [ {
kind: "Repeater",
name: "absenceList",
onSetupItem: "setAbsence",
components: [ {
kind: "onyx.Groupbox",
classes: "proteger-groupbox",
components: [ {
name: "absenceItem",
content: ""
} ]
} ]
} ]
} ]
} ]
} ],
create: function() {
this.inherited(arguments);
var e = messages.prevention;
this.textProteger = [];
for (var t = 0; t <= 6; t++) this.textProteger.push(e[t].texte);
this.textGestes = [];
for (var t = 7; t <= 18; t++) this.textGestes.push(e[t].texte);
this.textAbsence = [];
for (var t = 19; t <= 24; t++) this.textAbsence.push(e[t].texte);
this.$.protegerList.setCount(this.textProteger.length), this.$.gestesList.setCount(this.textGestes.length), this.$.absenceList.setCount(this.textAbsence.length);
},
prevPanel: function() {
this.$.protegerPanels.previous(), this.titlePanel();
},
updateArrows: function() {
var e = this.$.protegerPanels, t = e.getPanels(), n = this.$.flecheprevious, r = this.$.flechenext;
t.length == 1 ? (n.applyStyle("background-position", "left bottom"), r.applyStyle("background-position", "left bottom")) : e.index == 0 ? (n.applyStyle("background-position", "left bottom"), r.applyStyle("background-position", "left top")) : (n.applyStyle("background-position", "left top"), e.index == t.length - 1 ? r.applyStyle("background-position", "left bottom") : r.applyStyle("background-position", "left top"));
},
nextPanel: function() {
this.$.protegerPanels.next(), this.titlePanel();
},
titlePanel: function() {
this.updateArrows(), this.$.protegerPanels.index <= 0 ? this.$.titleproteger.setContent("Prot\u00e9ger son domicile") : this.$.protegerPanels.index == 1 ? this.$.titleproteger.setContent("Les bonnes pratiques") : this.$.protegerPanels.index == 2 && this.$.titleproteger.setContent("En cas d'absence durable");
},
setProteger: function(e, t) {
var n = t.index, r = t.item;
if (r) return r.$.protegerItem.setContent(this.textProteger[n]), !0;
},
setGestes: function(e, t) {
var n = t.index, r = t.item;
if (r) return r.$.gestesItem.setContent(this.textGestes[n]), !0;
},
setAbsence: function(e, t) {
var n = t.index, r = t.item;
if (r) return r.$.absenceItem.setContent(this.textAbsence[n]), !0;
}
});

// fichier_messages.js

var messages = {
prevention: [ {
texte: "Lorsque vous prenez possession d'un nouvel appartement ou d'une maison, pensez \u00e0 changer les serrures."
}, {
texte: "\u00c9quipez votre porte d'un syst\u00e8me de fermeture fiable et d'un entreb\u00e2illeur."
}, {
texte: "Installez des \u00e9quipements adapt\u00e9s et agr\u00e9\u00e9s (volets, grilles, \u00e9clairage automatique int\u00e9rieur/ext\u00e9rieur, alarmes ou protection \u00e9lectronique...). Demandez conseil \u00e0 un professionnel."
}, {
texte: "N'inscrivez pas vos nom et adresse sur votre trousseau de cl\u00e9s."
}, {
texte: "Si vous avez perdu vos cl\u00e9s et que l'on peut indentifier votre adresse, changez les serrures."
}, {
texte: "Ne laissez pas vos cl\u00e9s sous le paillasson ou le pot de fleurs, confiez-les plut\u00f4t \u00e0 une personne de confiance."
}, {
texte: "Si vous poss\u00e9dez un coffre-fort, il ne doit pas \u00eatre visible des personnes qui passent chez vous."
}, {
texte: "Fermez la porte \u00e0 double tour, m\u00eame lorsque vous \u00eates chez vous. Soyez vigilant sur tous les acc\u00e8s, ne laissez pas une cl\u00e9 sur la serrure int\u00e9rieure d'une porte vitr\u00e9e."
}, {
texte: "De nuit, en p\u00e9riode estivale, \u00e9vitez de laisser les fen\u00eatres ouvertes, surtout si elles sont accessibles depuis la voie publique."
}, {
texte: "Ne laissez pas tra\u00eener dans le jardin, une \u00e9chelle, des outils, un \u00e9chafaudage..."
}, {
texte: "Avant de laisser quelqu'un p\u00e9n\u00e9trer dans votre domicile, assurez-vous de son identit\u00e9 en utilisant l'interphone, le judas ou l'entreb\u00e2illeur de porte."
}, {
texte: "En cas de doute, m\u00eame si des cartes professionnelles vous sont pr\u00e9sent\u00e9es, appelez le service ou la soci\u00e9t\u00e9 dont vos interlocuteurs se r\u00e9clament."
}, {
texte: "Ne laissez jamais une personne inconnue seule dans une pi\u00e8ce de votre domicile."
}, {
texte: "Placez en lieu s\u00fbr et \u00e9loign\u00e9 des acc\u00e8s, vos bijoux, carte de cr\u00e9dit, sac \u00e0 main, cl\u00e9s de voiture et ne laissez pas d'objets de valeur qui soient visibles \u00e0 travers les fen\u00eatres."
}, {
texte: "Photographiez vos objets de valeur pour faciliter les recherches en cas de vol."
}, {
texte: "Notez le num\u00e9ro de s\u00e9rie et la r\u00e9f\u00e9rence des mat\u00e9riels, conservez vos factures, ou expertises pour les objets de tr\u00e8s grande valeur."
}, {
texte: "R\u00e9pertoriez vos biens sur une liste."
}, {
texte: "Lorsque vous vous absentez, fermez vos portes et fen\u00eatres."
}, {
texte: "Signalez \u00e0 la brigade de gendarmerie tout fait suspect pouvant laisser pr\u00e9sager la pr\u00e9paration ou la commission d'un cambriolage."
}, {
texte: "Avisez vos voisins ou le gardien de votre r\u00e9sidence."
}, {
texte: "Faitez suivre votre courrier ou faites-le relever par une personne de confiance : une boite \u00e0 lettres d\u00e9bordant de plis r\u00e9v\u00e8le une longue absence."
}, {
texte: "Votre domicile doit para\u00eetre habit\u00e9 ; demandez que l'on ouvre r\u00e9guli\u00e8rement les volets le matin."
}, {
texte: "Cr\u00e9ez l'illusion d'une pr\u00e9sence, \u00e0 l'aide d'un programmateur pour la lumi\u00e8re, la t\u00e9l\u00e9vision, la radio..."
}, {
texte: "Ne laissez pas de message sur votre r\u00e9pondeur t\u00e9l\u00e9phonique qui indiquerait la dur\u00e9e de votre absence. Transf\u00e9rez vos appels sur votre t\u00e9l\u00e9phone portable ou une autre ligne."
}, {
texte: 'Dans le cadre des op\u00e9rations "Tranquillit\u00e9 vacances" (OTV) organis\u00e9es durant les vacances scolaires, signalez votre absence au commissariat de police ou \u00e0 la brigade de gendarmerie ; des patrouilles pour surveiller votre domicile seront organis\u00e9es.'
} ]
};

// Alerte.js

enyo.kind({
name: "AlertePanel",
fit: !0,
components: [ {
kind: "FittableRows",
classes: "enyo-fit",
components: [ {
kind: "onyx.Toolbar",
classes: "alerte-toolbar",
components: [ {
content: "R\u00e9agir - Alerter"
} ]
}, {
kind: "FittableColumns",
classes: "alerte-navigation",
components: [ {
kind: "onyx.Icon",
name: "flecheprevious",
classes: "alerte-fleche",
src: "assets/gauche-1.png",
ontap: "prevPanel"
}, {
name: "titlealerte",
allowHtml: !0,
content: "",
fit: !0,
classes: "alerte-title"
}, {
kind: "onyx.Icon",
name: "flechenext",
classes: "proteger-fleche",
src: "assets/droite-1.png",
ontap: "nextPanel"
} ]
}, {
kind: "Panels",
name: "alertePanels",
fit: !0,
arrangerKind: "CardArranger",
classes: "alerte-panels",
onTransitionFinish: "titlePanel",
components: [ {
kind: "Scroller",
fit: !0,
components: [ {
kind: "Repeater",
name: "cambriolageList",
onSetupItem: "setCambriolage",
components: [ {
kind: "onyx.Groupbox",
classes: "alerte-groupbox",
components: [ {
name: "cambriolageItem",
content: ""
} ]
} ]
} ]
}, {
kind: "Scroller",
fit: !0,
components: [ {
kind: "Repeater",
name: "volList",
onSetupItem: "setVol",
components: [ {
kind: "onyx.Groupbox",
classes: "alerte-groupbox",
components: [ {
name: "volItem",
content: ""
} ]
} ]
} ]
} ]
}, {
kind: "onyx.Button",
classes: "alerte-button-send onyx-negative",
content: "Composer le 17",
ontap: "showPopup",
popup: "alertPopup"
} ]
}, {
kind: "onyx.Popup",
name: "alertPopup",
classes: "popup-light alert-popup",
centered: !0,
modal: !1,
floating: !0,
scrim: !0,
components: [ {
kind: "FittableRows",
components: [ {
name: "msgAlertPopup",
allowHtml: !0,
content: "",
style: "margin-bottom: 10px;"
}, {
content: "Vous allez appeler le 17.<br />Ceci est un num\u00e9ro d'urgence accessible gratuitement et sans couverture r\u00e9seau.<br />Attention, tout abus sera puni par la loi.<br /><br /><b>Souhaitez-vous poursuivre votre appel ?</b>",
style: "margin-bottom: 15px;",
allowHtml: !0
}, {
kind: "FittableRows",
components: [ {
kind: "onyx.Button",
classes: "alerte-button-rounded-send",
style: "margin-right: 10px;",
content: "Continuer",
disabled: !1,
ontap: "appel"
}, {
kind: "onyx.Button",
classes: "alerte-button-rounded-send",
content: "Annuler",
disabled: !1,
ontap: "closePopup"
} ]
} ]
} ]
} ],
create: function() {
this.inherited(arguments), this.messages = {
cambriolage: {
titre: "Si vous \u00eates victime<br />d'un cambriolage",
texte: [ "Ne touchez \u00e0 aucun objet, porte ou fen\u00eatre.", "Interdisez l'acc\u00e8s des lieux \u00e0 toute personne, sauf en cas de n\u00e9cessit\u00e9.", "Prot\u00e9gez les traces et les indices \u00e0 l'int\u00e9rieur comme \u00e0 l'ext\u00e9rieur.", "Pr\u00e9venez imm\u00e9diatement la brigade de gendarmerie du lieu de l'infraction. Si les cambrioleurs sont encore sur place, ne prenez pas de risques inconsid\u00e9r\u00e9s; privil\u00e9giez le recueil d'\u00e9l\u00e9ments d'identification (type de v\u00e9hicule, langage, stature, v\u00eatements...).", "D\u00e9posez plainte \u00e0 la brigade de votre choix (article 5 de la Charte d'accueil du public). Munissez-vous de votre pi\u00e8ce d'identit\u00e9.", "Faites opposition aupr\u00e8s de votre banque, pour vos ch\u00e9quiers et cartes de cr\u00e9dits d\u00e9rob\u00e9s.", "D\u00e9clarez le vol \u00e0 votre assureur.", "Le d\u00e9p\u00f4t de plainte apr\u00e8s un cambriolage est essentiel. Il permet aux cellules cambriolages implant\u00e9es dans chaque d\u00e9partement de faire des recoupements et ainsi d'appr\u00e9hender les malfaiteurs. Ces unit\u00e9s sont \u00e9paul\u00e9es par des policiers ou des gendarmes form\u00e9s en police technique et scientifique qui se d\u00e9placent sur chaque cambriolage pour relever les traces et indices." ]
},
vol: {
titre: "Si vous \u00eates victime<br />d'un vol \u00e0 main arm\u00e9e",
texte: [ "Observez votre agresseur pour en donner un signalement tr\u00e8s pr\u00e9cis.", "Notez son moyen et sa direction de fuite.", "M\u00e9morisez le type, le num\u00e9ro et la couleur du v\u00e9hicule.", "Maintenez les lieux en l'\u00e9tat jusqu'\u00e0 l'arriv\u00e9e de la gendarmerie nationale.", "Retenez ou identifiez les t\u00e9moins." ]
}
}, this.$.cambriolageList.setCount(this.messages.cambriolage.texte.length), this.$.volList.setCount(this.messages.vol.texte.length), this.updateArrows();
},
updateArrows: function() {
var e = this.$.alertePanels, t = e.getPanels(), n = this.$.flecheprevious, r = this.$.flechenext;
t.length <= 1 ? (n.applyStyle("background-position", "left bottom"), r.applyStyle("background-position", "left bottom")) : e.index == 0 ? (n.applyStyle("background-position", "left bottom"), r.applyStyle("background-position", "left top")) : (n.applyStyle("background-position", "left top"), e.index == t.length - 1 ? r.applyStyle("background-position", "left bottom") : r.applyStyle("background-position", "left top"));
},
setCambriolage: function(e, t) {
var n = t.index, r = t.item;
if (r) return r.$.cambriolageItem.setContent(this.messages.cambriolage.texte[n]), !0;
},
setVol: function(e, t) {
var n = t.index, r = t.item;
if (r) return r.$.volItem.setContent(this.messages.vol.texte[n]), !0;
},
titlePanel: function() {
this.updateArrows(), this.$.alertePanels.index <= 0 ? this.$.titlealerte.setContent(this.messages.cambriolage.titre) : this.$.alertePanels.index == 1 && this.$.titlealerte.setContent(this.messages.vol.titre);
},
prevPanel: function() {
this.$.alertePanels.previous(), this.titlePanel();
},
nextPanel: function() {
this.$.alertePanels.next(), this.titlePanel();
},
showPopup: function(e, t) {
this.$.msgAlertPopup.setContent("Vous allez \u00eatre mis en relation avec la gendarmerie.");
var n = this.$[e.popup];
n && n.show();
},
appel: function() {
var e = 17;
if (localStorage.getItem("idBrigade")) {
var t = enyo.$.app.$.brigade.getBrigade(localStorage.getItem("idBrigade"));
e = t.telephone;
}
window.location = "tel:" + e;
},
closePopup: function(e, t) {
this.$.alertPopup.hide();
}
});

// Compte.js

enyo.kind({
name: "ComptePanel",
fit: !0,
components: [ {
kind: "Scroller",
classes: "enyo-fit",
components: [ {
kind: "FittableRows",
classes: "compte-scroller",
components: [ {
kind: "onyx.Groupbox",
components: [ {
kind: "onyx.GroupboxHeader",
content: "Nom et Pr\u00e9nom"
}, {
kind: "onyx.InputDecorator",
components: [ {
kind: "onyx.Input",
name: "nameCompte",
placeholder: "Nom",
onchange: "setNameCompte"
} ]
}, {
kind: "onyx.InputDecorator",
components: [ {
kind: "onyx.Input",
name: "firstNameCompte",
placeholder: "Pr\u00e9nom",
onchange: "setFirstNameCompte"
} ]
} ]
}, {
kind: "onyx.Groupbox",
components: [ {
kind: "onyx.GroupboxHeader",
content: "Adresse"
}, {
kind: "onyx.InputDecorator",
components: [ {
kind: "onyx.Input",
name: "noStreetCompte",
placeholder: "Num\u00e9ro",
onchange: "setNoStreetCompte"
} ]
}, {
kind: "onyx.InputDecorator",
components: [ {
kind: "onyx.Input",
name: "nameStreetCompte",
placeholder: "Lib\u00e9ll\u00e9 de la voie",
onchange: "setNameStreetCompte"
} ]
}, {
kind: "onyx.InputDecorator",
components: [ {
kind: "onyx.Input",
name: "CPCompte",
placeholder: "Code postal",
onchange: "setCPCompte"
} ]
}, {
kind: "onyx.InputDecorator",
components: [ {
kind: "onyx.Input",
name: "cityCompte",
placeholder: "Ville",
onchange: "setCityCompte"
} ]
} ]
}, {
kind: "onyx.Groupbox",
components: [ {
kind: "onyx.GroupboxHeader",
content: "Informations personelles"
}, {
kind: "onyx.InputDecorator",
components: [ {
kind: "onyx.Input",
name: "phoneCompte",
placeholder: "T\u00e9l\u00e9phone",
onchange: "setPhoneCompte"
} ]
} ]
}, {
kind: "FittableColumns",
components: [ {
content: "Les donn\u00e9es seront enregistr\u00e9es localement sur votre t\u00e9l\u00e9phonne.",
classes: "compte-storage-text",
fit: !0
} ]
} ]
} ]
} ],
create: function() {
this.inherited(arguments), this.$.nameCompte.setValue(localStorage.getItem("nameCompte")), this.$.firstNameCompte.setValue(localStorage.getItem("firstNameCompte")), this.$.noStreetCompte.setValue(localStorage.getItem("noStreetCompte")), this.$.nameStreetCompte.setValue(localStorage.getItem("nameStreetCompte")), this.$.CPCompte.setValue(localStorage.getItem("CPCompte")), this.$.cityCompte.setValue(localStorage.getItem("cityCompte")), this.$.phoneCompte.setValue(localStorage.getItem("phoneCompte"));
},
setNameCompte: function(e, t) {
console.log(e.getValue()), localStorage.setItem("nameCompte", e.getValue());
},
setFirstNameCompte: function(e, t) {
console.log(e.getValue()), localStorage.setItem("firstNameCompte", e.getValue());
},
setNoStreetCompte: function(e, t) {
console.log(e.getValue()), localStorage.setItem("noStreetCompte", e.getValue());
},
setNameStreetCompte: function(e, t) {
console.log(e.getValue()), localStorage.setItem("nameStreetCompte", e.getValue());
},
setCPCompte: function(e, t) {
console.log(e.getValue()), localStorage.setItem("CPCompte", e.getValue());
},
setCityCompte: function(e, t) {
console.log(e.getValue()), localStorage.setItem("cityCompte", e.getValue());
},
setPhoneCompte: function(e, t) {
console.log(e.getValue()), localStorage.setItem("phoneCompte", e.getValue());
}
});

// OTV.js

enyo.kind({
name: "OTVPanel",
fit: !0,
components: [ {
kind: "FittableRows",
classes: "enyo-fit",
components: [ {
kind: "onyx.Toolbar",
classes: "otv-title",
components: [ {
content: "Op\u00e9ration Tranquillit\u00e9 Vacances"
} ]
}, {
kind: "Scroller",
fit: !0,
components: [ {
kind: "FittableRows",
classes: "otv-scroller",
components: [ {
kind: "gts.DividerDrawer",
caption: "Qu'est-ce que le plan OTV ?",
open: !1,
components: [ {
content: "La gendarmerie met en place un formulaire de demande individuelle vous permettant d'informer votre brigade de votre d\u00e9part.<br /><br />Cette demande renseign\u00e9e doit \u00eatre d\u00e9pos\u00e9e ou adress\u00e9e \u00e0 la brigade de gendarmerie de votre lieu de domicile.",
classes: "otv-texte",
allowHtml: !0
} ]
}, {
kind: "gts.DividerDrawer",
caption: "Comment s'inscrire ?",
open: !1,
components: [ {
content: "Pour b\u00e9n\u00e9ficier de ce service gratuit, inscrivez-vous avant votre d\u00e9part, \u00e0 votre brigade de proximit\u00e9 accompagn\u00e9 d'une pi\u00e8ce d'identit\u00e9 nationale et du formulaire de contact rempli, accessible depuis le site internet de la gendarmerie nationale.<br />Vous pouvez t\u00e9l\u00e9charger ci-dessous le formulaire et le transf\u00e9rer par mail sur votre messagerie \u00e9lectronique.<br /><br /><center><span style='font-size: 18px; font-weight: bold;'><a href='http://www.gendarmerie.interieur.gouv.fr/fre/content/download/585/5167/file/Demande_individuelle.pdf'>Formulaire de contact OTV</a></span></center>",
classes: "otv-texte",
allowHtml: !0
} ]
}, {
style: "text-align: center; padding: 10px;",
components: [ {
kind: "onyx.Button",
classes: "otv-button-map",
ontap: "maps",
popup: "otvPopup",
components: [ {
content: "Se d\u00e9placer \u00e0 la brigade<br />ou au commissariat",
allowHtml: !0
} ]
} ]
} ]
} ]
} ]
}, {
kind: "onyx.Popup",
name: "otvPopup",
classes: "popup-light alert-popup",
centered: !0,
modal: !1,
floating: !0,
scrim: !0,
components: [ {
kind: "FittableRows",
style: "margin: 5px;",
components: [ {
content: "Veuillez pr\u00e9alablement rechercher la<br />brigade de gendarmerie ou le<br />commissariat de police concern\u00e9",
style: "margin-bottom: 15px;",
allowHtml: !0
}, {
kind: "FittableRows",
components: [ {
kind: "onyx.Button",
classes: "otv-popup-buttons",
content: "Oui",
disabled: !1,
ontap: "brigades"
}, {
kind: "onyx.Button",
classes: "otv-popup-buttons",
content: "Non",
disabled: !1,
ontap: "closePopup"
} ]
} ]
} ]
}, {
kind: "Notification",
name: "notifOTV"
} ],
create: function() {
this.inherited(arguments);
},
maps: function(e, t) {
var n = localStorage.getItem("idBrigade");
if (!n) this.$.otvPopup.show(); else {
var r = enyo.$.app.$.brigade.getBrigade(n), i = "?q=" + r.adresse + ", " + r.nom + ", France", s = "http://maps.google.com/maps", o = "not detected";
/OS [2-4](_\d)(_\d)? like Mac OS X/i.test(navigator.userAgent) ? (s = "http://maps.google.com/maps", o = "ios 2-4") : /CPU like Mac OS X/i.test(navigator.userAgent) ? (s = "http://maps.google.com/maps", o = "ios 1") : /OS [5](_\d)(_\d)? like Mac OS X/i.test(navigator.userAgent) ? (s = "http://maps.google.com/maps", o = "ios 5") : (s = "http://maps.apple.com/", o = "ios 6 or greater"), s += i, window.location.href = s;
}
},
callc: function(e) {},
brigades: function() {
this.$.otvPopup.hide(), enyo.$.app.$.menuBrigade.setActive(!0), enyo.$.app.$.mainPanels.setIndex(1);
},
showPopup: function(e, t) {
var n = this.$[e.popup];
n && n.show();
},
closePopup: function(e, t) {
this.$.otvPopup.hide();
}
});

// PreventMsg.js

enyo.kind({
name: "PreventMsgPanel",
fit: !0,
components: [ {
kind: "FittableRows",
classes: "enyo-fit",
components: [ {
kind: "onyx.Toolbar",
classes: "prevent-title",
components: [ {
name: "title",
content: "Informations"
} ]
}, {
kind: "Scroller",
name: "prevent-scroller",
fit: !0,
components: [ {
name: "intro",
classes: "prevent-message",
content: "Pensez-y ",
allowHtml: !0
}, {
kind: "onyx.Groupbox",
classes: "prevent-groupbox",
components: [ {
kind: "FittableRows",
classes: "prevent-message",
components: [ {
name: "message",
content: "",
allowHtml: !0
} ]
} ]
} ]
} ]
} ],
create: function() {
this.inherited(arguments), this.randomText();
},
randomText: function() {
var e = messages.prevention, t = Math.floor(Math.random() * 24 + 1);
t <= 6 ? this.$.intro.setContent("Pensez-y ! Pour prot\u00e9gez votre domicile...\n\n") : t <= 18 ? this.$.intro.setContent("Pensez-y ! Les bons gestes...\n\n") : this.$.intro.setContent("Pensez-y ! En cas d'absence durable...\n\n"), this.$.message.setContent(e[t].texte);
}
});

// brigades.js

var bddBrigades = {
brigades: [ {
id: 1,
nom: "Capestang",
compagnie: 1,
type: 1,
cob: null,
adresse: "33 avenue de la R\u00e9publique",
telephone: "04 67 93 30 31",
fax: ""
}, {
id: 2,
nom: "Olonzac",
compagnie: 1,
type: 1,
cob: null,
adresse: "Avenue d'Homps",
telephone: "04 68 91 20 17",
fax: ""
}, {
id: 4,
nom: "Bedarieux",
compagnie: 1,
type: 3,
cob: 1,
adresse: "Caserne Canitrot route de Saint-Pons",
telephone: "04 67 95 09 29",
fax: "04 67 95 86 74"
}, {
id: 5,
nom: "St Gervais sur Mare",
compagnie: 1,
type: 3,
cob: 1,
adresse: "16 avenue des Treilles",
telephone: "04 67 23 60 25",
fax: ""
}, {
id: 6,
nom: "Le Bousquet d'Orb",
compagnie: 1,
type: 3,
cob: 1,
adresse: "39 all\u00e9e Jean Bringer",
telephone: "04 67 23 80 28",
fax: "04 67 23 96 53"
}, {
id: 7,
nom: "Murviel les B\u00e9ziers",
compagnie: 1,
type: 3,
cob: 2,
adresse: "23 avenue de la R\u00e9publique",
telephone: "04 67 37 83 99",
fax: ""
}, {
id: 8,
nom: "St Chinian",
compagnie: 1,
type: 3,
cob: 2,
adresse: "7 rue de l'Ev\u00eaque",
telephone: "04 67 38 00 13",
fax: "04 67 38 00 90"
}, {
id: 9,
nom: "St Pons de Thomi\u00e8res",
compagnie: 1,
type: 3,
cob: 3,
adresse: "18 avenue de la Gare",
telephone: "04 67 97 01 12",
fax: "04 67 97 91 28"
}, {
id: 10,
nom: "La Salvetat sur Agout",
compagnie: 1,
type: 3,
cob: 3,
adresse: "All\u00e9e Saint-Etienne-de-Cavall",
telephone: "04 67 97 60 42",
fax: "04 67 97 64 78"
}, {
id: 11,
nom: "Olargues",
compagnie: 1,
type: 3,
cob: 3,
adresse: "Route de Saint-Pons",
telephone: "04 67 97 70 01",
fax: "04 67 97 69 57"
}, {
id: 12,
nom: "Valras Plage",
compagnie: 1,
type: 3,
cob: 4,
adresse: "Chemin des Cosses-sous-la-Tour",
telephone: "04 67 32 02 53",
fax: ""
}, {
id: 13,
nom: "B\u00e9ziers",
compagnie: 1,
type: 3,
cob: 4,
adresse: "14 boulevard du Mar\u00e9chal Leclerc",
telephone: "04 67 35 17 17",
fax: ""
}, {
id: 14,
nom: "Castelnau le Lez",
compagnie: 2,
type: 1,
cob: null,
adresse: "635 avenue de la Monnaie",
telephone: "04 99 74 29 50",
fax: "04 99 74 29 64"
}, {
id: 15,
nom: "Jacou/ Clapiers",
compagnie: 2,
type: 1,
cob: null,
adresse: "10 avenue de l'Europe",
telephone: "04 99 63 68 50",
fax: "04 99 63 68 51"
}, {
id: 16,
nom: "Montpellier",
compagnie: 2,
type: 1,
cob: null,
adresse: "Caserne Lepic 359 rue de Font-Couverte",
telephone: "04 99 53 59 34",
fax: "04 99 53 59 35"
}, {
id: 17,
nom: "Palavas les Flots",
compagnie: 2,
type: 1,
cob: null,
adresse: "1 rue de la Tramontane",
telephone: "04 67 07 01 20",
fax: "04 67 07 01 29"
}, {
id: 18,
nom: "St Gely du Fesc",
compagnie: 2,
type: 1,
cob: null,
adresse: "458 rue du Devois",
telephone: "04 67 91 73 00",
fax: "04 67 91 73 09"
}, {
id: 19,
nom: "St Georges d'Orques",
compagnie: 2,
type: 1,
cob: null,
adresse: "5 avenue de l'Occitanie",
telephone: "04 67 75 18 99",
fax: "04 67 75 84 17"
}, {
id: 20,
nom: "Saint Jean de Vedas",
compagnie: 2,
type: 1,
cob: null,
adresse: "330 avenue de Librilla",
telephone: "04 67 99 45 70",
fax: "04 67 99 45 79"
}, {
id: 21,
nom: "Saint Mathieu de Treviers",
compagnie: 2,
type: 1,
cob: null,
adresse: "Avenue de Montpellier",
telephone: "04 67 55 20 02",
fax: "04 67 55 17 35"
}, {
id: 22,
nom: "Villeneuve les Maguelone",
compagnie: 2,
type: 1,
cob: null,
adresse: "364 avenue de la Gare",
telephone: "04 67 69 52 69",
fax: ""
}, {
id: 23,
nom: "Ganges",
compagnie: 3,
type: 1,
cob: null,
adresse: "1 place Joseph Boudouresques",
telephone: "04 67 73 85 13",
fax: "04 67 73 75 98"
}, {
id: 24,
nom: "Saint Martin de Londres",
compagnie: 3,
type: 1,
cob: null,
adresse: "240 avenue des C\u00e9vennes",
telephone: "04 67 55 00 01",
fax: "04 67 55 05 54"
}, {
id: 25,
nom: "Clermont l'H\u00e9rault",
compagnie: 3,
type: 3,
cob: 5,
adresse: "8 rue Jean Moulin",
telephone: "04 67 96 00 32",
fax: "04 67 96 99 53"
}, {
id: 26,
nom: "Paulhan",
compagnie: 3,
type: 3,
cob: 5,
adresse: "78 cours National",
telephone: "04 67 25 00 09",
fax: "04 67 25 21 77"
}, {
id: 27,
nom: "Aniane",
compagnie: 3,
type: 3,
cob: 6,
adresse: "31 rue Jean Cast\u00e9ran",
telephone: "04 67 57 70 20",
fax: "04 67 57 02 72"
}, {
id: 28,
nom: "Gignac",
compagnie: 3,
type: 3,
cob: 6,
adresse: "576 chemin Sainte-Claire Roqueyrol",
telephone: "04 67 57 50 05",
fax: "04 67 57 04 48"
}, {
id: 29,
nom: "Le Caylar",
compagnie: 3,
type: 3,
cob: 7,
adresse: "163 avenue de Lod\u00e8ve",
telephone: "04 67 44 50 01",
fax: ""
}, {
id: 30,
nom: "Lod\u00e8ve",
compagnie: 3,
type: 3,
cob: 7,
adresse: "160 boulevard du G\u00e9n\u00e9ral Leclerc",
telephone: "04 67 44 00 25",
fax: "04 67 44 91 14"
}, {
id: 31,
nom: "Castries",
compagnie: 4,
type: 1,
cob: null,
adresse: "18 avenue du Moulin-\u00e0-Vent",
telephone: "04 67 70 03 31",
fax: "04 67 70 88 60"
}, {
id: 32,
nom: "La Grande Motte",
compagnie: 4,
type: 1,
cob: null,
adresse: "Avenue Melgueil",
telephone: "04 67 56 50 29",
fax: ""
}, {
id: 33,
nom: "Lunel",
compagnie: 4,
type: 1,
cob: null,
adresse: "Caserne Vauban 171 avenue du G\u00e9n\u00e9ral de Gaulle",
telephone: "04 67 83 06 23",
fax: "04 67 83 47 84"
}, {
id: 34,
nom: "Mauguio",
compagnie: 4,
type: 1,
cob: null,
adresse: "Rue Jean-S\u00e9bastien Bach",
telephone: "04 67 29 30 04",
fax: "04 67 29 85 72"
}, {
id: 35,
nom: "M\u00e8ze",
compagnie: 5,
type: 1,
cob: null,
adresse: "1 chemin de Cagueloup",
telephone: "04 67 43 80 11",
fax: "04 67 43 82 85"
}, {
id: 36,
nom: "Agde",
compagnie: 5,
type: 3,
cob: 8,
adresse: "80 chemin de Janin",
telephone: "04 67 21 10 29",
fax: "04 67 21 34 56"
}, {
id: 37,
nom: "Florensac",
compagnie: 5,
type: 3,
cob: 8,
adresse: "Rue des Coquelicots",
telephone: "04 67 77 90 48",
fax: "04 67 77 90 16"
}, {
id: 38,
nom: "Gigean",
compagnie: 5,
type: 3,
cob: 9,
adresse: "17 avenue de Montpellier",
telephone: "04 67 78 72 66",
fax: ""
}, {
id: 39,
nom: "S\u00e8te",
compagnie: 5,
type: 3,
cob: 9,
adresse: "17 avenue de Montpellier",
telephone: "04 67 78 72 66",
fax: "04 67 78 90 58"
}, {
id: 40,
nom: "Montagnac",
compagnie: 5,
type: 3,
cob: 10,
adresse: "20 rue Aspirant Lebaron",
telephone: "04 67 24 06 10",
fax: "04 67 24 81 85"
}, {
id: 41,
nom: "P\u00e9zenas",
compagnie: 5,
type: 3,
cob: 10,
adresse: "12 avenue de Plaisance",
telephone: "04 67 98 13 65",
fax: "04 67 98 98 56"
}, {
id: 42,
nom: "Roujan",
compagnie: 5,
type: 3,
cob: 11,
adresse: "Avenue de Caux",
telephone: "04 67 24 60 13",
fax: ""
}, {
id: 43,
nom: "Servian",
compagnie: 5,
type: 3,
cob: 11,
adresse: "Route de La Roque",
telephone: "04 67 39 10 20",
fax: ""
}, {
id: 44,
nom: "Police",
compagnie: null,
type: 4,
cob: null,
adresse: "",
telephone: "",
fax: ""
}, {
id: 45,
nom: "B\u00e9ziers",
compagnie: null,
type: 4,
cob: null,
adresse: "Ciat de BEZIERS, Boulevard Edouard Herriot",
telephone: "04 67 49 54 00",
fax: ""
}, {
id: 46,
nom: "Agde",
compagnie: null,
type: 4,
cob: null,
adresse: "Ciat d'Agde, Avenue du G\u00e9n\u00e9ral de Gaulle",
telephone: "04 67 01 02 00",
fax: ""
}, {
id: 47,
nom: "Cap d'Agde",
compagnie: null,
type: 4,
cob: null,
adresse: "Ciat Cap d'Agde, Avenue des sergents",
telephone: "04 67 00 14 21",
fax: ""
}, {
id: 48,
nom: "S\u00e8te",
compagnie: null,
type: 4,
cob: null,
adresse: "Ciat de S\u00e8te, 50 quai bosc",
telephone: "04 67 46 80 22",
fax: ""
}, {
id: 49,
nom: "Frontignan",
compagnie: null,
type: 4,
cob: null,
adresse: "Poste de Frontignan, 1 Avenue Fr\u00e9deric Mistral (saison estivale)",
telephone: "04 67 18 49 30",
fax: ""
}, {
id: 50,
nom: "Lattes",
compagnie: null,
type: 4,
cob: null,
adresse: "Ciat de Lattes, 1 Avenue de l'Agau",
telephone: "04 99 13 67 00",
fax: ""
}, {
id: 51,
nom: "Montpellier",
compagnie: null,
type: 4,
cob: null,
adresse: "206 avenue du Comte de Melgueil",
telephone: "04 99 13 50 00",
fax: ""
} ],
groupe: [ {
id: 1,
nom: "B\u00e9darieux",
compagnie: 1
}, {
id: 2,
nom: "Murviel Les B\u00e9ziers",
compagnie: 1
}, {
id: 3,
nom: "Saint Pons de Thomi\u00e8res",
compagnie: 1
}, {
id: 4,
nom: "Valras",
compagnie: 1
}, {
id: 5,
nom: "Clermont l'H\u00e9rault",
compagnie: 3
}, {
id: 6,
nom: "Gignac",
compagnie: 3
}, {
id: 7,
nom: "Lod\u00e8ve",
compagnie: 3
}, {
id: 8,
nom: "Agde",
compagnie: 5
}, {
id: 9,
nom: "Gigean",
compagnie: 5
}, {
id: 10,
nom: "P\u00e9zenas",
compagnie: 5
}, {
id: 11,
nom: "Servian",
compagnie: 5
} ],
type: [ {
id: 1,
nom: "BTA",
detail: "Brigade Territoriale Autonome"
}, {
id: 3,
nom: "BTP",
detail: "Brigade Territoriale de Proximit\u00e9"
}, {
id: 4,
nom: "ZPN",
detail: "Zone Police Nationale"
} ],
compagnies: [ {
id: 1,
nom: "Beziers"
}, {
id: 2,
nom: "Castelnau Le Lez"
}, {
id: 3,
nom: "Lod\u00e8ve"
}, {
id: 4,
nom: "Lunel"
}, {
id: 5,
nom: "P\u00e9zenas"
} ],
villes: [ {
id: 1,
nom: "Capestang",
brigade: 1,
CP: 34310
}, {
id: 2,
nom: "Colombiers",
brigade: 1,
CP: 34410
}, {
id: 3,
nom: "Creissan",
brigade: 1,
CP: 34370
}, {
id: 4,
nom: "Maureilhan",
brigade: 1,
CP: 34370
}, {
id: 5,
nom: "Montady",
brigade: 1,
CP: 34310
}, {
id: 6,
nom: "Montels",
brigade: 1,
CP: 34310
}, {
id: 7,
nom: "Nissan lez Enserune",
brigade: 1,
CP: 34440
}, {
id: 8,
nom: "Poilhes",
brigade: 1,
CP: 34310
}, {
id: 9,
nom: "Puisserguiers",
brigade: 1,
CP: 34620
}, {
id: 10,
nom: "Quarante",
brigade: 1,
CP: 34310
}, {
id: 11,
nom: "Aigne",
brigade: 2,
CP: 34210
}, {
id: 12,
nom: "Azillanet",
brigade: 2,
CP: 34210
}, {
id: 13,
nom: "Beaufort",
brigade: 2,
CP: 34210
}, {
id: 14,
nom: "Cassagnoles",
brigade: 2,
CP: 34210
}, {
id: 15,
nom: "Cesseras",
brigade: 2,
CP: 34210
}, {
id: 16,
nom: "Felines Minervois",
brigade: 2,
CP: 34210
}, {
id: 17,
nom: "Ferrals les Montagnes",
brigade: 2,
CP: 34210
}, {
id: 18,
nom: "La Caunette",
brigade: 2,
CP: 34210
}, {
id: 19,
nom: "La Livini\u00e8re",
brigade: 2,
CP: 34210
}, {
id: 20,
nom: "Minerve",
brigade: 2,
CP: 34210
}, {
id: 21,
nom: "Olonzac",
brigade: 2,
CP: 34210
}, {
id: 22,
nom: "Oupia",
brigade: 2,
CP: 34210
}, {
id: 23,
nom: "Siran",
brigade: 2,
CP: 34210
}, {
id: 24,
nom: "Bedarieux",
brigade: 4,
CP: 34600
}, {
id: 25,
nom: "Camplong",
brigade: 4,
CP: 34260
}, {
id: 26,
nom: "Carlencas et Levas",
brigade: 4,
CP: 34600
}, {
id: 27,
nom: "Faugeres",
brigade: 4,
CP: 34600
}, {
id: 28,
nom: "Graissessac",
brigade: 4,
CP: 34260
}, {
id: 29,
nom: "Herepian",
brigade: 4,
CP: 34600
}, {
id: 30,
nom: "La Tour sur Orb",
brigade: 4,
CP: 34260
}, {
id: 31,
nom: "Le Pradal",
brigade: 4,
CP: 34600
}, {
id: 32,
nom: "Pezennes les Mines",
brigade: 4,
CP: 34600
}, {
id: 33,
nom: "Saint Etienne Estrechoux",
brigade: 4,
CP: 34260
}, {
id: 34,
nom: "Villemagne l'Argenti\u00e8re",
brigade: 4,
CP: 34600
}, {
id: 35,
nom: "Castanet le Haut",
brigade: 5,
CP: 34610
}, {
id: 36,
nom: "Combes",
brigade: 5,
CP: 34240
}, {
id: 37,
nom: "Lamalou les Bains",
brigade: 5,
CP: 34240
}, {
id: 38,
nom: "Le Poujol sur Orb",
brigade: 5,
CP: 34600
}, {
id: 39,
nom: "Les Aires",
brigade: 5,
CP: 34600
}, {
id: 40,
nom: "Rosis",
brigade: 5,
CP: 34610
}, {
id: 41,
nom: "Saint Genies de Varensal",
brigade: 5,
CP: 34610
}, {
id: 42,
nom: "Saint Gervais sur Mare",
brigade: 5,
CP: 34610
}, {
id: 43,
nom: "Taussac la Billi\u00e8re",
brigade: 5,
CP: 34600
}, {
id: 44,
nom: "Avene",
brigade: 6,
CP: 34260
}, {
id: 45,
nom: "Brenas",
brigade: 6,
CP: 34650
}, {
id: 46,
nom: "Ceihes et Rocozels",
brigade: 6,
CP: 34260
}, {
id: 47,
nom: "Dio et Valquieres",
brigade: 6,
CP: 34650
}, {
id: 48,
nom: "Joncels",
brigade: 6,
CP: 34650
}, {
id: 49,
nom: "Le Bousquet d'Orb",
brigade: 6,
CP: 34260
}, {
id: 50,
nom: "Lunas",
brigade: 6,
CP: 34650
}, {
id: 51,
nom: "Autignac",
brigade: 7,
CP: 34480
}, {
id: 52,
nom: "Cabrerolles",
brigade: 7,
CP: 34480
}, {
id: 53,
nom: "Causses et Veyran",
brigade: 7,
CP: 34490
}, {
id: 54,
nom: "Caussiniojouls",
brigade: 7,
CP: 34600
}, {
id: 55,
nom: "Cazouls les B\u00e9ziers",
brigade: 7,
CP: 34370
}, {
id: 56,
nom: "Corneilhan",
brigade: 7,
CP: 34490
}, {
id: 57,
nom: "Laurens",
brigade: 7,
CP: 34480
}, {
id: 58,
nom: "Lignan sur Orb",
brigade: 7,
CP: 34490
}, {
id: 59,
nom: "Maraussan",
brigade: 7,
CP: 34370
}, {
id: 60,
nom: "Murviel les B\u00e9ziers",
brigade: 7,
CP: 34490
}, {
id: 61,
nom: "Pailhes",
brigade: 7,
CP: 34490
}, {
id: 62,
nom: "Puimisson",
brigade: 7,
CP: 34480
}, {
id: 63,
nom: "Saint Genies de Fontedit",
brigade: 7,
CP: 34480
}, {
id: 64,
nom: "Saint Nazaire de Ladarez",
brigade: 7,
CP: 34490
}, {
id: 65,
nom: "Thezan les B\u00e9ziers",
brigade: 7,
CP: 34490
}, {
id: 66,
nom: "Agel",
brigade: 8,
CP: 34210
}, {
id: 67,
nom: "Aigues vives",
brigade: 8,
CP: 34210
}, {
id: 68,
nom: "Assignan",
brigade: 8,
CP: 34360
}, {
id: 69,
nom: "Babeau Bouldoux",
brigade: 8,
CP: 34360
}, {
id: 70,
nom: "Cazedarnes",
brigade: 8,
CP: 34460
}, {
id: 71,
nom: "Cruzy",
brigade: 8,
CP: 34310
}, {
id: 72,
nom: "Ferrieres Poussarou",
brigade: 8,
CP: 34360
}, {
id: 73,
nom: "Montouliers",
brigade: 8,
CP: 34310
}, {
id: 74,
nom: "Pierrerue",
brigade: 8,
CP: 34360
}, {
id: 75,
nom: "Prades sur Vernazobre",
brigade: 8,
CP: 34360
}, {
id: 76,
nom: "Saint Chinian",
brigade: 8,
CP: 34360
}, {
id: 77,
nom: "Villespassans",
brigade: 8,
CP: 34360
}, {
id: 78,
nom: "Boisset",
brigade: 9,
CP: 34220
}, {
id: 79,
nom: "Courniou",
brigade: 9,
CP: 34220
}, {
id: 80,
nom: "Pardailhan",
brigade: 9,
CP: 34360
}, {
id: 81,
nom: "Rieussec",
brigade: 9,
CP: 34220
}, {
id: 82,
nom: "Riols",
brigade: 9,
CP: 34220
}, {
id: 83,
nom: "Saint Jean de Minervois",
brigade: 9,
CP: 34360
}, {
id: 84,
nom: "Saint Pons de Thomi\u00e8res",
brigade: 9,
CP: 34220
}, {
id: 85,
nom: "Velieux",
brigade: 9,
CP: 34220
}, {
id: 86,
nom: "Verreries de Moussans",
brigade: 9,
CP: 34220
}, {
id: 87,
nom: "Cambon et Salvergues",
brigade: 10,
CP: 34330
}, {
id: 88,
nom: "Fraisse sur Agout",
brigade: 10,
CP: 34330
}, {
id: 89,
nom: "La Salvetat sur Agout",
brigade: 10,
CP: 34330
}, {
id: 90,
nom: "Le Soulie",
brigade: 10,
CP: 34330
}, {
id: 91,
nom: "Berlou",
brigade: 11,
CP: 34360
}, {
id: 92,
nom: "Colombi\u00e8res sur Orb",
brigade: 11,
CP: 34390
}, {
id: 93,
nom: "Mons",
brigade: 11,
CP: 34390
}, {
id: 94,
nom: "Olargues",
brigade: 11,
CP: 34390
}, {
id: 95,
nom: "Premian\nPremian",
brigade: 11,
CP: 34390
}, {
id: 96,
nom: "Roquebrun",
brigade: 11,
CP: 34460
}, {
id: 97,
nom: "Saint Etienne d'Albagnan",
brigade: 11,
CP: 34390
}, {
id: 98,
nom: "Saint Julien",
brigade: 11,
CP: 34390
}, {
id: 99,
nom: "Saint Martin de l'Arcon",
brigade: 11,
CP: 34390
}, {
id: 100,
nom: "Saint Vincent d'Olargues",
brigade: 11,
CP: 34390
}, {
id: 101,
nom: "Vieussan",
brigade: 11,
CP: 34390
}, {
id: 102,
nom: "Lespignan",
brigade: 12,
CP: 34710
}, {
id: 103,
nom: "Portiragnes",
brigade: 12,
CP: 34420
}, {
id: 104,
nom: "Sauvian",
brigade: 12,
CP: 34410
}, {
id: 105,
nom: "Serignan",
brigade: 12,
CP: 34410
}, {
id: 106,
nom: "Valras-Plage",
brigade: 12,
CP: 34350
}, {
id: 107,
nom: "Vendres",
brigade: 12,
CP: 34350
}, {
id: 108,
nom: "Castelnau le Lez",
brigade: 14,
CP: 34170
}, {
id: 109,
nom: "Le Cres",
brigade: 14,
CP: 34920
}, {
id: 110,
nom: "Assas",
brigade: 15,
CP: 34820
}, {
id: 111,
nom: "Clapiers",
brigade: 15,
CP: 34830
}, {
id: 112,
nom: "Guzargues",
brigade: 15,
CP: 34820
}, {
id: 113,
nom: "Jacou",
brigade: 15,
CP: 34830
}, {
id: 114,
nom: "Montferrier sur Lez",
brigade: 15,
CP: 34980
}, {
id: 115,
nom: "Prades le Lez",
brigade: 15,
CP: 34730
}, {
id: 116,
nom: "Saint Vincent de Barbeyargues",
brigade: 15,
CP: 34730
}, {
id: 117,
nom: "Teyran",
brigade: 15,
CP: 34820
}, {
id: 118,
nom: "Palavas les Flots",
brigade: 17,
CP: 34250
}, {
id: 119,
nom: "Carnon (Mauguio)",
brigade: 17,
CP: 34130
}, {
id: 120,
nom: "Combaillaux",
brigade: 18,
CP: 34980
}, {
id: 121,
nom: "Grabels",
brigade: 18,
CP: 34790
}, {
id: 122,
nom: "Les Matelles",
brigade: 18,
CP: 34270
}, {
id: 123,
nom: "Murles",
brigade: 18,
CP: 34980
}, {
id: 124,
nom: "Saint Cl\u00e9ment de Rivi\u00e8re",
brigade: 18,
CP: 34980
}, {
id: 125,
nom: "Saint Gely de Fesc",
brigade: 18,
CP: 34480
}, {
id: 126,
nom: "Vailhauques",
brigade: 18,
CP: 34750
}, {
id: 127,
nom: "Juvignac",
brigade: 19,
CP: 34990
}, {
id: 128,
nom: "Laverune",
brigade: 19,
CP: 34880
}, {
id: 129,
nom: "Montarnaud",
brigade: 19,
CP: 34570
}, {
id: 130,
nom: "Murviel les Montpellier",
brigade: 19,
CP: 34570
}, {
id: 131,
nom: "Pignan",
brigade: 19,
CP: 34570
}, {
id: 132,
nom: "Saint Georges d'Orques",
brigade: 19,
CP: 34680
}, {
id: 133,
nom: "Saint Paul et Valmalle",
brigade: 19,
CP: 34570
}, {
id: 134,
nom: "Fabregues",
brigade: 20,
CP: 34690
}, {
id: 135,
nom: "Saussan",
brigade: 20,
CP: 34570
}, {
id: 136,
nom: "Saint Jean de Vedas",
brigade: 20,
CP: 34430
}, {
id: 137,
nom: "Buzignargues",
brigade: 21,
CP: 34160
}, {
id: 138,
nom: "Campagne",
brigade: 21,
CP: 34160
}, {
id: 139,
nom: "Cazevieille",
brigade: 21,
CP: 34270
}, {
id: 140,
nom: "Claret",
brigade: 21,
CP: 34270
}, {
id: 141,
nom: "Fontanes",
brigade: 21,
CP: 34270
}, {
id: 142,
nom: "Galargues",
brigade: 21,
CP: 34160
}, {
id: 143,
nom: "Garrigues",
brigade: 21,
CP: 34160
}, {
id: 144,
nom: "Lauret",
brigade: 21,
CP: 34270
}, {
id: 145,
nom: "Le Triadou",
brigade: 21,
CP: 34270
}, {
id: 146,
nom: "Montaud",
brigade: 21,
CP: 34160
}, {
id: 147,
nom: "Sauteyrargues",
brigade: 21,
CP: 34270
}, {
id: 148,
nom: "Saint Bauzille de Montmel",
brigade: 21,
CP: 34160
}, {
id: 149,
nom: "Saint Hilaire de Beauvoir",
brigade: 21,
CP: 34160
}, {
id: 150,
nom: "Saint Jean de Cornies",
brigade: 21,
CP: 34160
}, {
id: 151,
nom: "Saint Jean de Cuculles",
brigade: 21,
CP: 34270
}, {
id: 152,
nom: "Saint Mathieu de Treviers",
brigade: 21,
CP: 34270
}, {
id: 153,
nom: "Saint Croix de Quintillargues",
brigade: 21,
CP: 34270
}, {
id: 154,
nom: "Vacquieres",
brigade: 21,
CP: 34270
}, {
id: 155,
nom: "Valflaunes",
brigade: 21,
CP: 34270
}, {
id: 156,
nom: "Mireval",
brigade: 22,
CP: 34110
}, {
id: 157,
nom: "Vic la Gardiole",
brigade: 22,
CP: 34110
}, {
id: 158,
nom: "Villeneuve les Maguelone",
brigade: 22,
CP: 34750
}, {
id: 159,
nom: "Agones",
brigade: 23,
CP: 34190
}, {
id: 160,
nom: "Brissac",
brigade: 23,
CP: 34190
}, {
id: 161,
nom: "Cazilhac",
brigade: 23,
CP: 34190
}, {
id: 162,
nom: "Ganges",
brigade: 23,
CP: 34190
}, {
id: 163,
nom: "Gornies",
brigade: 23,
CP: 34190
}, {
id: 164,
nom: "Laroque",
brigade: 23,
CP: 34190
}, {
id: 165,
nom: "Montoulieu",
brigade: 23,
CP: 34190
}, {
id: 166,
nom: "Moules et Baucels",
brigade: 23,
CP: 34190
}, {
id: 167,
nom: "Saint Bauzille de Putois",
brigade: 23,
CP: 34190
}, {
id: 168,
nom: "Causse de la Selle",
brigade: 24,
CP: 34380
}, {
id: 169,
nom: "Ferrieres les Verreries",
brigade: 24,
CP: 34190
}, {
id: 170,
nom: "Mas de Londres",
brigade: 24,
CP: 34380
}, {
id: 171,
nom: "Notre Dame de Londres",
brigade: 24,
CP: 34380
}, {
id: 172,
nom: "Rouet",
brigade: 24,
CP: 34380
}, {
id: 173,
nom: "Saint Andr\u00e9 de Bueges",
brigade: 24,
CP: 34390
}, {
id: 174,
nom: "Saint Jean de Bu\u00e8ges",
brigade: 24,
CP: 34380
}, {
id: 175,
nom: "Saint Martin de Londres",
brigade: 24,
CP: 34380
}, {
id: 176,
nom: "Viols en Laval",
brigade: 24,
CP: 34380
}, {
id: 177,
nom: "Viols le Fort",
brigade: 24,
CP: 34380
}, {
id: 178,
nom: "Aspiran",
brigade: 25,
CP: 34800
}, {
id: 179,
nom: "Brignac",
brigade: 25,
CP: 34800
}, {
id: 180,
nom: "Cabri\u00e8res",
brigade: 25,
CP: 34800
}, {
id: 181,
nom: "Canet",
brigade: 25,
CP: 34800
}, {
id: 182,
nom: "Ceyras",
brigade: 25,
CP: 34800
}, {
id: 183,
nom: "Clermont l'H\u00e9rault",
brigade: 25,
CP: 34800
}, {
id: 184,
nom: "Lacoste",
brigade: 25,
CP: 34800
}, {
id: 185,
nom: "Liausson",
brigade: 25,
CP: 34800
}, {
id: 186,
nom: "Lieuran Cabri\u00e8res",
brigade: 25,
CP: 34800
}, {
id: 187,
nom: "Merifons",
brigade: 25,
CP: 34800
}, {
id: 188,
nom: "Moureze",
brigade: 25,
CP: 34800
}, {
id: 189,
nom: "Nebian",
brigade: 25,
CP: 34800
}, {
id: 190,
nom: "Peret",
brigade: 25,
CP: 34800
}, {
id: 191,
nom: "Salasc",
brigade: 25,
CP: 34800
}, {
id: 192,
nom: "Saint Felix de Lodez",
brigade: 25,
CP: 34725
}, {
id: 193,
nom: "Valmascle",
brigade: 25,
CP: 34800
}, {
id: 194,
nom: "Villeneuvette",
brigade: 25,
CP: 34800
}, {
id: 195,
nom: "Belarga",
brigade: 26,
CP: 34230
}, {
id: 196,
nom: "Campagnan",
brigade: 26,
CP: 34230
}, {
id: 197,
nom: "Le Pouget",
brigade: 26,
CP: 34230
}, {
id: 198,
nom: "Paulhan",
brigade: 26,
CP: 34230
}, {
id: 199,
nom: "Plaissan",
brigade: 26,
CP: 34230
}, {
id: 200,
nom: "Puilacher",
brigade: 26,
CP: 34230
}, {
id: 201,
nom: "Saint Pargoire",
brigade: 26,
CP: 34230
}, {
id: 202,
nom: "Tressan",
brigade: 26,
CP: 34230
}, {
id: 203,
nom: "Aniane",
brigade: 27,
CP: 34150
}, {
id: 204,
nom: "Argelliers",
brigade: 27,
CP: 34380
}, {
id: 205,
nom: "La Boissi\u00e8re",
brigade: 27,
CP: 34150
}, {
id: 206,
nom: "Puechabon",
brigade: 27,
CP: 34150
}, {
id: 207,
nom: "Saint Guilhem de D\u00e9sert",
brigade: 27,
CP: 34150
}, {
id: 208,
nom: "Arboras",
brigade: 28,
CP: 34150
}, {
id: 209,
nom: "Aumelas",
brigade: 28,
CP: 34230
}, {
id: 210,
nom: "Gignac",
brigade: 28,
CP: 34150
}, {
id: 211,
nom: "Jonqui\u00e8res",
brigade: 28,
CP: 34725
}, {
id: 212,
nom: "Lagamas",
brigade: 28,
CP: 34150
}, {
id: 213,
nom: "Montpeyroux",
brigade: 28,
CP: 34150
}, {
id: 214,
nom: "Popian",
brigade: 28,
CP: 34230
}, {
id: 215,
nom: "Pouzols",
brigade: 28,
CP: 34230
}, {
id: 216,
nom: "Saint Andr\u00e9 de Sagonis",
brigade: 28,
CP: 34725
}, {
id: 217,
nom: "Saint Bauzille de la Sylve",
brigade: 28,
CP: 34230
}, {
id: 218,
nom: "Saint Guiraud",
brigade: 28,
CP: 34725
}, {
id: 219,
nom: "Saint Jean de Fos",
brigade: 28,
CP: 34150
}, {
id: 220,
nom: "Saint Saturnin de Lucian",
brigade: 28,
CP: 34725
}, {
id: 221,
nom: "Vendemian",
brigade: 28,
CP: 34230
}, {
id: 222,
nom: "Le Caylar",
brigade: 29,
CP: 34520
}, {
id: 223,
nom: "Le Cros",
brigade: 29,
CP: 34520
}, {
id: 224,
nom: "Les Rives",
brigade: 29,
CP: 34520
}, {
id: 225,
nom: "Pegairolles de l'Escalette",
brigade: 29,
CP: 34700
}, {
id: 226,
nom: "Romigui\u00e8res",
brigade: 29,
CP: 34650
}, {
id: 227,
nom: "Roqueredonde",
brigade: 29,
CP: 34650
}, {
id: 228,
nom: "Sorbs",
brigade: 29,
CP: 34520
}, {
id: 229,
nom: "Saint Felix de l'Heras",
brigade: 29,
CP: 34520
}, {
id: 230,
nom: "Saint Maurice Navacelles",
brigade: 29,
CP: 34520
}, {
id: 231,
nom: "Saint Michel",
brigade: 29,
CP: 34520
}, {
id: 232,
nom: "Celles",
brigade: 30,
CP: 34700
}, {
id: 233,
nom: "Fozieres",
brigade: 30,
CP: 34700
}, {
id: 234,
nom: "La Vacquerie et St Martin",
brigade: 30,
CP: 34520
}, {
id: 235,
nom: "Lauroux",
brigade: 30,
CP: 34700
}, {
id: 236,
nom: "Lavalette",
brigade: 30,
CP: 34700
}, {
id: 237,
nom: "Le Bosc",
brigade: 30,
CP: 34700
}, {
id: 238,
nom: "Le Puech",
brigade: 30,
CP: 34700
}, {
id: 239,
nom: "Les Plans",
brigade: 30,
CP: 34700
}, {
id: 240,
nom: "Lod\u00e8ve",
brigade: 30,
CP: 34700
}, {
id: 241,
nom: "Octon",
brigade: 30,
CP: 34800
}, {
id: 242,
nom: "Olmet et Villecun",
brigade: 30,
CP: 34700
}, {
id: 243,
nom: "Poujols",
brigade: 30,
CP: 34700
}, {
id: 244,
nom: "Soubes",
brigade: 30,
CP: 34700
}, {
id: 245,
nom: "Soumont",
brigade: 30,
CP: 34700
}, {
id: 246,
nom: "Saint Etienne de Gourgas",
brigade: 30,
CP: 34700
}, {
id: 247,
nom: "Saint Jean de la Blaqui\u00e8re",
brigade: 30,
CP: 34700
}, {
id: 248,
nom: "Saint Pierre de la Fage",
brigade: 30,
CP: 34520
}, {
id: 249,
nom: "Saint Privat",
brigade: 30,
CP: 34700
}, {
id: 250,
nom: "Saint Usclas du Bosc",
brigade: 30,
CP: 34700
}, {
id: 251,
nom: "Baillargues",
brigade: 31,
CP: 34670
}, {
id: 252,
nom: "Beaulieu",
brigade: 31,
CP: 34160
}, {
id: 253,
nom: "Castries",
brigade: 31,
CP: 34160
}, {
id: 254,
nom: "Restinclieres",
brigade: 31,
CP: 34160
}, {
id: 255,
nom: "Saint Bres",
brigade: 31,
CP: 34670
}, {
id: 256,
nom: "Saint Drezery",
brigade: 31,
CP: 34160
}, {
id: 257,
nom: "Saint Genies des Mourgues",
brigade: 31,
CP: 34160
}, {
id: 258,
nom: "Sussargues",
brigade: 31,
CP: 34160
}, {
id: 259,
nom: "Vendargues",
brigade: 31,
CP: 34740
}, {
id: 260,
nom: "La Grande Motte",
brigade: 32,
CP: 34280
}, {
id: 261,
nom: "Boisseron",
brigade: 33,
CP: 34160
}, {
id: 262,
nom: "Lunel",
brigade: 33,
CP: 34400
}, {
id: 263,
nom: "Lunel Viel",
brigade: 33,
CP: 34400
}, {
id: 264,
nom: "Marsillargues",
brigade: 33,
CP: 34590
}, {
id: 265,
nom: "Saturargues",
brigade: 33,
CP: 34400
}, {
id: 266,
nom: "Saussines",
brigade: 33,
CP: 34160
}, {
id: 267,
nom: "Saint Christol",
brigade: 33,
CP: 34400
}, {
id: 268,
nom: "Saint Just",
brigade: 33,
CP: 34400
}, {
id: 269,
nom: "Saint Nazaire de Pezan",
brigade: 33,
CP: 34400
}, {
id: 270,
nom: "Saint Series",
brigade: 33,
CP: 34400
}, {
id: 271,
nom: "Valergues",
brigade: 33,
CP: 34130
}, {
id: 272,
nom: "Verargues",
brigade: 33,
CP: 34400
}, {
id: 273,
nom: "Villetelle",
brigade: 33,
CP: 34400
}, {
id: 274,
nom: "Candillargues",
brigade: 34,
CP: 34130
}, {
id: 275,
nom: "Lansargues",
brigade: 34,
CP: 34130
}, {
id: 276,
nom: "Mauguio",
brigade: 34,
CP: 34130
}, {
id: 277,
nom: "Mudaison",
brigade: 34,
CP: 34130
}, {
id: 278,
nom: "Saint Aunes",
brigade: 34,
CP: 34130
}, {
id: 279,
nom: "Bouzigues",
brigade: 35,
CP: 34140
}, {
id: 280,
nom: "Loupian",
brigade: 35,
CP: 34140
}, {
id: 281,
nom: "Meze",
brigade: 35,
CP: 34140
}, {
id: 282,
nom: "Poussan",
brigade: 35,
CP: 34560
}, {
id: 283,
nom: "Villeveyrac",
brigade: 35,
CP: 34560
}, {
id: 284,
nom: "Bessan",
brigade: 36,
CP: 34550
}, {
id: 285,
nom: "Marseillan",
brigade: 36,
CP: 34340
}, {
id: 286,
nom: "Vias",
brigade: 36,
CP: 34450
}, {
id: 287,
nom: "Florensac",
brigade: 37,
CP: 34510
}, {
id: 288,
nom: "Pinet",
brigade: 37,
CP: 34850
}, {
id: 289,
nom: "Pomerols",
brigade: 37,
CP: 34810
}, {
id: 290,
nom: "Balaruc le Vieux",
brigade: 38,
CP: 34540
}, {
id: 291,
nom: "Balaruc les Bains",
brigade: 38,
CP: 34540
}, {
id: 292,
nom: "Cournonsec",
brigade: 38,
CP: 34660
}, {
id: 293,
nom: "Cournonterral",
brigade: 38,
CP: 34660
}, {
id: 294,
nom: "Gigean",
brigade: 38,
CP: 34770
}, {
id: 295,
nom: "Montbazin",
brigade: 38,
CP: 34560
}, {
id: 296,
nom: "Adissan",
brigade: 40,
CP: 34230
}, {
id: 297,
nom: "Aumes",
brigade: 40,
CP: 34530
}, {
id: 298,
nom: "Cazouls d'H\u00e9rault",
brigade: 40,
CP: 34120
}, {
id: 299,
nom: "Fontes",
brigade: 40,
CP: 34320
}, {
id: 300,
nom: "Lezignan la Cebe",
brigade: 40,
CP: 34120
}, {
id: 301,
nom: "Montagnac",
brigade: 40,
CP: 34530
}, {
id: 302,
nom: "Nizas",
brigade: 40,
CP: 34320
}, {
id: 303,
nom: "Saint Pons de Mauchiens",
brigade: 40,
CP: 34230
}, {
id: 304,
nom: "Usclas d'H\u00e9rault",
brigade: 40,
CP: 34230
}, {
id: 305,
nom: "Alignan du Vent",
brigade: 41,
CP: 34290
}, {
id: 306,
nom: "Castelnau de Guers",
brigade: 41,
CP: 34120
}, {
id: 307,
nom: "Caux",
brigade: 41,
CP: 34720
}, {
id: 308,
nom: "Montblanc",
brigade: 41,
CP: 34290
}, {
id: 309,
nom: "Nezignan l'Eveque",
brigade: 41,
CP: 34120
}, {
id: 310,
nom: "Pezenas",
brigade: 41,
CP: 34120
}, {
id: 311,
nom: "Saint Thibery",
brigade: 41,
CP: 34630
}, {
id: 312,
nom: "Tourbes",
brigade: 41,
CP: 34120
}, {
id: 313,
nom: "Valros",
brigade: 41,
CP: 34290
}, {
id: 314,
nom: "Fos",
brigade: 42,
CP: 34320
}, {
id: 315,
nom: "Fouzilhon",
brigade: 42,
CP: 34480
}, {
id: 316,
nom: "Gabian",
brigade: 42,
CP: 34320
}, {
id: 317,
nom: "Magalas",
brigade: 42,
CP: 34480
}, {
id: 318,
nom: "Margon",
brigade: 42,
CP: 34320
}, {
id: 319,
nom: "Montesquieu",
brigade: 42,
CP: 34320
}, {
id: 320,
nom: "Neffies",
brigade: 42,
CP: 34320
}, {
id: 321,
nom: "Pouzolles",
brigade: 42,
CP: 34480
}, {
id: 322,
nom: "Roquessels",
brigade: 42,
CP: 34320
}, {
id: 323,
nom: "Roujan",
brigade: 42,
CP: 34320
}, {
id: 324,
nom: "Vailhan",
brigade: 42,
CP: 34320
}, {
id: 325,
nom: "Abeilhan",
brigade: 43,
CP: 34290
}, {
id: 326,
nom: "Bassan",
brigade: 43,
CP: 34290
}, {
id: 327,
nom: "Coulobres",
brigade: 43,
CP: 34290
}, {
id: 328,
nom: "Espondeilhan",
brigade: 43,
CP: 34290
}, {
id: 329,
nom: "Lieuran les B\u00e9ziers",
brigade: 43,
CP: 34290
}, {
id: 330,
nom: "Puissalicon",
brigade: 43,
CP: 34480
}, {
id: 331,
nom: "Servian",
brigade: 43,
CP: 34290
}, {
id: 332,
nom: "Agde",
brigade: 46,
CP: 34300
}, {
id: 333,
nom: "B\u00e9ziers",
brigade: 45,
CP: 34500
}, {
id: 335,
nom: "Boujan sur Libron",
brigade: 45,
CP: 34760
}, {
id: 336,
nom: "Cebazan",
brigade: 8,
CP: 34360
}, {
id: 337,
nom: "Cers",
brigade: 45,
CP: 34420
}, {
id: 338,
nom: "Cessenon sur Orb",
brigade: 8,
CP: 34460
}, {
id: 339,
nom: "Frontignan",
brigade: 49,
CP: 34410
}, {
id: 340,
nom: "Lattes",
brigade: 50,
CP: 34970
}, {
id: 341,
nom: "Montpellier",
brigade: 51,
CP: 34e3
}, {
id: 342,
nom: "P\u00e9gairolles de Bu\u00e8ges",
brigade: 24,
CP: 34380
}, {
id: 343,
nom: "P\u00e9rols",
brigade: 50,
CP: 34470
}, {
id: 344,
nom: "S\u00e8te",
brigade: 48,
CP: 34200
}, {
id: 345,
nom: "Usclas du Bosc",
brigade: 30,
CP: 34700
}, {
id: 346,
nom: "Villeneuve les B\u00e9ziers",
brigade: 45,
CP: 34420
}, {
id: 347,
nom: "Cap d'Agde",
brigade: 47,
CP: 34300
} ]
};
